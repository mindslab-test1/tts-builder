# TTS Builder

## Framwork
    - Frontend: Vue
    - Backend: Spring boot

## 변경사항
#### Frontend
- 로그인 페이지 추가
- 회원가입 페이지 추가
- TTS 다운로드 기능 추가
- TTS 다운로드 목록 및 재다운로드 기능 추가
- 저장된 작업 목록 및 불러오기 기능 추가
- 계정 별 글자 수 제한 추가
- 결제 기능 추가
- 결제 관리자 페이지 추가

#### Backend
- user 테이블 추가
- login관련된 rest api 추가 
- 다운로드 기능 추가
	- 모든 음성을 하나의 wav 파일로 다운로드 가능
	- 각 줄마다 개별 wav 파일을 생성하여 zip파일을 다운로드 가능
- redownload 기능 추가
- 이니시스 결제 시스템 추가
	
