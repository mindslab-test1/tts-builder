package ai.mindslab.ttsmaker.configurers;

import ai.mindslab.ttsmaker.api.login.auth.AuthEntryPointJwt;
import ai.mindslab.ttsmaker.api.login.auth.AuthTokenFilter;
import ai.mindslab.ttsmaker.api.login.auth.JwtUtils;
import ai.mindslab.ttsmaker.api.login.service.UserDetailsServiceImpl;
import ai.mindslab.ttsmaker.api.login.vo.MembershipRoleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
        // securedEnabled = true,
        // jsr250Enabled = true,
        prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    final JwtUtils jwtUtils;

    final UserDetailsServiceImpl userDetailsService;

    private final AuthEntryPointJwt unauthorizedHandler;

    public WebSecurityConfig(JwtUtils jwtUtils, UserDetailsServiceImpl userDetailsService, AuthEntryPointJwt unauthorizedHandler) {
        this.jwtUtils = jwtUtils;
        this.userDetailsService = userDetailsService;
        this.unauthorizedHandler = unauthorizedHandler;
    }

    @Bean
    public AuthTokenFilter authenticationJwtTokenFilter() {
        return new AuthTokenFilter(jwtUtils, userDetailsService);
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        List<String> permitAllList = new ArrayList<String>();
        permitAllList.add("/**");
        permitAllList.add("/static/**");
        permitAllList.add("/maumAi/**");
        permitAllList.add("/script/**");
        permitAllList.add("/tts/**");
        permitAllList.add("/speaker/**");

        // TODO: CSRF || FRAME OPTIONS || CORS
        http.cors().and()
                .csrf().disable()
                .cors().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                /*.csrf().and().addFilterAfter(new CsrfCookieGeneratorFilter(), CsrfFilter.class)
                .exceptionHandling().and()*/
                .authorizeRequests()
//                .antMatchers(permitAllList.toArray(new String[]{})).permitAll()
                .antMatchers("/login/**").permitAll()
                .antMatchers("/payment/**").permitAll()
                .antMatchers("/admin/**").hasAuthority(MembershipRoleVo.ROLE_ADMIN.toString())
                .antMatchers("/job/download/**").hasAuthority(MembershipRoleVo.ROLE_PAID.toString())
                .anyRequest().authenticated();

        http
                .headers()
                .frameOptions().sameOrigin();

        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean(name = "corsConfigurationSource")
    CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
//        configuration.setAllowedOrigins(Arrays.asList("https://kbs-tool.maum.ai", "http://kbs-tool.maum.ai", "http://15.222.174.72:8002", "http://15.222.174.72", "http://15.165.16.245:8002", "https://15.165.16.245:8002", "http://114.108.173.114:8002", "https://localhost:8002", "http://127.0.0.1:8002", "http://10.52.180.37:8000", "http://localhost:8888", "https://tts.tobegin.net", "https://www.tobegin.net:8888"));
        configuration.setAllowedOrigins(Arrays.asList("https://kbs-tool.maum.ai", "http://kbs-tool.maum.ai", "http://15.222.174.72:8002", "http://15.222.174.72",  "http://3.35.96.173", "http://15.165.16.245:8002", "https://15.165.16.245:8002", "http://114.108.173.114:8002", "https://localhost:8002", "http://127.0.0.1:8002", "http://10.52.180.37:8000", "http://localhost:8888","https://tts.tobegin.net", "https://www.tobegin.net:8888", "https://ai.mindslab.net", "https://www.ai.mindslab:8888"));

        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "OPTIONS", "DELETE", "PUT"));
        configuration.setAllowedHeaders(Arrays.asList("Content-Type", "content-type", "x-requested-with", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "x-auth-token", "x-app-id", "Origin", "Accept", "Authorization", "X-Requested-With", "Access-Control-Request-Method", "Access-Control-Request-Headers", "X-Frame-Options"));
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }

}
