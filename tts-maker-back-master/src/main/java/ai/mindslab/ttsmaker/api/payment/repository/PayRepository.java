package ai.mindslab.ttsmaker.api.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ai.mindslab.ttsmaker.api.payment.domain.Pay;

import java.time.LocalDate;
import java.time.LocalDateTime;

public interface PayRepository extends JpaRepository<Pay, Long> {
    @Transactional
    @Query(value = "insert into PAY ( PRODUCT, PAYMENT, ISSUER, CARD_NO, DATE_FROM, DATE_TO, IMP, IMP_MONTH, PRICE" +
            ", USER_ID, STATUS, TID, PA_NO, PAYMENT_DATE, CREATE_DATE, CREATE_USER, UPDATE_DATE, UPDATE_USER) " +
            "values ( ?1, ?2, ?3, ?4,?5, ?6" +
            ", null, null, ?7, ?8, ?9, ?10, ?11, ?12, ?13, ?8, null, null ) ",
            nativeQuery = true)
    void insertPayInfo(int goodCode, String payment, String issuer, String cardNo, LocalDate dateFrom, LocalDate dateTo
            , long price, Long userNo, String status, String tid, String pano, LocalDate payDate
            , LocalDateTime createDate);

    @Transactional
    @Modifying
    @Query(value = "UPDATE PAY SET STATUS = '99', UPDATE_DATE = ?1, UPDATE_USER = ?2" +
            ", CANCELLED_DATE = ?1, CANCEL_ADMIN = ?2 WHERE 1 = 1 AND TID = ?3 ",
            nativeQuery = true)
    void updatePayToCancel(LocalDate now, Long updateUser, String tid);

    @Query(value = "select * from PAY WHERE TID = ?1",
            nativeQuery = true)
    Pay findByTid(String tid);
}
