package ai.mindslab.ttsmaker.api.Fcst.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ai.mindslab.ttsmaker.api.Fcst.domain.LandFcstItem;
import ai.mindslab.ttsmaker.api.Fcst.domain.SeaFcstItem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class FcstServiceImpl implements FcstService{

    @Value("${WEATHER_API}")
    private String WEATHER_API;

    @Value("${WEATHER_API_KEY}")
    private String WEATHER_API_KEY;

    private String current = "";

    public String getSeaFcst() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy년 MM월dd일");
        Date time = new Date();
        String currentTime = format.format(time);

        String fullScript = "안녕하십니까 기상청 reportTime 발표기준 새벽 네시 어업기상통봅니다. 먼저 각 육상별 자세한 날씹니다.@\n";

        Map<String, String> locations = new LinkedHashMap<>();
        Map<String, String> landLocations = new LinkedHashMap<>();
        locations = getLocationCode();
        landLocations = getLandLocationCode();

        Set<String> set = landLocations.keySet();
        Iterator<String> iter = set.iterator();

        //get land weather
        set = landLocations.keySet();
        iter = set.iterator();
        while(iter.hasNext()){
            String key = iter.next();
            fullScript = fullScript.concat(fcstApi("http://apis.data.go.kr/1360000/VilageFcstMsgService/getLandFcst", key, landLocations.get(key), "land"));
        }

        fullScript = fullScript.concat("다음으로 각 해상별로 자세한 날씹니다.@\n");

        // get sea weather
        set = locations.keySet();
        iter = set.iterator();
        while (iter.hasNext()) {
            String key = iter.next();
            fullScript = fullScript.concat(fcstApi(WEATHER_API, key, locations.get(key), "sea"));
        }

        fullScript = fullScript.replace("reportTime", current.substring(0, 4) + "년 " + current.substring(4, 6) + "월 " + current.substring(6, 8) + "일 " + current.substring(8, 10) + "시 ");

        return fullScript.concat("지금까지 KBS 어업기상통보였습니다.\n");
    }

    public String fcstApi(String apiUrl, String locationCode, String location, String seaLand) {
        String result = "";
        String script = "";

        try {
            StringBuilder urlBuilder = new StringBuilder(apiUrl);
            urlBuilder.append("?" + URLEncoder.encode("ServiceKey", "UTF-8") + WEATHER_API_KEY); /*Service Key*/
            urlBuilder.append("&" + URLEncoder.encode("pageNo", "UTF-8") + "=" + URLEncoder.encode("1", "UTF-8"));
            urlBuilder.append("&" + URLEncoder.encode("numOfRows", "UTF-8") + "=" + URLEncoder.encode("10", "UTF-8"));
            urlBuilder.append("&" + URLEncoder.encode("dataType", "UTF-8") + "=" + URLEncoder.encode("JSON", "UTF-8"));
            urlBuilder.append("&" + URLEncoder.encode("regId", "UTF-8") + "=" + URLEncoder.encode(locationCode, "UTF-8"));

            URL url = new URL(urlBuilder.toString());
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-type", "application/json");

            BufferedReader rd;
            if(conn.getResponseCode()==500){
                return "";
            }
            if (conn.getResponseCode() >= 200 && conn.getResponseCode() <= 300) {
                rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            } else {
                rd = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
            }
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                sb.append(line);
            }
            rd.close();
            conn.disconnect();
            result = sb.toString();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

        //convert String to json
        try {
            JSONParser parser = new JSONParser();
            JSONObject obj = (JSONObject) parser.parse(result);
            JSONObject parse_response = (JSONObject) obj.get("response");
            JSONObject parse_body = (JSONObject) parse_response.get("body");
            JSONObject parse_item = (JSONObject) parse_body.get("items");
            JSONArray jsonItems = (JSONArray) parse_item.get("item");

            String items = jsonItems.toJSONString();
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<SeaFcstItem>>() {}.getType();
            Type landListType = new TypeToken<ArrayList<LandFcstItem>>(){}.getType();

            List<SeaFcstItem> itemList= new LinkedList<>();
            List<LandFcstItem> landitemList = new LinkedList<>();

            if("sea".equals(seaLand)) {
                itemList = gson.fromJson(items, listType);
                script = getScriptFromLocation(itemList, location);
            }
            else if("land".equals(seaLand)){
                landitemList = gson.fromJson(items, landListType);
                script = getLandScriptFromLocation(landitemList, location);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return script;
    }

    public String getLandScriptFromLocation(List<LandFcstItem> locationItem, String location) {

        Map<String, String> windDirection = new HashMap<>();
        windDirection = getWindDirection();

        SimpleDateFormat format = new SimpleDateFormat("HH");
        Date time = new Date();
        int currentTime = Integer.parseInt(format.format(time));

        LandFcstItem am = new LandFcstItem();
        LandFcstItem pm = new LandFcstItem();

        if (currentTime >= 17) {
            am = locationItem.get(1);
            pm = locationItem.get(2);
        } else if (currentTime >= 5 && currentTime < 11) {
            am = locationItem.get(0);
            pm = locationItem.get(1);
        } else {
            am = locationItem.get(1);
            pm = locationItem.get(2);
        }

        //getWeatherSky(am.getWf());
        if (am.getAnnounceTime() != null && am.getAnnounceTime() != "") {
            current = am.getAnnounceTime();
        }

		/*String script =
				location + "\n" +
						"오전 기온은 " +am.getTa() + "도이며 바람은 " + windDirection.get(am.getWd1()) + " 내지 " + windDirection.get(am.getWd2()) + " 방향으로 " + getwslt(am.getWsIt()) + " 불겠고 날씨는 " + getWeatherSky(am.getWf()) + ".\n" +
						"오후 기온은 " + pm.getTa() + "도이며 바람은 " + windDirection.get(pm.getWd1()) + " 내지 " + windDirection.get(pm.getWd2()) + " 방향으로 " + getwslt(pm.getWsIt()) + " 불겠고 날씨는 " + getWeatherSky(pm.getWf()) + "@ \n";*/
        String script =
                location + "\n" +
                        "오전 기온은 " +am.getTa() + "도이며" + " 날씨는 " + getWeatherSky(am.getWf()) + ".\n" +
                        "오후 기온은 " + pm.getTa() + "도이며" + " 날씨는 " + getWeatherSky(pm.getWf()) + "@ \n";

        script = script.replaceAll("맑음", "맑습").replaceAll("많음", "많습");
        script = script.replaceAll("흐림", "흐립");

        return script;
    }

    public String getwslt(String wslt){

        if("1".equals(wslt))
            return "바람이 약간 강하게";
        else if("2".equals(wslt))
            return "바람이 강하게";
        else if("3".equals(wslt))
            return "바람이 매우 강하게";
        else
            return "";
    }



    public String getScriptFromLocation(List<SeaFcstItem> locationItem, String location) {

        Map<String, String> windDirection = new HashMap<>();
        windDirection = getWindDirection();

        SimpleDateFormat format = new SimpleDateFormat("HH");
        Date time = new Date();
        int currentTime = Integer.parseInt(format.format(time));

        SeaFcstItem am = new SeaFcstItem();
        SeaFcstItem pm = new SeaFcstItem();


        if (currentTime >= 17) {
            am = locationItem.get(1);
            pm = locationItem.get(2);
        } else if (currentTime >= 5 && currentTime < 11) {
            am = locationItem.get(0);
            pm = locationItem.get(1);
        } else {
            am = locationItem.get(1);
            pm = locationItem.get(2);
        }

        //getWeatherSky(am.getWf());
        if (am.getTmFc() != null && am.getTmFc() != "") {
            current = am.getTmFc();
        }

        String script =
                location + "\n" +
                        "오전 " + windDirection.get(am.getWd1()) + " 내지 " + windDirection.get(am.getWd2()) + " 방향으로, " + checkDouble(Double.parseDouble(am.getWs1())) + "에서 "
                        + checkDouble(Double.parseDouble(am.getWs2())) + "미터로 불겠고 날씨는 " + getWeatherSky(am.getWf()) + ".\n" +
                        "오후 " + windDirection.get(pm.getWd1()) + " 내지 " + windDirection.get(pm.getWd2()) + " 방향으로, " + checkDouble(Double.parseDouble(pm.getWs1())) + "에서 "
                        + checkDouble(Double.parseDouble(pm.getWs2())) + "미터로 불겠고 날씨는 " + getWeatherSky(pm.getWf()) + ".\n" +
                        "물결은 " + checkDouble(Double.parseDouble(am.getWh1())) + " 내지 "
                        + checkDouble(Double.parseDouble(am.getWh2())) + "미터로 일겠습니다.@ \n";

        script = script.replaceAll("맑음", "맑습").replaceAll("많음", "많습");
        script = script.replaceAll("흐림", "흐립");

        return script;
    }


    //조사 체크
    public Map<String, String> numberKor() {
        Map<String, String> numberKor = new LinkedHashMap<>();
        numberKor.put("0", "십");
        numberKor.put("1", "일");
        numberKor.put("2", "이");
        numberKor.put("3", "삼");
        numberKor.put("4", "사");
        numberKor.put("5", "오");
        numberKor.put("6", "육");
        numberKor.put("7", "칠");
        numberKor.put("8", "팔");
        numberKor.put("9", "구");

        return numberKor;
    }

    public String getPostWord(String str, String firstVal, String secondVal) {

        Map<String, String> numberKor = numberKor();

        try {
            //char laststr = str.charAt(str.length() - 1);
            String lastNum = numberKor.get(String.valueOf(str.charAt(str.length() - 1)));
            char laststr = lastNum.charAt(lastNum.length() - 1);

            // 한글의 제일 처음과 끝의 범위밖일 경우는 오류
            if (laststr < 0xAC00 || laststr > 0xD7A3) {
                return str;
            }

            int lastCharIndex = (laststr - 0xAC00) % 28;

            // 종성인덱스가 0이상일 경우는 받침이 있는경우이며 그렇지 않은경우는 받침이 없는 경우
            if (lastCharIndex > 0) {
                // 받침이 있는경우
                // 조사가 '로'인경우 'ㄹ'받침으로 끝나는 경우는 '로' 나머지 경우는 '으로'
                if (firstVal.equals("으로") && lastCharIndex == 8) {
                    str += secondVal;
                } else {
                    str += firstVal;
                }
            } else {
                // 받침이 없는 경우
                str += secondVal;
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }

        return str;
    }

    public String checkDouble(double d) {

        if (d == (int) d)
            return Integer.toString((int) d);
        else
            return Double.toString(d);
    }

    public String getWeatherSky(String weatherSky) {
        String[] strarr = weatherSky.split(" ");

        if ("눈".equals(strarr[strarr.length - 1]))
            weatherSky = weatherSky.concat("이 오겠습니다.");
        else if ("비".equals(strarr[strarr.length - 1]))
            weatherSky = weatherSky.concat("가 내리겠습니다.");
        else
            weatherSky = weatherSky.concat("니다");

        return weatherSky;
    }

    public Map<String, String> getWindDirection() {
        Map<String, String> windDirection = new HashMap<>();

        windDirection.put("N", "북");
        windDirection.put("NNE", "북북동");
        windDirection.put("NE", "북동");
        windDirection.put("ENE", "동북동");
        windDirection.put("E", "동");
        windDirection.put("ESE", "동남동");
        windDirection.put("SE", "남동");
        windDirection.put("SSE", "남남동");
        windDirection.put("S", "남");
        windDirection.put("SSW", "남남서");
        windDirection.put("SW", "남서");
        windDirection.put("WSW", "서남서");
        windDirection.put("W", "서");
        windDirection.put("WNW", "서북서");
        windDirection.put("NW", "북서");
        windDirection.put("NNW", "북북서");


        return windDirection;
    }

    public Map<String, String> getLandLocationCode() {
        Map<String, String> locationCode = new LinkedHashMap<>();
        locationCode.put("11B10101", "서울");
        locationCode.put("11H20201", "부산");
        locationCode.put("11H10701", "대구");
        locationCode.put("11F20501", "광주");
        locationCode.put("11F10201", "전주");
        locationCode.put("11C20401", "대전");
        locationCode.put("11C10301", "청주");
        locationCode.put("11D10301", "강원");
        locationCode.put("11G00201", "제주");


        return locationCode;
    }

    public Map<String, String> getLocationCode() {
        Map<String, String> locationCode = new LinkedHashMap<>();

        locationCode.put("12B20100", "남해동부앞바다");
        locationCode.put("12B20200", "남해동부먼바다");
        locationCode.put("12B10100", "남해서부앞바다");
        locationCode.put("12C10100", "동해남부앞바다");
        locationCode.put("12C30100", "동해북부앞바다");
        locationCode.put("12C30200", "동해북부먼바다");
        locationCode.put("12C20100", "동해중부앞바다");
        locationCode.put("12C20200", "동해중부먼바다");
        locationCode.put("12A30100", "서해남부앞바다");
        locationCode.put("12A10200", "서해북부먼바다");
        locationCode.put("12A20200", "서해중부먼바다");
        locationCode.put("12A20100", "서해중부앞바다");
        locationCode.put("12B10400", "제주도남쪽먼바다");
        locationCode.put("12B10300", "제주도앞바다");
        locationCode.put("12A20102", "인천·경기남부앞바다");
        locationCode.put("12A20101", "경기북부앞바다");
        locationCode.put("12B20103", "부산앞바다");
        locationCode.put("12C10101", "울산앞바다");
        locationCode.put("12B20102", "경남중부남해앞바다");
        locationCode.put("12B20101", "경남서부남해앞바다");
        locationCode.put("12B20104", "거제시동부앞바다");
        locationCode.put("12C10102", "경북남부앞바다");
        locationCode.put("12C10103", "경북북부앞바다");
        locationCode.put("22A30103", "전남북부서해앞바다");
        locationCode.put("22A30104", "전남중부서해앞바다");
        locationCode.put("22A30105", "전남남부서해앞바다");
        locationCode.put("12B10101", "전남서부남해앞바다");
        locationCode.put("12B10102", "전남동부남해앞바다");
        locationCode.put("22A30101", "전북북부앞바다");
        locationCode.put("22A30102", "전북남부앞바다");
        locationCode.put("12A20103", "충남북부앞바다");
        locationCode.put("12A20104", "충남남부앞바다");
        locationCode.put("12C20103", "강원북부앞바다");
        locationCode.put("12C20102", "강원중부앞바다");
        locationCode.put("12C20101", "강원남부앞바다");
        locationCode.put("12B10302", "제주도북부앞바다");
        locationCode.put("12B10303", "제주도남부앞바다");
        locationCode.put("12B10301", "제주도동부앞바다");
        locationCode.put("12B10304", "제주도서부앞바다");
        locationCode.put("12B10201", "남해서부서쪽먼바다");
        locationCode.put("12B10202", "남해서부동쪽먼바다");

        return locationCode;
    }
}
