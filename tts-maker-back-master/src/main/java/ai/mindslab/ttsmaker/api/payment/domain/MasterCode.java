package ai.mindslab.ttsmaker.api.payment.domain;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "MASTER_CODE")
public class MasterCode implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private int id;

    @Column(name = "CATEGORY1", columnDefinition = "varchar(50)")
    private String category1;

    @Column(name = "CATEGORY2", columnDefinition = "varchar(50)")
    private String category2;

    @Column(name = "CATEGORY3", columnDefinition = "varchar(50)")
    private String category3;

    @Column(name = "NAME", columnDefinition = "varchar(50)")
    private String name;
}
