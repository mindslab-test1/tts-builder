package ai.mindslab.ttsmaker.api.payment.domain;

import ai.mindslab.ttsmaker.api.login.domain.User;
import ai.mindslab.ttsmaker.api.payment.vo.PaymentVo;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@SqlResultSetMapping(
        name = "RoutineBillingMapper",
        classes = {
                @ConstructorResult(
                        targetClass = PaymentVo.class,
                        columns = {
                                @ColumnResult(name = "userNo", type = Long.class),
                                @ColumnResult(name = "billingKey", type = String.class),
                                @ColumnResult(name = "userName", type = String.class),
                                @ColumnResult(name = "userEmail", type = String.class),
                                @ColumnResult(name = "goodCode", type = Integer.class),
                                @ColumnResult(name = "price", type = Long.class),
                        })
        })
@SqlResultSetMapping(
        name = "UnpaymentMapper",
        classes = {
                @ConstructorResult(
                        targetClass = PaymentVo.class,
                        columns = {
                                @ColumnResult(name = "userNo", type = Long.class),
                                @ColumnResult(name = "userEmail", type = String.class),
                                @ColumnResult(name = "paymentDate", type = String.class),
                        })
        })
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "BILLING", uniqueConstraints = {@UniqueConstraint(name = "UK_BILLING_ID", columnNames = {"ID", "USER_ID"})})
public class Billing {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private int id;

    @Column(name = "BILL_KEY", columnDefinition = "varchar(200)")
    private String billKey;

    @ManyToOne
    @JoinColumn(name = "PRODUCT")
    private Product product;

    @Column(name = "PAYMENT", columnDefinition = "varchar(50)")
    private String payment;

    //    @Column(name = "ISSUER", columnDefinition = "varchar(10)")
    @OneToOne
    @JoinColumn(name = "ISSUER", referencedColumnName = "CATEGORY3")
    private MasterCode issuer;

    @Column(name = "NUMBER", columnDefinition = "varchar(30)")
    private String number;

    @Column(name = "PAYMENT_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate paymentDate;


    @OneToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private User userId;

    @Column(name = "ACTIVE", columnDefinition = "int(1)")
    private Integer active;

    //등록일시
    @Column(name = "CREATE_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createDate;

    @Column(name = "CREATE_USER", columnDefinition = "int(8)")
    private Long createUser;

    //수정일시
    @Column(name = "UPDATE_DATE")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updateDate;

    @Column(name = "UPDATE_USER", columnDefinition = "int(8)")
    private Long updateUser;

    @Column(name = "FAIL_REASON", columnDefinition = "varchar(70)")
    private String failReason;

    @OneToOne(mappedBy = "billing")
    private BillLog billLog;
}
