package ai.mindslab.ttsmaker.api.job.vo.response;

import ai.mindslab.ttsmaker.api.job.domain.JobM;
import lombok.*;

import java.util.List;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobContentVo {
	private long id;
	private String jobId;
	private String jobTitle;
	private List<JobSpeakerVo> jobSpeakers;
	private List<JobScriptVo> jobScripts;
	
	public static JobContentVo of(JobM entity, List<JobSpeakerVo> speakers, List<JobScriptVo> scripts) {
        return JobContentVo.builder()
        		.id(entity.getId())
        		.jobId(entity.getJobId())
        		.jobTitle(entity.getJobTitle())
        		.jobSpeakers(speakers)
        		.jobScripts(scripts)
        		.build();
    }
}
