package ai.mindslab.ttsmaker.api.payment.vo;

import lombok.*;

@Data
@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class PaymentVo {

    private int id;					// id
    private Long userNo;				// 회원번호
    private String userName	;		// 회원이름
    private String userEmail;		// 회원이메일
    private String payment;			// 결제수단
    private String issuer;			// 카드사
    private String issuerName;		// 카드사 이름
    private String billingKey;		// 빌링키
    private String cardNo;			// 카드번호
    private String status;			// 결제결과값
    private String goodName;		// 상품 이름
    private int goodCode;			// 상품 정보 코드
    private long price;				// 가격
    private long priceUs;			// paypal 가격
    private String pano;			// 결제승인번호
    private String createDate;		// 데이터생성일
    private String dateFrom;		// 결제시작일
    private String dateTo;			// 결제종료일
    private String payDate;			// 결제승인일
    private String tid;				// 이니시스 결제 취소를 위해 필요한 정보
    private String paymentDate;		// 다음결제일 (billing_t)
    private String cancelledDate;   // 결제 취소일자
    private String cancelAdmin;		// admin에서 취소시 userId

    private int imp;				// 할부여부
    private int impMonth;			// 할부개월수

    private String failReason;		// 결제실패이유 (ResultDetailMsg)

//	private String amount;			// 가격(paypal)

    private int active;			// 활성화여부(billing_t active)

    public PaymentVo(Long userNo, String billingKey, String userName, String userEmail, Integer goodCode, Long price){
        this.userNo = userNo;
        this.billingKey = billingKey;
        this.userName = userName;
        this.userEmail = userEmail;
        this.goodCode = goodCode;
        this.price = price;
    }

    public PaymentVo(Long userNo, String userEmail, String paymentDate){
        this.userNo = userNo;
        this.userEmail = userEmail;
        this.payDate = paymentDate;
    }

    @Override
    public String toString() {
        return "PaymentVo{" +
                "userNo=" + userNo +
                ", userName='" + userName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", payment='" + payment + '\'' +
                ", issuer='" + issuer + '\'' +
                ", issuerName='" + issuerName + '\'' +
                ", billingKey='" + billingKey + '\'' +
                ", cardNo='" + cardNo + '\'' +
                ", status='" + status + '\'' +
                ", goodName='" + goodName + '\'' +
                ", goodCode=" + goodCode +
                ", price=" + price +
                ", pano='" + pano + '\'' +
                ", createDate='" + createDate + '\'' +
                ", dateFrom='" + dateFrom + '\'' +
                ", dateTo='" + dateTo + '\'' +
                ", payDate='" + payDate + '\'' +
                ", tid='" + tid + '\'' +
                ", imp=" + imp +
                ", impMonth=" + impMonth +
                ", cancelledDate=" + cancelledDate +
                ", cancelAdmin=" + cancelAdmin +
                '}';
    }
}
