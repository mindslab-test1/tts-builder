package ai.mindslab.ttsmaker.api.payment.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "PAYMENT")
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private Long id;

    //이름
    @Column(name = "NAME", columnDefinition = "varchar(100)")
    private String name;

    //user ID
    @Column(name = "USER_ID", columnDefinition = "varchar(100)")
    private String userId;

    //회사
    @Column(name = "COMPANY", columnDefinition = "varchar(20)")
    private String company;

    //전화번호
    @Column(name = "PHONE_NUMBER", columnDefinition = "varchar(11)")
    private String phoneNumber;

    @Column(name = "PAYMENT", columnDefinition = "varchar(50)")
    private String payment;

    @Column(name = "PRODUCT_NAME", columnDefinition = "varchar(50)")
    private String productName;

    @Column(name = "PRICE", columnDefinition = "int(11)")
    private Long price;

    @Column(name = "CURRENCY", columnDefinition = "varchar(10)")
    private String currency;

        @Column(name = "CREATE_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createDate;

    //수정일시
    @Column(name = "CANCELLED_DATE")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate cancelledDate;

    @Column(name = "CARD_NAME", columnDefinition = "varchar(50)")
    private String cardName;

    @Column(name = "TID", columnDefinition = "varchar(60)")
    private String tid;

    @Column(name = "PA_NO", columnDefinition = "varchar(30)")
    private String paNo;

    @Column(name = "STATUS", columnDefinition = "varchar(10)")
    private String status;

    //Pay Product
    @Column(name="PAY_COUNT", columnDefinition = "int(3) default 0")
    private Long payCount;

    @Column(name = "PAYMENT_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime paymentDate;

    // Cancellable
    @Column(name="CAN_CANCEL", columnDefinition = "int(1)")
    private Integer canCancel;
}
