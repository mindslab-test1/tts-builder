package ai.mindslab.ttsmaker.api.payment.repository;

import ai.mindslab.ttsmaker.api.payment.domain.MasterCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MasterCodeRepository extends JpaRepository<MasterCode, Long> {
    MasterCode findByCategory3(String category3);
}
