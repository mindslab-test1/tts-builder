package ai.mindslab.ttsmaker.api.job.vo.response;


import ai.mindslab.ttsmaker.api.job.domain.TTSFileDownload;
import ai.mindslab.ttsmaker.util.DateUtils;
import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDownloadInfoVo {
    private Long id;
    private String downloadedAt;
    private String fileName;
    private double playLength;
    private int charCount;
    private String downloadUrl;
    private boolean isZiped;


    public static UserDownloadInfoVo of(TTSFileDownload entity) {
        return UserDownloadInfoVo.builder()
                .id(entity.getId())
                .downloadUrl(entity.getDownloadURL())
                .playLength(entity.getPlayLength())
                .fileName(entity.getJobId())
                .charCount(entity.getScriptText().replaceAll("\\s|;","").length())
                .downloadedAt(DateUtils.asLocalDate(entity.getDownloadedAt()))
                .isZiped(entity.getIsZiped().equals("Y"))
                .build();
    }
}
