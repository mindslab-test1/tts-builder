package ai.mindslab.ttsmaker.api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.reactive.function.client.WebClientException;

public class TTSMakerApiException extends WebClientException {

	private static final long serialVersionUID = 3639406765064921043L;
	private final HttpStatus statusCode;
	private final String errorCode;
	private final String errorMessage;
    
    public TTSMakerApiException(HttpStatus statusCode, String errorCode, String errorMessage) {
		super(statusCode.getReasonPhrase());
        this.statusCode = statusCode;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

	public HttpStatus getStatusCode() {
		return statusCode;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}
}