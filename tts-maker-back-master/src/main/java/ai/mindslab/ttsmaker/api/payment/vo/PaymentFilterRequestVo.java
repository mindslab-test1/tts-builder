package ai.mindslab.ttsmaker.api.payment.vo;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentFilterRequestVo {
    @JsonProperty("isFiltered")
    private boolean isFiltered;
    @JsonProperty("isLatestOnly")
    private boolean isLatestOnly;
    private String selectPayment;
//    private String payDayStart;
//    private String payDayEnd;
    private String termPicked;
    private String selectedStatus;
    private String payCount;
    private String payCountRange;
    private String userName;
    private String userEmail;
    private String userCompany;
    private String productName;
    private String roleName;
}
