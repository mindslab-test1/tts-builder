package ai.mindslab.ttsmaker.api.job.vo.request;

import ai.mindslab.ttsmaker.api.job.vo.response.DmzDestVo;
import lombok.*;

import java.util.List;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobContentSaveVo {
	private String jobTitle;								// 작업 제목
	private List<JobSpeakerSaveVo> speakers;			// 선택한 화자 리스트
	private List<JobSpeakerSaveVo> deletedSpeakers;	// 삭제한 화자 리스트(DB 저장 데이터 한해)
	private List<JobScriptSaveVo> scripts;				// 작성한 스크립트 리스트
	private List<JobScriptSaveVo> deletedScripts;		// 삭제할 스크립트 리스트
	private String userId;
	private DmzDestVo destination;
}
