package ai.mindslab.ttsmaker.api.script.controller;

import ai.mindslab.ttsmaker.api.job.service.JobService;
import ai.mindslab.ttsmaker.api.job.vo.response.JobScriptVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/script")
public class ScriptRestController {

	private final JobService jobService;

	private static Logger logger = LoggerFactory.getLogger(ScriptRestController.class.getName());

	public ScriptRestController(JobService jobService) {
		this.jobService = jobService;
	}


	@ResponseBody
	@GetMapping(value = "/{jobId}/list")
	public List<JobScriptVo> getJobScriptList(@PathVariable String jobId) {
		logger.info("[Get List of Job Scripts] : {}", jobId);
		return jobService.getJobScriptList(jobId);
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}/{scriptId}")
	public JobScriptVo getJobScript(@PathVariable String jobId, @PathVariable String scriptId) {
		logger.info("[Get Job Script] : {}, ", jobId, scriptId);
		return jobService.getJobScript(jobId, scriptId);
	}
}
