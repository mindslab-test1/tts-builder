package ai.mindslab.ttsmaker.api.payment.domain;

import ai.mindslab.ttsmaker.api.login.domain.User;
import ai.mindslab.ttsmaker.api.payment.vo.PaymentFilterResponseVo;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@SqlResultSetMapping(
        name = "GetPaymentsMapping",
        classes = {
                @ConstructorResult(
                        targetClass = PaymentFilterResponseVo.class,
                        columns = {
                                @ColumnResult(name = "id", type = Long.class),
                                @ColumnResult(name = "name", type = String.class),
                                @ColumnResult(name = "userId", type = String.class),
                                @ColumnResult(name = "phoneNumber", type = String.class),
                                @ColumnResult(name = "company", type = String.class),
                                @ColumnResult(name = "payment", type = String.class),
                                @ColumnResult(name = "productName", type = String.class),
                                @ColumnResult(name = "price", type = Long.class),
                                @ColumnResult(name = "currency", type = String.class),
                                @ColumnResult(name = "createDate", type = String.class),
                                @ColumnResult(name = "cancelledDate", type = String.class),
                                @ColumnResult(name = "cardName", type = String.class),
                                @ColumnResult(name = "tid", type = String.class),
                                @ColumnResult(name = "paNo", type = String.class),
                                @ColumnResult(name = "status", type = String.class),
                                @ColumnResult(name = "payCount", type = Long.class),
                                @ColumnResult(name = "paymentDate", type = String.class),
                                @ColumnResult(name = "canCancel", type = Integer.class),
                        }
                )
        }
)
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "PAY")
public class Pay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "PRODUCT", referencedColumnName = "ID")
    private Product product;

    @Column(name = "PAYMENT", columnDefinition = "varchar(50)")
    private String payment;

    //    @Column(name = "ISSUER", columnDefinition = "varchar(10)")
    @OneToOne
    @JoinColumn(name = "ISSUER", referencedColumnName = "CATEGORY3")
    private MasterCode issuer;

    @Column(name = "CARD_NO", columnDefinition = "varchar(30)")
    private String cardNo;

    //등록일시
    @Column(name = "DATE_FROM")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate dateFrom;

    //수정일시
    @Column(name = "DATE_TO")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate dateTo;

    @Column(name = "IMP", columnDefinition = "varchar(10)")
    private String imp;

    @Column(name = "IMP_MONTH", columnDefinition = "varchar(10)")
    private String impMonth;

    @Column(name = "PRICE", columnDefinition = "int(11)")
    private Long price;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private User userId;

    @Column(name = "STATUS", columnDefinition = "varchar(10)")
    private String status;

    @Column(name = "TID", columnDefinition = "varchar(60)")
    private String tid;

    @Column(name = "PA_NO", columnDefinition = "varchar(30)")
    private String paNo;

    @Column(name = "PAYMENT_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate paymentDate;

    //등록일시
    @Column(name = "CREATE_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createDate;

    @Column(name = "CREATE_USER", columnDefinition = "int(8)")
    private Long createUser;

    //수정일시
    @Column(name = "UPDATE_DATE")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updateDate;

    @Column(name = "UPDATE_USER", columnDefinition = "int(8)")
    private Long updateUser;

    //수정일시
    @Column(name = "CANCELLED_DATE")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime cancelledDate;

    @Column(name = "CANCEL_ADMIN", columnDefinition = "int(8)")
    private Long cancelAdmin;

//    @OneToMany(mappedBy = "pay")
//    private List<LogPay> logPayList;
}
