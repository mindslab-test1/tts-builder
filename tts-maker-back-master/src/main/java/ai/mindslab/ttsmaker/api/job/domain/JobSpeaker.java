package ai.mindslab.ttsmaker.api.job.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import ai.mindslab.ttsmaker.api.speaker.domain.Speaker;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "JOB_SPEAKER")
public class JobSpeaker implements Serializable {
	
	private static final long serialVersionUID = 9182436812539983497L;

	//관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private long id;
    
    //JOB ID
    @Column(name = "JOB_ID", columnDefinition = "varchar(36)", nullable = false)
    private String jobId;
    
    //화자 ID
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SPEAKER_ID", columnDefinition = "varchar(36)", referencedColumnName = "SPEAKER_ID", updatable = true, nullable = false ,foreignKey = @ForeignKey(name = "SPEAKER_FK1"))
    private Speaker speaker;
    
    //정렬순서
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_SEQ", columnDefinition = "int(11)")
    private long orderSeq;
    
    //삭제여부
    @Column(name = "DEL_YN", columnDefinition = "char(1) default 'N'")
    private String isDeleted;
    
    //등록일시
    @Column(name = "INS_DT")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    //수정일시
    @Column(name = "UPD_DT")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}
