package ai.mindslab.ttsmaker.api.speaker.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ai.mindslab.ttsmaker.api.speaker.domain.Speaker;

import java.util.List;

public interface SpeakerRepository extends JpaRepository<Speaker, Long> { 
	
	@Query("select p from Speaker p where p.isDeleted = 'N'")
	List<Speaker> findActiveList();
}
