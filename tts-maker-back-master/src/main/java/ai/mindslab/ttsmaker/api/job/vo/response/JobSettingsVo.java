package ai.mindslab.ttsmaker.api.job.vo.response;

import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobSettingsVo {
	private String style;
	private Integer volume;
	private String speed;
	private Long sleep;

	public static JobSettingsVo of(JobScript entity) {
        return JobSettingsVo.builder()
        		.style(entity.getSettingsStyle())
        		.volume(entity.getSettingsVolume())
        		.speed(entity.getSettingsSpeed())
        		.sleep(entity.getSettingsSleep())
        		.build();
    }
}
