package ai.mindslab.ttsmaker.api.speaker.service;

import ai.mindslab.ttsmaker.api.speaker.domain.Speaker;
import ai.mindslab.ttsmaker.api.speaker.vo.SpeakerVo;

import java.util.List;

public interface SpeakerService {
	
	/**
	 * <pre>
	 * 스피커(화자) 전체 리스트 조회
	 * </pre>
	 * @return
	 */
	public List<SpeakerVo> getAllList();
	
	
	/**
	 * <pre>
	 * 스피커(화자) 전체 모델 엔티 조회
	 * </pre>
	 * @return
	 */
	public List<Speaker> allEntities();


	/**
	 * <pre>
	 * 스피커(화자) 랜덤 조회
	 * </pre>
	 * @return
	 */
	public SpeakerVo getRandomSpeaker();
}
