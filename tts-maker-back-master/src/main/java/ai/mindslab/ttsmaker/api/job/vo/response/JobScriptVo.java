package ai.mindslab.ttsmaker.api.job.vo.response;

import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import lombok.*;
import ai.mindslab.ttsmaker.api.speaker.domain.Speaker;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobScriptVo {
	private long id;
	private String jobId;
	private String scriptId;
	private String speakerId;
	private String speakerNm;
	private String voiceName; // TTS VoiceName
	private String imgUrl;
	private String scriptText;
	private String ttsResourceUrl;
	private String ttsResourceExpiryDate;
	private JobSettingsVo settings;
	private long orderSeq;
	private String ttsState;
	private String rowState;
	
	public static JobScriptVo of(JobScript entity) {
		Speaker speaker = entity.getSpeaker();
        return JobScriptVo.builder()
        		.id(entity.getId())
        		.jobId(entity.getJobId())
        		.scriptId(entity.getScriptId())
        		.speakerId(speaker.getSpeakerId())
        		.speakerNm(speaker.getSpeakerNm())
        		.voiceName(speaker.getVoiceName())
        		.imgUrl(speaker.getImgUrl())
        		.scriptText(entity.getScriptText())
        		.ttsResourceUrl(entity.getTtsResourceUrl())
        		.ttsResourceExpiryDate(entity.getTtsResourceExpiryDate())
        		.settings(JobSettingsVo.of(entity))
        		.orderSeq(entity.getOrderSeq())
        		.ttsState(entity.getTtsState())
        		.rowState(entity.getRowState())
        		.build();
    }
}
