package ai.mindslab.ttsmaker.api.login.vo;

import lombok.*;
import ai.mindslab.ttsmaker.api.login.domain.User;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserMembershipInfoVo {
    private String userId;
    private String role;


    public static UserMembershipInfoVo of(User entity) {
        UserMembershipInfoVo userInfo = UserMembershipInfoVo.builder()
                .userId(entity.getUserId())
                .role(entity.getMembershipRole().getName().toString())
                .build();
        return userInfo;
    }

}
