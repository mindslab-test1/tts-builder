package ai.mindslab.ttsmaker.api.login.controller;

import ai.mindslab.ttsmaker.api.login.vo.UserMembershipInfoVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import ai.mindslab.ttsmaker.api.login.domain.User;
import ai.mindslab.ttsmaker.api.login.service.UserService;

import java.io.IOException;

@RestController
@RequestMapping("/user")
public class UserController {

    final UserService userService;

    private static Logger logger = LoggerFactory.getLogger(UserController.class.getName());

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @ResponseBody
    @GetMapping(value="/{userId}/charConsumed")
    public int getCharConsumed(@PathVariable String userId) throws IOException {
        logger.info("[Get User charConsumed] : {}", userId);
        return userService.getCharConsumed(userId);
    }

    @ResponseBody
    @PutMapping(value="/{userId}/charConsumed")
    public void updateCharConsumed(@PathVariable String userId, @RequestParam int charConsumed ) throws IOException {
        logger.info("[Update User charConsumed] : {}", userId);
        userService.updateCharConsumed(userId, charConsumed);
    }


    @ResponseBody
    @GetMapping(value="/{userId}/userMembershipInfo")
    public UserMembershipInfoVo getUserMembershipInfo(@PathVariable String userId) throws IOException {
        logger.info("[Get User membership] : {}", userId);
        return userService.getUserMembershipInfo(userId);
    }

//    @ResponseBody
//    @PutMapping(value="/{userId}/membership")
//    public void updateMembership(@PathVariable String userId, @RequestParam("membership") UserMembership membership ) throws IOException {
//        logger.info("[Update User membership] : {}", userId);
//        userService.updateMembership(userId, membership);
//    }

    @ResponseBody
    @GetMapping(value="/{userId}")
    public User getUserInfo(@PathVariable String userId) throws IOException {
        logger.info("[Get User Info] : {}", userId);
        return userService.getUserInfo(userId);
    }
}
