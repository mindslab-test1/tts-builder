package ai.mindslab.ttsmaker.api.login.vo;

import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ReauthReqVo {
    String token;
}
