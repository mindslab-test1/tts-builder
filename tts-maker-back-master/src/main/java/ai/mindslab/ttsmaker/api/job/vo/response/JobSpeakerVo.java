package ai.mindslab.ttsmaker.api.job.vo.response;

import ai.mindslab.ttsmaker.api.job.domain.JobSpeaker;
import lombok.*;
import ai.mindslab.ttsmaker.api.speaker.domain.Speaker;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobSpeakerVo {
	private long id;
	private String jobId;
	private String speakerId;
	private String speakerNm;
	private String voiceName; // TTS VoiceName
	private String imgUrl;
	private String speakerDesc;
	private long orderSeq;
	
	public static JobSpeakerVo of(JobSpeaker entity) {
		Speaker speaker = entity.getSpeaker();
        return JobSpeakerVo.builder()
        		.id(entity.getId())
        		.jobId(entity.getJobId())
        		.speakerId(speaker.getSpeakerId())
        		.speakerNm(speaker.getSpeakerNm())
        		.voiceName(speaker.getVoiceName())
        		.imgUrl(speaker.getImgUrl())
        		.speakerDesc(speaker.getSpeakerDesc())
        		.orderSeq(entity.getOrderSeq())
        		.build();
    }
}
