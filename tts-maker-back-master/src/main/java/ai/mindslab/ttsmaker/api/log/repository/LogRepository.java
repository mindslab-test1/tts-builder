package ai.mindslab.ttsmaker.api.log.repository;

import ai.mindslab.ttsmaker.api.log.domain.Log;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LogRepository extends JpaRepository<Log, Long> {
    @Query("select l from Log l where l.userId = :search or l.code = :search or l.name = :search or l.content like concat('%',:search, '%')")
    List<Log> searchLog(String search);
}
