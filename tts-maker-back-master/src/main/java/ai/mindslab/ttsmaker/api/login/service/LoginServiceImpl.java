package ai.mindslab.ttsmaker.api.login.service;

import ai.mindslab.ttsmaker.api.login.auth.JwtUtils;
import ai.mindslab.ttsmaker.api.login.auth.UserDetailsImpl;
import ai.mindslab.ttsmaker.api.login.domain.MembershipRole;
import ai.mindslab.ttsmaker.api.login.repository.MembershipRoleRepository;
import ai.mindslab.ttsmaker.api.login.repository.UserRepository;
import ai.mindslab.ttsmaker.api.login.vo.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.login.domain.User;
import ai.mindslab.ttsmaker.util.DateUtils;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LoginServiceImpl implements LoginService {

    private final UserRepository userRepository;

    final MembershipRoleRepository membershipRoleRepository;

    final AuthenticationManager authenticationManager;

    final PasswordEncoder encoder;

    final JwtUtils jwtUtils;

    private final RedisTemplate<String, Object> redisTemplate;

    private static Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class.getName());

    public LoginServiceImpl(UserRepository userRepository, MembershipRoleRepository membershipRoleRepository, AuthenticationManager authenticationManager, PasswordEncoder encoder, JwtUtils jwtUtils, RedisTemplate<String, Object> redisTemplate) {
        this.userRepository = userRepository;
        this.membershipRoleRepository = membershipRoleRepository;
        this.authenticationManager = authenticationManager;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
        this.redisTemplate = redisTemplate;
    }

    public ResponseEntity<?> userCheck(UserVo userVo, HttpSession session) {
        logger.info("Authenticating user {}", userVo);
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userVo.getUserId(), userVo.getUserPw()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtUtils.generateJwtToken(authentication);
        logger.info("Token {} generated", jwt);


        UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());


        userVo.setUserNo(userDetails.getId());
        userVo.setProductId(userDetails.getProductId());
        session.setAttribute("accessUser", userVo);

        if (userVo.getToken() != null) {
            deleteTokenByRedis(userVo.getToken());
        }

        setTokenByRedis(jwt, userVo.getUserId(), userVo.getUserPw());

        String pathUrl = "MakeHomeView";

        if (roles.get(0).equals("ROLE_ADMIN")) {
            pathUrl = "AdminView";
        }

        return ResponseEntity.ok(new AuthResponseVo(jwt, userDetails.getId(), userDetails.getUsername(),
                roles.get(0), pathUrl, userVo.getProductId()
                , userDetails.getCharConsumed(), userDetails.getCharLimit()));
    }

    public ResponseEntity<?> reauth(String token) {
        logger.info("Reauthorizing token {}", token);
        RedisTokenVo userInfo = getTokenByRedis(token);

        logger.info("Authenticating user {}", userInfo);

        if (userInfo == null) {
            return ResponseEntity.noContent().build();
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(userInfo.getUserId(), userInfo.getUserPw()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtUtils.generateJwtToken(authentication);
        logger.info("New token generated {}", jwt);

        try {
            setTokenByRedis(jwt, userInfo.getUserId(), userInfo.getUserPw());
            deleteTokenByRedis(token);
        } catch (Exception e) {
            throw e;
        }

        User user = userRepository.findByUserId(userInfo.getUserId())
                .orElseThrow(() -> new EntityNotFoundException("토큰 재인증 시도 중 유저 정보 조회에 실패하였습니다."));

        return ResponseEntity.ok(new AuthResponseVo(jwt, user.getId(), user.getUserId()
                , user.getMembershipRole().getName().toString(), ""
                , user.getProductId(), user.getCharConsumed(), user.getCharLimit()));
    }

    public ResponseEntity<?> logout(String token) {
        logger.info("Logging Out token {}", token);
        RedisTokenVo userInfo = getTokenByRedis(token);

        try {
            deleteTokenByRedis(token);
        } catch (Exception e) {
            throw e;
        }

        return ResponseEntity.ok(new ReauthRespVo(token, userInfo.getUserId()));
    }

    private void setTokenByRedis(String token, String userId, String userPw) {
        try {
            String key = "ttsTool:token:" + token;

            logger.info("Setting token {} for user {}", token, userId);
            redisTemplate.opsForValue().set(key, new RedisTokenVo(userId, userPw));
            redisTemplate.expireAt(key, DateUtils.asDate(DateUtils.localDateTimeNow().plusMinutes(30)));

        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0006", "TokenReqVo 업데이트 처리중 오류가 발생했습니다.");
        }
    }

    private void deleteTokenByRedis(String token) {
        try {
            String key = "ttsTool:token:" + token;

            logger.info("Deleting token {}", token);
            redisTemplate.delete(key);
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0006", "TokenReqVo 삭제 처리중 오류가 발생했습니다.");
        }
    }


    private RedisTokenVo getTokenByRedis(String token) {
        try {
            String key = "ttsTool:token:" + token;

            logger.info("Getting token  {}", token);
            return (RedisTokenVo) redisTemplate.opsForValue().get(key);
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0007", "TokenReqVo 조회 처리중 오류가 발생했습니다.");
        }
    }


    public boolean idCheck(String id) {
        logger.info("Checking userId {}", id);
        if (userRepository.getUserId(id) == null) {
            return true;
        } else {
            return false;
        }
    }

    @Transactional
    public ResponseEntity registUser(User user) {
        logger.info("Registering user {}", user);

        if (userRepository.existsByUserId(user.getUserId())) {
            logger.info("Username taken");
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        // Create new user's account
        user.setConfirm(2);
        user.setAuth(2);
        user.setCharLimit(3000);
        user.setCharConsumed(0);
        user.setProductId(4); // Free 회원 = PRODUCT_ID가 4
        user.setCreateDate(DateUtils.localDateTimeNow());
        user.setUserPw(encoder.encode(user.getUserPw()));
        MembershipRole membershipRole = membershipRoleRepository.findByName(MembershipRoleVo.ROLE_FREE).orElseThrow(
                () -> new EntityNotFoundException("신규 유저 등록중 사용자 권한 조회에 실패하였습니다."));
        user.setMembershipRole(membershipRole);


        MembershipRole userMembershipRole = membershipRoleRepository.findByName(MembershipRoleVo.ROLE_FREE)
                .orElseThrow(() -> new EntityNotFoundException("신규 유저 등록중 사용자 권한 조회에 실패하였습니다."));
        user.setMembershipRole(userMembershipRole);

        logger.info("Saving user in DB ==> {}", user);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }

    public List<User> waitingUser() {
        List<User> userList = userRepository.waitingUser();
        return userList;
    }

    public String registConfirm(User user) {
        try {
            user.setConfirm(2);
            userRepository.save(user);
        } catch (Exception e) {
            System.out.print(e);
            return "FAIL";
        }

        return "SUCCESS";
    }

    public String registReject(User user) {

        try {
            User userInfo = userRepository.getUserInfo(user.getUserId());
            userRepository.delete(userInfo);
        } catch (Exception e) {
            e.printStackTrace();
            ;
            return "FAIL";
        }
        return "SUCCESS";
    }

}
