package ai.mindslab.ttsmaker.api.payment.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "PRODUCT")
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private int id;

    @Column(name = "NAME", columnDefinition = "varchar(50)")
    private String name;

    @Column(name = "PRICE", columnDefinition = "int(11)")
    private Long price;

    @Column(name = "PRICE_US", columnDefinition = "int(11)")
    private Long priceUs;

    @Column(name = "_LIMIT", columnDefinition = "int(11)")
    private Long limit;

    @Column(name = "ACTIVE", columnDefinition = "int(1)")
    private int active;

    //등록일시
    @Column(name = "CREATE_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createDate;

    //수정일시
    @Column(name = "UPDATE_DATE")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updateDate;

    @OneToMany(mappedBy="product")
    private List<Billing> billingList;
}
