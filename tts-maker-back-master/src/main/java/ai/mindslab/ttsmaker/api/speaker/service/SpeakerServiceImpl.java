package ai.mindslab.ttsmaker.api.speaker.service;

import ai.mindslab.ttsmaker.api.speaker.repository.SpeakerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.speaker.domain.Speaker;
import ai.mindslab.ttsmaker.api.speaker.vo.SpeakerVo;
import ai.mindslab.ttsmaker.util.DateUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class SpeakerServiceImpl implements SpeakerService {

    private final SpeakerRepository speakerRepository;

    private final RedisTemplate<String, Object> redisTemplate;

    private static Logger logger = LoggerFactory.getLogger(SpeakerServiceImpl.class.getName());

    public SpeakerServiceImpl(SpeakerRepository speakerRepository, RedisTemplate<String, Object> redisTemplate) {
        this.speakerRepository = speakerRepository;
        this.redisTemplate = redisTemplate;
    }

    @Override
    public List<SpeakerVo> getAllList() {
        try {
            logger.info("Getting list of all speakers");
            List<Speaker> list = speakerRepository.findActiveList();
            return list.stream().map(SpeakerVo::of).collect(Collectors.toList());
        } catch (Exception ex) {
            logger.error("{} ERRS0001 - 화자 리스트 조회중 오류가 발생했습니다.", HttpStatus.INTERNAL_SERVER_ERROR);
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0001", "화자 리스트 조회중 오류가 발생했습니다.");
        }
    }

    public List<Speaker> allEntities() {
        try {
            logger.info("Getting list of all speakers DB entities");
            return speakerRepository.findActiveList();
        } catch (Exception ex) {
            logger.error("{} ERRS0002 - 화자 리스트 조회중 오류가 발생했습니다.", HttpStatus.INTERNAL_SERVER_ERROR);
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0002", "화자 리스트 조회중 오류가 발생했습니다.");
        }
    }

    @Override
    public SpeakerVo getRandomSpeaker() {
        try {
            SpeakerVo speakerVo = getRandomSpeakerTagetByRedis();
            if (speakerVo == null) {
                logger.info("Getting random speakers from DB");
                List<Speaker> allEntities = speakerRepository.findActiveList();

                int count = allEntities.size();
                int rnd = new Random().nextInt(count);

                speakerVo = SpeakerVo.of(allEntities.get(rnd));

                setRandomSpeakerTagetByRedis(speakerVo);
            }

            return speakerVo;
        } catch (TTSMakerApiException ex) {
            throw ex;
        } catch (Exception ex) {
            logger.error("{} ERRS0003 - 랜덤 화자 조회중 오류가 발생했습니다.", HttpStatus.INTERNAL_SERVER_ERROR);
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0003", "랜덤 화자 조회중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * 기본 화자 SpeakerVo 저장(Redis)
     * </pre>
     *
     * @param speakerVo
     */
    private void setRandomSpeakerTagetByRedis(SpeakerVo speakerVo) {
        logger.info("Setting random speakers to cache");
        try {
            String key = "default:randomSpeaker";
            redisTemplate.opsForValue().set(key, speakerVo);
            redisTemplate.expireAt(key, DateUtils.asDate(DateUtils.localDateTimeNow().plusDays(1)));
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0004", "JobScriptSaveVo 업데이트 처리중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * 기본 화자 SpeakerVo 불러오기(Redis)
     * </pre>
     *
     * @return
     */
    private SpeakerVo getRandomSpeakerTagetByRedis() {
        logger.info("Getting random speakers from cache");

        try {
            String key = "default:randomSpeaker";
            return (SpeakerVo) redisTemplate.opsForValue().get(key);
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRS0005", "JobScriptSaveVo 업데이트 처리중 오류가 발생했습니다.");
        }
    }
}
