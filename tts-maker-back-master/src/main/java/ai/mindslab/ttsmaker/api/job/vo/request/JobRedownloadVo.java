package ai.mindslab.ttsmaker.api.job.vo.request;

import lombok.*;

import java.util.ArrayList;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobRedownloadVo {
    private Long id;
    private String jobTitle;
    private String userId;
    private ArrayList<String> urlArray;
    private String url;
    private int scriptsCount;

}