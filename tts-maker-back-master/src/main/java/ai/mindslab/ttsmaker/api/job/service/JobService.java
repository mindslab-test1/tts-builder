package ai.mindslab.ttsmaker.api.job.service;

import ai.mindslab.ttsmaker.api.job.vo.request.JobContentSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.request.JobRedownloadVo;
import ai.mindslab.ttsmaker.api.job.vo.request.JobScriptSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.response.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface JobService {
	/**
	 * <pre>
	 * JOB List 조회
	 * </pre>
	 * @return
	 */
	public List<JobListVo> getJobList();

	/**
	 * <pre>
	 * 각 User ID마다 JOB List 조회
	 * </pre>
	 *
	 * @return
	 */
	public List<JobListVo> getJobListByUserId(String userId);
	
	/**
	 * <pre>
	 * JOB ID별 선택한 화자 및 스크립트 로드
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public JobContentVo getJobContent(String jobId);
	
	/**
	 * <pre>
	 * Job ID별 선택한 스피커(화자) List 조회
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public List<JobSpeakerVo> getJobSpeakerList(String jobId);
	
	/**
	 * <pre>
	 * Job ID별 스크립트 LIST 조회
	 * </pre>
	 * @param jobId
	 * @return
	 */
	public List<JobScriptVo> getJobScriptList(String jobId);
	
	/**
	 * <pre>
	 * script 조회(Job ID + Script ID)
	 * </pre>
	 * @param jobId
	 * @param scriptId
	 * @return
	 */
	public JobScriptVo getJobScript(String jobId, String scriptId);

	/**
	 * <pre>
	 * 스크립트(JobScriptSaveVo) 저장(Redis)
	 * </pre>
	 * @param jobId
	 * @param scriptId
	 * @param saveVo
	 * @return
	 */
	public boolean setJobScriptsSaveVoTagetByRedis(String jobId, String scriptId, JobScriptSaveVo saveVo);
	
	/**
	 * <pre>
	 * 선택한 화자 List(JobSpeakerSaveVo) 저장(Redis)
	 * </pre>
	 * @param jobId
	 * @param jobSpeakerIdList
	 * @return
	 */
	public boolean setJobSpeakerSaveVosTagetByRedis(String jobId, List<String> jobSpeakerIdList);

	/**
	 * <pre>
	 * JOB 저장
	 * </pre>
	 * @param jobId
	 * @param jobContentSaveVo
	 * @return
	 */
	public boolean saveAsJobContent(String jobId, JobContentSaveVo jobContentSaveVo);

	boolean saveAsJobContentOverwrite(String jobId, String newJobTitle, String newJobId, JobContentSaveVo jobContentSaveVo);

	public boolean saveAsJobContentNew(String jobId, JobContentSaveVo jobContentSaveVo);

    /**
     * <pre>
     * 해상 openApi 정보 가져옴
     * </pre>
     *
     * @return
     */
    public String getSeaFcst();

	// Download
	public void downloadTTS(JobContentSaveVo jobContentSaveVo, HttpServletResponse response) throws IOException;
	public byte[] downloadEachTTS(JobContentSaveVo jobContentSaveVo) throws IOException;
	public void redownloadTTS(JobRedownloadVo jobRedownloadVo, HttpServletResponse response) throws IOException;
	public  byte[] redownloadEachTTS(JobRedownloadVo jobRedownloadVo) throws IOException;

	// Download Info
	/**
	 * <pre>
	 * 사용자 Download 기록
	 * </pre>
	 * @param userId
	 * @return
	 */
	public List<UserDownloadInfoVo> getUserDownloadInfo(String userId);

	// Send
	public Object sendTTStoDMZ(JobContentSaveVo jobContentSaveVo) throws IOException;
	public Object checkItemCode(JobContentSaveVo jobContentSaveVo);
}
