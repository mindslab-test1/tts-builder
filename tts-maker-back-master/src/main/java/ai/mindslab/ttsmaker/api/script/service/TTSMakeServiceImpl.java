package ai.mindslab.ttsmaker.api.script.service;

import ai.mindslab.ttsmaker.api.script.domain.dto.response.TTSMakeResultVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import reactor.core.publisher.Mono;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.service.TTSApiService;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIResponseFile;

import java.util.Random;
import java.util.UUID;

@Service
public class TTSMakeServiceImpl implements TTSMakeService {

    private final TTSApiService maumAiApiService;

    private static Logger logger = LoggerFactory.getLogger(TTSMakeServiceImpl.class.getName());

    public TTSMakeServiceImpl(TTSApiService maumAiApiService) {
        this.maumAiApiService = maumAiApiService;
    }

    public Mono<TTSMakeResultVo> requestTTSMakeFile(String voiceName, String text, String dummyFileName, String downloadId) {
        if (StringUtils.hasText(dummyFileName)) {
            return fakeRequestTTSMakeFile(voiceName, text, dummyFileName);
        }

        logger.info("Requesting TTS Make File");
        Mono<MaumAIResponseFile> result = maumAiApiService.requestTTSMakeFile(voiceName, text, downloadId);

        Mono<TTSMakeResultVo> convertResult = result.map(data -> {
                    TTSMakeResultVo ttsMakeResult = TTSMakeResultVo.of(voiceName, text, data);
                    logger.info("TTS Make File: {}", ttsMakeResult);
                    return ttsMakeResult;
                }
        );

        return convertResult;
    }

    public Mono<TTSMakeResultVo> fakeRequestTTSMakeFile(String voiceName, String text, String dummyFileName) {
        TTSMakeResultVo result = TTSMakeResultVo.builder()
                .requestId(UUID.randomUUID().toString())
                .voiceName(voiceName)
                .text(text)
                .statusCd(200)
                .statusNm("정상")
                .downloadURL(String.format("/static/voice/%s", dummyFileName))
                .expiryDate("2019-12-31")
                .build();

        Random rnd = new Random();
        int sleep = rnd.nextInt(30) * 100;
        try {
            Thread.sleep(sleep);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return Mono.just(result);
    }
}
