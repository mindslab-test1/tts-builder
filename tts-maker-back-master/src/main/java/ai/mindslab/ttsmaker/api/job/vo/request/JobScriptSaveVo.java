package ai.mindslab.ttsmaker.api.job.vo.request;

import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import ai.mindslab.ttsmaker.api.job.vo.response.JobSettingsVo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import ai.mindslab.ttsmaker.api.speaker.domain.Speaker;

import java.util.Optional;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobScriptSaveVo {
	private String jobId;					// JOB ID
	private String scriptId;				// 스크립트 고유 번호
	private String speakerId;				// 화자 ID
	private String voiceName;
	private String scriptText;				// 스크립트 텍스
	private String ttsResourceUrl;			// TTS Resource URL
	private String ttsResourceExpiryDate;	// TTS Resource 만료
	private JobSettingsVo settings;			// 음성제어 정보
	private String ttsState;				// TTS 변환 상태
	private String rowState;				// Row 상태
	
	@JsonIgnore
	public static JobScriptSaveVo of(JobScript entity) {
		Speaker speaker = Optional.ofNullable(entity.getSpeaker()).orElse(new Speaker());
		return JobScriptSaveVo.builder()
        		.jobId(entity.getJobId())
        		.scriptId(entity.getScriptId())
        		.speakerId(speaker.getSpeakerId())
				.voiceName(speaker.getVoiceName())
        		.scriptText(entity.getScriptText())
        		.ttsResourceUrl(entity.getTtsResourceUrl())
        		.ttsResourceExpiryDate(entity.getTtsResourceExpiryDate())
        		.settings(JobSettingsVo.of(entity))
        		.ttsState(entity.getTtsState())
        		.rowState(entity.getRowState())
        		.build();
    }
}
