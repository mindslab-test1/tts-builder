package ai.mindslab.ttsmaker.api.log.service;

import ai.mindslab.ttsmaker.api.log.domain.Log;

import java.util.List;

public interface LogService {
    public void saveLog(Log log);

    public List<Log> getAllLog();

    public List<Log> searchLog(String search);
}
