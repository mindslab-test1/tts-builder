package ai.mindslab.ttsmaker.api.login.service;

import ai.mindslab.ttsmaker.api.login.domain.MembershipRole;
import ai.mindslab.ttsmaker.api.login.repository.UserRepository;
import ai.mindslab.ttsmaker.api.login.vo.UserMembershipInfoVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ai.mindslab.ttsmaker.api.login.domain.User;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    private static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class.getName());

    final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public int getCharConsumed(String userId) {
        return userRepository.getCharConsumedbyUserId(userId);
    }

    @Transactional
    @Override
    public void updateCharConsumed(String userId, int charConsumed) {
        logger.info("Adding {} charConsumed ==> {}", userId, charConsumed);
        int newCharConsumed = getCharConsumed(userId) + charConsumed;
        userRepository.updateUserCharConsumed(userId, newCharConsumed);
    }

    @Override
    public UserMembershipInfoVo getUserMembershipInfo(String userId) {
        logger.info("Getting {} membershipInfo", userId);
        UserMembershipInfoVo userMembershipInfoVo = new UserMembershipInfoVo();
        User user = userRepository.getUserMembershipInfo(userId);
        try {
            userMembershipInfoVo = UserMembershipInfoVo.of(user);
        } catch (Exception e) {
            logger.error("Error: getUserMembershipInfo in UserServiceImpl  ==> {}", e.getMessage());
        }
        return userMembershipInfoVo;
    }

    @Override
    public User getUserInfo(String userId) {
        return userRepository.findByUserId(userId).orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + userId));
    }

    @Override
    public void increaseUserPayCount(String userId, Long payCount) {
        userRepository.increaseUserPayCount(userId, payCount);
    }

    @Override
    public List<User> findAllByProductId(int i) {
        return userRepository.findAllByProductId(i);
    }

    @Override
    public int updateUserProduct(Long id, int prodId) {
        return userRepository.updateUserProduct(id, prodId);
    }

    @Override
    public int updateUserMembershipRole(String userEmail, MembershipRole membershipRole) {
        return userRepository.updateUserMembershipRole(userEmail, membershipRole);
    }

    @Override
    public Long getUserIdByUserEmail(String userId) {
        return userRepository.getUserIdByEmail(userId);
    }

}
