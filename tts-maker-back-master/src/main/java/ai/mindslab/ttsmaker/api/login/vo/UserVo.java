package ai.mindslab.ttsmaker.api.login.vo;

import lombok.*;

import java.io.Serializable;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserVo implements Serializable {
    private Long userNo;
    private String userId;
    private String userPw;
    private String membership;
    private String token;
    private int productId;
}
