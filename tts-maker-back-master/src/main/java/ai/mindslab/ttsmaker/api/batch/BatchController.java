package ai.mindslab.ttsmaker.api.batch;

import ai.mindslab.ttsmaker.util.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import ai.mindslab.ttsmaker.api.login.domain.MembershipRole;
import ai.mindslab.ttsmaker.api.login.domain.User;
import ai.mindslab.ttsmaker.api.login.repository.MembershipRoleRepository;
import ai.mindslab.ttsmaker.api.login.service.UserService;
import ai.mindslab.ttsmaker.api.login.vo.MembershipRoleVo;
import ai.mindslab.ttsmaker.api.payment.service.PaymentService;
import ai.mindslab.ttsmaker.api.payment.vo.PaymentVo;

import javax.persistence.EntityNotFoundException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;

@Controller
public class BatchController {
    @Value("${batch.status}")
    private String mBatchStatus;

    @Value("${batch.alive_check}")
    private int mFlag_AliveCheck;

    private final PaymentService paymentService;

    private final UserService userService;

    private final MembershipRoleRepository membershipRoleRepository;


    private static final Logger logger = LoggerFactory.getLogger(BatchController.class);

    public BatchController(PaymentService paymentService, UserService userService, MembershipRoleRepository membershipRoleRepository) {
        this.paymentService = paymentService;
        this.userService = userService;
        this.membershipRoleRepository = membershipRoleRepository;
    }

    /*
     * routineBill 주기 : 매일 12시
     * Modified by LYJ - 결제 방법에 따른 분기 추가 - 2019. 10. 19
     */
    @Scheduled(cron = "0 0 12 * * ?")
//    @Scheduled(cron = "*/10 * * * * ?")
    public void routineBill() {
        logger.info("ROUTINE BILL");
        if ("start".equals(mBatchStatus)) {
            logger.info("Batch schedule(routineBill) start!!");

            List<PaymentVo> RegularList = paymentService.getRoutineUserList(DateUtils.localDateNow());

            for (PaymentVo paymentVo : RegularList) {
                try {

                    String paymentMethodStr = paymentService.getPaymentMethod(paymentVo.getUserNo());

                    if ("Card".equalsIgnoreCase(paymentMethodStr)) {
                        logger.info("Starting routine bill for user --> {}", paymentVo.getUserEmail());
                        paymentService.routineBill(paymentVo);
                    }

                } catch (Exception e) {
                    logger.error("Batch schedule(routineBill) Exception!! " + e);
                }
            }
        } else {
            logger.info("Batch schedule(routineBill) stop!!");
        }

    }

    /*
     * Batch 구독 해지 - 주기 : 1시간마다
     * 19.09.25 이예준
     */
    @Scheduled(cron = "0 0 0/1 * * ?")
//    @Scheduled(cron = "*/10 * * * * ?")
    public void batchUnsubscribe() {
        if ("start".equals(mBatchStatus)) {
            logger.info("Batch schedule(batchUnsubscribe) start!!");

            try {
                List<User> unsubscribingUsers = userService.findAllByProductId(5);

                for (User unsubscribingUser : unsubscribingUsers) {
                    logger.info("Unsubscribing user --> {}", unsubscribingUser.getUserId());

                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

                    // 만료 날짜
                    LocalDate expireDate = paymentService.getUserPaymentExpirationDate(unsubscribingUser.getId());

                    // 현재 날짜
                    LocalDate currentDate = DateUtils.localDateNow();

                    logger.info("Expiration date [{}] || Current date [{}] --> user :", expireDate, currentDate, unsubscribingUser.getUserId());
                    //만료 날짜, 현재 날짜 비교 (만료 날짜가 지났을 경우 해지)
                    if (currentDate.compareTo(expireDate) > 0) {
                        userService.updateUserProduct(unsubscribingUser.getId(), 4);

                        MembershipRole membershipRole = membershipRoleRepository.findByName(MembershipRoleVo.ROLE_FREE)
                                .orElseThrow(() -> new EntityNotFoundException());
                        userService.updateUserMembershipRole(unsubscribingUser.getUserId(), membershipRole);

                        paymentService.planCancelProcedure(unsubscribingUser.getId()); // 프로시저를 이용해 정기결제 해지
                        paymentService.deleteBillInfo(unsubscribingUser.getId()); // 사용자의 결제 정보 삭제

                        logger.info("Batch schedule(batchUnsubscribe) - unsubscribe user : " + unsubscribingUser.getUserId());

                    }
                }
            } catch (Exception e) {
                logger.error("Batch schedule(batchUnsubscribe) Exception!!" + e);
            }
        } else {
            logger.info("Batch schedule(batchUnsubscribe) stop!!");
        }
    }

    /*
     * Batch 5일동안 미결제 사용자에 대한 해지 - 주기 : 12시간마다
     * 20.01.30 이예준
     * Modified 20.03.02 by LYJ : 5일이상 미결제한 사용자 분류하는 로직 이슈
     */
    @Scheduled(cron = "0 0 0/12 * * ?")
//    @Scheduled(cron = "*/20 * * * * ?")
    public void batchUnpaymentUser() {
        logger.info("Batch schedule (batchUnpaymentUser) start!!");

        if ("start".equals(mBatchStatus)) {
            try {
                List<PaymentVo> unpaymentUsers = paymentService.getUnpaymentList();

                for (PaymentVo user : unpaymentUsers) {
                    logger.info("Unpaid user --> {}", user.getUserEmail());
                    userService.updateUserProduct(user.getUserNo(), 4);

                    MembershipRole membershipRole = membershipRoleRepository.findByName(MembershipRoleVo.ROLE_FREE)
                            .orElseThrow(() -> new EntityNotFoundException());
                    userService.updateUserMembershipRole(user.getUserEmail(), membershipRole);

                    paymentService.deleteBillInfo(user.getUserNo());

                    logger.info(" @ batchUnpaymentUser : " + user.getUserEmail() + " ==> UNSUBSCRIBED  & deleteBillingInfo");

                }
            } catch (Exception e) {
                logger.error("Batch schedule (batchUnpaymentUser) Exception!!" + e);
            }
        } else {
            logger.info("Batch schedule (batchUnpaymentUser) stop!!");
        }
    }
}