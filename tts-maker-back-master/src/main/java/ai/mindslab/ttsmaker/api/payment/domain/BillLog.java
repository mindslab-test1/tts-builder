package ai.mindslab.ttsmaker.api.payment.domain;

import ai.mindslab.ttsmaker.api.login.domain.User;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "BILL_LOG")
public class BillLog {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private int id;

    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID")
    private User userId;

    @Column(name = "PAYMENT", columnDefinition = "varchar(50)")
    private String payment;

    //    @Column(name = "CODE", columnDefinition = "varchar(10)")
    @OneToOne
    @JoinColumn(name = "CODE", referencedColumnName = "CATEGORY3")
    private MasterCode code;

    @Column(name = "BILL_KEY", columnDefinition = "varchar(200)")
    private String billKey;

    @Column(name = "NUMBER", columnDefinition = "varchar(30)")
    private String number;

    @Column(name = "RESULT", columnDefinition = "varchar(10)")
    private String result;

    @ManyToOne
    @JoinColumn(name = "PRODUCT")
    private Product product;

    @Column(name = "PRICE", columnDefinition = "int(11)")
    private Long price;

    @Column(name = "AC_CODE", columnDefinition = "varchar(100)")
    private String acCode;

    @Column(name = "PAYMENT_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate paymentDate;

    //등록일시
    @Column(name = "DATE_FROM")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate dateFrom;

    //수정일시
    @Column(name = "DATE_TO")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate dateTo;


    @Column(name = "TID", columnDefinition = "varchar(60)")
    private String tid;

    //등록일시
    @Column(name = "CREATE_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createDate;

    //수정일시
    @Column(name = "UPDATE_DATE")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updateDate;

    @Column(name = "CREATE_USER", columnDefinition = "int(8)")
    private Long createUser;


    @OneToOne
    @JoinColumn(name = "BILLING_ID", referencedColumnName = "ID")
    private Billing billing;
}
