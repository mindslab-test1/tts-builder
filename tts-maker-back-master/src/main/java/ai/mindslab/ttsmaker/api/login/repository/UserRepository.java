package ai.mindslab.ttsmaker.api.login.repository;

import ai.mindslab.ttsmaker.api.login.domain.MembershipRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import ai.mindslab.ttsmaker.api.login.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    @Query("select u from User u where u.userId = ?1 and u.userPw = ?2")
    User searchUser(String id, String pw);

    @Query("select u.userId from User u where u.userId = ?1")
    String getUserId(String id);

    @Query("select u from User u where u.confirm = 0")
    List<User> waitingUser();

    @Query("select u from User u where u.userId = ?1")
    User getUserInfo(String id);

    @Query("select u from User u where u.userId = ?1")
    User getUserMembershipInfo(String userid);

    @Query("select u from User u where u.userId = ?1")
    User getUserbyUserId(String userId);

    @Transactional
    @Modifying
    @Query("Update User u set u.charLimit=?2 Where u.userId = ?1")
    int updateUserCharLimit(String userId, int newCharLimit);

    @Transactional
    @Modifying
    @Query("Update User u set u.productId=?2 Where u.id = ?1")
    int updateUserProduct(Long id, int prodId);

    @Query("select u.charConsumed from User u where u.userId = ?1")
    int getCharConsumedbyUserId(String userId);

    @Modifying
    @Query("Update User u set u.charConsumed=?2 Where u.userId = ?1")
    int updateUserCharConsumed(String userId, int newCharConsumed);

    @Transactional
    @Modifying
    @Query("Update User u set u.payCount = u.payCount + ?2 Where u.userId = ?1")
    int increaseUserPayCount(String userId, Long payCount);

    @Query("select u.payCount from User u where u.userId = ?1")
    Integer getPayCountbyUserId(String userId);

    @Transactional
    @Modifying
    @Query("Update User u SET u.membershipRole = ?2 where u.userId = ?1")
    int updateUserMembershipRole(String userid, MembershipRole role);

    Optional<User> findByUserId(String username);

    List<User> findAllByProductId(int productId);

    boolean existsByUserId(String username);

    @Query("select u.membershipRole from User u where u.userId = ?1")
    MembershipRole getUserMembershipRole(String userid);

    @Query("select u.id from User u where u.userId = ?1")
    Long getUserIdByEmail(String userid);

}
