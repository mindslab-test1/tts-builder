package ai.mindslab.ttsmaker.api.login.controller;

import ai.mindslab.ttsmaker.api.login.vo.ReauthReqVo;
import ai.mindslab.ttsmaker.api.login.vo.UserVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ai.mindslab.ttsmaker.api.login.domain.User;
import ai.mindslab.ttsmaker.api.login.service.LoginService;

import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
@RequestMapping("/login")
public class LoginController {

    final LoginService loginService;

    private static Logger logger = LoggerFactory.getLogger(LoginController.class.getName());

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @ResponseBody
    @PostMapping(value = "/userCheck")
    public ResponseEntity<?> userCheck(@RequestBody UserVo userVo, HttpSession session) throws IOException{
        logger.info("[Logging In] : {}", userVo);

        return loginService.userCheck(userVo, session);
    }

    @ResponseBody
    @PostMapping(value = "/reauth")
    public ResponseEntity<?> reauth(@RequestBody ReauthReqVo reauthReqVo) throws IOException{
        logger.info("[Reauthorizing] : {}", reauthReqVo);


        return loginService.reauth(reauthReqVo.getToken());
    }

    @ResponseBody
    @PostMapping(value = "/logout")
    public ResponseEntity<?> logout(@RequestBody UserVo userVo) throws IOException{
        logger.info("[Logging Out] : {}", userVo);

        return loginService.logout(userVo.getToken());
    }

    @ResponseBody
    @PostMapping(value = "/idCheck")
    public boolean idCheck(@RequestBody UserVo userVo) throws IOException {
        logger.info("[User Id Check] : {}", userVo);

        return loginService.idCheck(userVo.getUserId());
    }

    @ResponseBody
    @PostMapping(value = "/regist")
    public ResponseEntity registUser(@RequestBody User user) throws IOException {
        logger.info("[Registering User] : {}", user);

        return loginService.registUser(user);
    }

//    @ResponseBody
//    @PostMapping(value = "/waitingUser")
//    public List<User> waitingUser() throws IOException {
//        return loginService.waitingUser();
//    }
//
//    @ResponseBody
//    @PostMapping(value = "/registConfirm")
//    public String registConfirm(@RequestBody User user) throws IOException {
//
//        return loginService.registConfirm(user);
//    }
//
//    @ResponseBody
//    @PostMapping(value = "/registReject")
//    public String registReject(@RequestBody User user) throws IOException {
//
//        return loginService.registReject(user);
//    }
}
