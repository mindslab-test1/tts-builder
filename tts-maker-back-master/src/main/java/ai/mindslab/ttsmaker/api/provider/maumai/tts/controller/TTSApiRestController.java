package ai.mindslab.ttsmaker.api.provider.maumai.tts.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.service.TTSApiService;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIResponseFile;

import java.io.IOException;

@RestController
@RequestMapping("/maumAi/tts")
public class TTSApiRestController {

	private final TTSApiService maumAiApiService;

	private static Logger logger = LoggerFactory.getLogger(TTSApiRestController.class.getName());

	public TTSApiRestController(TTSApiService maumAiApiService) {
		this.maumAiApiService = maumAiApiService;
	}

	@ResponseBody
	@PostMapping(value = "/requestMakeFile/{voiceName}")
	public Mono<MaumAIResponseFile> requestMakeFile(@PathVariable String voiceName, @RequestParam String text) throws IOException, InterruptedException {
		logger.info("[Make TTS File] : {}, {}", voiceName, text);
		return maumAiApiService.requestTTSMakeFile(voiceName, text, null);
	}
}
