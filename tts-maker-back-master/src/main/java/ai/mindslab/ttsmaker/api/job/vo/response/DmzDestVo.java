package ai.mindslab.ttsmaker.api.job.vo.response;

import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DmzDestVo {
    private String kind;
    private String channel;
    private String date;
    private String categoryCode;
    private String categoryId;
    private String title;
    private String itemCode;
}