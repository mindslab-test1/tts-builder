package ai.mindslab.ttsmaker.api.Fcst.domain;

import lombok.Data;

@Data
public class LandFcstItem {
    public String announceTime;
    public String numEf;
    public String regId;
    public String rnSt;
    public String rnYn;
    public String ta;
    public String wd1;
    public String wd2;
    public String wdTnd;
    public String wf;
    public String wfCd;
    public String wsIt;
}
