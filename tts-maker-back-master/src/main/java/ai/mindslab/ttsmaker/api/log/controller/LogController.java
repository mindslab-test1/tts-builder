package ai.mindslab.ttsmaker.api.log.controller;

import ai.mindslab.ttsmaker.api.log.domain.Log;
import ai.mindslab.ttsmaker.api.log.service.LogService;
import ai.mindslab.ttsmaker.api.log.vo.LogVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/log")
public class LogController {

    final LogService logService;

    public LogController(LogService logService) {
        this.logService = logService;
    }

    @ResponseBody
    @GetMapping(value = "/getAllLog")
    public List<Log> getAllLog() {

        return logService.getAllLog();
    }

    @ResponseBody
    @PostMapping(value = "/searchLog")
    public List<Log> getSearchLog(@RequestBody LogVo logVo) {

        System.out.println("search : " + logVo.getSearch());

        return logService.searchLog(logVo.getSearch());
    }
}
