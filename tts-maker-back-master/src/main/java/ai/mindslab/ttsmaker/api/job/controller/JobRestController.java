package ai.mindslab.ttsmaker.api.job.controller;

import ai.mindslab.ttsmaker.api.job.vo.request.JobContentSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.request.JobRedownloadVo;
import ai.mindslab.ttsmaker.api.job.vo.request.JobScriptSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.response.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;
import ai.mindslab.ttsmaker.api.job.service.JobService;
import ai.mindslab.ttsmaker.api.login.service.UserService;
import ai.mindslab.ttsmaker.api.script.domain.dto.response.TTSMakeResultVo;
import ai.mindslab.ttsmaker.api.script.service.TTSMakeService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;


@RestController
@RequestMapping("/job")
public class JobRestController {
	private static Logger logger = LoggerFactory.getLogger(JobRestController.class.getName());

	@Value("${KBS_OPENAPI}")
	private String KBS_OPENAPI;

	@Value("${DMZ_SERVER}")
	private String DMZ_SERVER;

	@Value("${OPEN_API}")
	private String OPEN_API;

	private final JobService jobService;

	private final TTSMakeService ttsService;

	private final UserService userService;

	public JobRestController(JobService jobService, TTSMakeService ttsService, UserService userService) {
		this.jobService = jobService;
		this.ttsService = ttsService;
		this.userService = userService;
	}

	@ResponseBody
	@GetMapping(value = "/list")
	public List<JobListVo> list() throws IOException {
		return jobService.getJobList();
	}

	@ResponseBody
	@GetMapping(value = "/listByUserId/{userId}")
	public List<JobListVo> listByUserId(@PathVariable String userId) throws IOException {
		logger.info("[Listing Job By User] : {}", userId);
		return jobService.getJobListByUserId(userId);
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}")
	public JobContentVo getJobContent(@PathVariable String jobId) throws IOException {
		logger.info("[Getting Job Content] : {}", jobId);
		return jobService.getJobContent(jobId);
	}
	
	@ResponseBody
	@PutMapping(value = "/{jobId}")
	public boolean saveAsJobContent(@PathVariable String jobId, @RequestBody JobContentSaveVo jobContentSaveVo) {
		logger.info("[Saving Job Content] : {}", jobId, jobContentSaveVo);
		return jobService.saveAsJobContent(jobId, jobContentSaveVo);
	}

	@ResponseBody
	@PutMapping(value = "/{jobId}/new")
	public boolean saveAsJobContentNew(@PathVariable String jobId, @RequestBody JobContentSaveVo jobContentSaveVo) {
		logger.info("[Saving Job Content] : {}", jobId, jobContentSaveVo);
		return jobService.saveAsJobContentNew(jobId, jobContentSaveVo);
	}

	@ResponseBody
	@PutMapping(value = "/{jobId}/{newJobTitle}/{newJobId}/overwrite")
	public boolean saveAsJobContentOverwrite(@PathVariable String jobId, @PathVariable String newJobTitle,
											 @PathVariable String newJobId, @RequestBody JobContentSaveVo jobContentSaveVo) {
		logger.info("[Saving Job Content] : {}", jobId, jobContentSaveVo);
		return jobService.saveAsJobContentOverwrite(jobId, newJobTitle, newJobId, jobContentSaveVo);
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}/speaker")
	public List<JobSpeakerVo> jobSpeakerList(@PathVariable String jobId) throws IOException {
		logger.info("[Listing Job Speaker] : {}", jobId);
		return jobService.getJobSpeakerList(jobId);
	}
		
	@ResponseBody
	@PutMapping(value = "/{jobId}/speaker")
	public boolean setJobSpeakerSaveVosTagetByRedis(@PathVariable String jobId, @RequestBody List<String> jobSpeakerIdList) {
		logger.info("[Saving Job Speaker] : {}", jobId);
		return jobService.setJobSpeakerSaveVosTagetByRedis(jobId, jobSpeakerIdList);
	}
	
	@ResponseBody
	@GetMapping(value = "/{jobId}/script")
	public List<JobScriptVo> jobScriptList(@PathVariable String jobId) throws IOException {
		logger.info("[Listing Job Script] : {}", jobId);

		return jobService.getJobScriptList(jobId);
	}
	
	@ResponseBody
	@PutMapping(value = "/{jobId}/script/{scriptId}")
	public boolean setJobScriptsSaveVoTagetByRedis(@PathVariable String jobId, @PathVariable String scriptId, @RequestBody JobScriptSaveVo saveVo) {
		logger.info("[Setting Job Script] : {}, {}", jobId, scriptId);
		return jobService.setJobScriptsSaveVoTagetByRedis(jobId, scriptId, saveVo);
	}
	
	@ResponseBody
	@PostMapping(value = "/toMakeTTS/{voiceName}")
	public Mono<TTSMakeResultVo> requestTTSMakeFile(@PathVariable String voiceName, @RequestParam String text, @RequestParam(required = false) String dummyFileName) throws IOException, InterruptedException {
		logger.info("[Requesting TTSMakeFile] : {}, {}, {}", voiceName, text, dummyFileName);
		return ttsService.requestTTSMakeFile(voiceName, text, dummyFileName, null);
	}

	@ResponseBody
	@PostMapping(value = "/sendTTS")
	public Object sendTTStoDMZ(@RequestBody JobContentSaveVo jobContentSaveVo) throws IOException {
		logger.info("[Sending TTS to DMZ] : {}", jobContentSaveVo.getUserId());
		return jobService.sendTTStoDMZ(jobContentSaveVo);
	}

	@ResponseBody
	@PostMapping(value = "/checkItemCode")
	public Object checkItemCode(@RequestBody JobContentSaveVo jobContentSaveVo) {
		logger.info("[Checking Item Code] : {}", jobContentSaveVo.getUserId());

		return jobService.checkItemCode(jobContentSaveVo);
	}

	@ResponseBody
	@PostMapping(value = "/download/redownloadTTS")
	public void redownloadTTS(@RequestBody JobRedownloadVo jobRedownloadVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.info("[Redownloading Combined TTS File] : {}", jobRedownloadVo.getUserId());
		jobService.redownloadTTS(jobRedownloadVo, response);
		int textLength = jobRedownloadVo.getScriptsCount();
	}
	@ResponseBody
	@PostMapping(value = "/download/redownloadEachTTS")
	public byte[] redownloadEachTTS(@RequestBody JobRedownloadVo jobRedownloadVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.info("[Redownloading Each TTS File] : {}", jobRedownloadVo.getUserId());
		int textLength = jobRedownloadVo.getScriptsCount();

		return jobService.redownloadEachTTS(jobRedownloadVo);
	}

	@ResponseBody
	@PostMapping(value = "/download/downloadTTS")
	public void downloadTTS(@RequestBody JobContentSaveVo jobContentSaveVo, HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.info("[Downloading Combined TTS File] : {}", jobContentSaveVo.getUserId());

		jobService.downloadTTS(jobContentSaveVo, response);

		int textLength = jobContentSaveVo.getScripts().stream().mapToInt(script -> script.getScriptText().replaceAll("\\s","").length()).sum();
		userService.updateCharConsumed(jobContentSaveVo.getUserId(),textLength);
	}

	@ResponseBody
	@PostMapping(value = "/download/downloadEachTTS")
	public byte[] downloadEachTTS(@RequestBody JobContentSaveVo jobContentSaveVo, HttpServletRequest request, HttpServletResponse response) throws IOException, URISyntaxException {
		logger.info("[Downloading Each TTS File] : {}", jobContentSaveVo.getUserId());

		int textLength = jobContentSaveVo.getScripts().stream().mapToInt(script -> script.getScriptText().replaceAll("\\s","").length()).sum();
		userService.updateCharConsumed(jobContentSaveVo.getUserId(),textLength);

		return jobService.downloadEachTTS(jobContentSaveVo);
	}


	@ResponseBody
	@GetMapping(value = "/{userId}/downloadInfo")
	public List<UserDownloadInfoVo> getUserDownloadInfo(@PathVariable String userId) throws IOException {
		logger.info("[Getting Download Info] : {}", userId);
		return jobService.getUserDownloadInfo(userId);
	}



	@ResponseBody
    @GetMapping(value = "/meteorological")
    public String getMeteorologicalApi() {
		logger.info("[Getting Meteorological API]");
		return jobService.getSeaFcst();
    }
}
