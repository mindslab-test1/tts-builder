package ai.mindslab.ttsmaker.api.login.repository;

import ai.mindslab.ttsmaker.api.login.domain.MembershipRole;
import ai.mindslab.ttsmaker.api.login.vo.MembershipRoleVo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MembershipRoleRepository extends JpaRepository<MembershipRole, Long> {
    Optional<MembershipRole> findByName(MembershipRoleVo name);
}