package ai.mindslab.ttsmaker.api.login.auth;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ai.mindslab.ttsmaker.api.login.domain.User;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

public class UserDetailsImpl implements UserDetails {
    private static final long serialVersionUID = 1L;

    private Long id;

    private String username;

    @JsonIgnore
    private String password;

    private Collection<? extends GrantedAuthority> authorities;

    private int productId;

    private int charConsumed;

    private int charLimit;

    public UserDetailsImpl(Long id, String username, String password,
                           Collection<? extends GrantedAuthority> authorities, int productId, int charConsumed, int charLimit) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.authorities = authorities;
        this.productId = productId;
        this.charConsumed = charConsumed;
        this.charLimit = charLimit;
    }

    public static UserDetailsImpl build(User user) {
        List<GrantedAuthority> authorities = Arrays.asList(new SimpleGrantedAuthority(
                        user.getMembershipRole().getName().toString()
                )
        );

        return new UserDetailsImpl(
                user.getId(),
                user.getUserId(),
                user.getUserPw(),
                authorities,
                user.getProductId(),
                user.getCharConsumed(),
                user.getCharLimit()
        );
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        UserDetailsImpl user = (UserDetailsImpl) o;
        return Objects.equals(id, user.id);
    }

    public int getProductId() {
        return this.productId;
    }

    public int getCharConsumed() {
        return this.charConsumed;
    }

    public int getCharLimit() {
        return this.charLimit;
    }
}