package ai.mindslab.ttsmaker.api.payment.vo;

import lombok.Data;

@Data  /* Billlig_T와 Billing_T의 정보를 다루기 위한 VO */
public class BillingVo {
    private Long userNo;             // 사용자 정보
    private String payment;         // 결제 수단
    private String issuer;          // 금융사 코드
    private String billKey;         // 빌링키
    private String number;          // 결제 수단 번호
    private String result;          // 결과 코드
    private int product;            // 상품 정보
    private Long price;              // 결제 금액
    private String acCode;          // 승인 코드
    private String pDate;           // 결제 일자
    private String SdateFrom;       // 서비스 시작일
    private String SdateTo;         // 서비스 종료일 ==> 다음결제일(paymentDate)과 동일한 날짜
    private String tid;             // 이니시스 결제 취소에 사용되는 정보
    private String createDate;      // 데이터 생성일
    private int createUser;         // 데이터 생성자

    private String paymentDate;     // 다음 결제일
    private int active;             // 활성화 여부
    private String updateDate;      // 데이터 변경일
    private int updateUser;         // 데이터 변경자
    private String failReason;      // 실패 이유
    private String description;
}