package ai.mindslab.ttsmaker.api.job.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Table(name = "JOB_M", uniqueConstraints = {@UniqueConstraint(name = "UK_JOB_ID", columnNames = {"JOB_ID", "USERID"})})
public class JobM implements Serializable {
	
	private static final long serialVersionUID = 4111328443959601779L;

	//관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private long id;

    //저장한 유저 정보
    @Column(name = "USERID", columnDefinition = "varchar(100)")
    private String userId;

    //JOB ID
    @Column(name = "JOB_ID", columnDefinition = "varchar(36)")
    private String jobId;
   
    //JOB_TITLE
    @Column(name = "JOB_TITLE", columnDefinition = "varchar(255)")
    private String jobTitle;
    
    //정렬순서
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_SEQ", columnDefinition = "int(11)")
    private long orderSeq;
    
    @Getter
    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name = "JOB_ID",  referencedColumnName = "JOB_ID")
    private List<JobSpeaker> jobSpeakers; 
    
    @Getter
    @LazyCollection(LazyCollectionOption.FALSE)  
    @OneToMany(fetch=FetchType.LAZY)
    @JoinColumn(name = "JOB_ID",  referencedColumnName = "JOB_ID")
    private List<JobScript> jobScripts;
    
    //삭제여부
    @Column(name = "DEL_YN", columnDefinition = "char(1) default 'N'")
    private String isDeleted;
    
    //등록일시
    @Column(name = "INS_DT")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    //수정일시
    @Column(name = "UPD_DT")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}
