package ai.mindslab.ttsmaker.api.job.service;

import ai.mindslab.ttsmaker.api.job.domain.JobM;
import ai.mindslab.ttsmaker.api.job.domain.JobScript;
import ai.mindslab.ttsmaker.api.job.domain.JobSpeaker;
import ai.mindslab.ttsmaker.api.job.domain.TTSFileDownload;
import ai.mindslab.ttsmaker.api.job.repository.JobMRepository;
import ai.mindslab.ttsmaker.api.job.repository.JobScriptRepository;
import ai.mindslab.ttsmaker.api.job.repository.JobSpeakerRepository;
import ai.mindslab.ttsmaker.api.job.repository.TTSFileDownloadRepository;
import ai.mindslab.ttsmaker.api.job.vo.request.JobContentSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.request.JobRedownloadVo;
import ai.mindslab.ttsmaker.api.job.vo.request.JobScriptSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.request.JobSpeakerSaveVo;
import ai.mindslab.ttsmaker.api.job.vo.response.*;
import ai.mindslab.ttsmaker.api.log.domain.Log;
import ai.mindslab.ttsmaker.api.log.service.LogService;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.compress.archivers.zip.ZipArchiveOutputStream;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Sort;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import ai.mindslab.ttsmaker.api.Fcst.service.FcstServiceImpl;
import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.login.domain.User;
import ai.mindslab.ttsmaker.api.login.repository.UserRepository;
import ai.mindslab.ttsmaker.api.script.service.TTSMakeService;
import ai.mindslab.ttsmaker.api.speaker.domain.Speaker;
import ai.mindslab.ttsmaker.api.speaker.service.SpeakerService;
import ai.mindslab.ttsmaker.util.DateUtils;
import ai.mindslab.ttsmaker.util.FfmpegUtils;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class JobServiceImpl implements JobService {
    private static Logger logger = LoggerFactory.getLogger(JobServiceImpl.class.getName());

    @Value("${WEATHER_API}")
    private String WEATHER_API;

    @Value("${WEATHER_API_KEY}")
    private String WEATHER_API_KEY;

    @Value("${DMZ_SERVER}")
    private String DMZ_SERVER;

    private final JobMRepository jobMRepository;

    private final JobSpeakerRepository jobSpeakerRepository;

    private final JobScriptRepository jobScriptRepository;

    private final RedisTemplate<String, Object> redisTemplate;

    private final SpeakerService speakerService;

    private final TTSMakeService ttsService;

    private final LogService logService;

    private final UserRepository userRepository;

    private final TTSFileDownloadRepository ttsFileDownloadRepository;

    private final FcstServiceImpl fcstService;

    @PersistenceContext
    private EntityManager em;

    private String current = "";

    private static final String API_BASE_URL = "http://localhost:8000";

    public JobServiceImpl(JobMRepository jobMRepository, JobSpeakerRepository jobSpeakerRepository, JobScriptRepository jobScriptRepository, RedisTemplate<String, Object> redisTemplate, SpeakerService speakerService, TTSMakeService ttsService, LogService logService, UserRepository userRepository, TTSFileDownloadRepository ttsFileDownloadRepository, FcstServiceImpl fcstService) {
        this.jobMRepository = jobMRepository;
        this.jobSpeakerRepository = jobSpeakerRepository;
        this.jobScriptRepository = jobScriptRepository;
        this.redisTemplate = redisTemplate;
        this.speakerService = speakerService;
        this.ttsService = ttsService;
        this.logService = logService;
        this.userRepository = userRepository;
        this.ttsFileDownloadRepository = ttsFileDownloadRepository;
        this.fcstService = fcstService;
    }

    @Override
    public List<JobListVo> getJobList() {
        try {
            List<JobM> list = jobMRepository.getActiveJobList(Sort.by("orderSeq"));
            return list.stream().map(JobListVo::of).collect(Collectors.toList());
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0001", "작업 리스트 조회 중 오류가 발생했습니다.");
        }
    }

    @Override
    public List<JobListVo> getJobListByUserId(String userId) {
        try {
            List<JobM> list = jobMRepository.getActiveJobListByUserId(userId, Sort.by("orderSeq"));
            return list.stream().map(JobListVo::of).collect(Collectors.toList());
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0001", "작업 리스트 조회 중 오류가 발생했습니다.");
        }
    }

    @Override
    public JobContentVo getJobContent(String jobId) {
        try {
            JobM jobM = jobMRepository.getJobContentFindByJobId(jobId);
            List<JobSpeakerVo> jobSpeakerVoList = Lists.newArrayList();
            List<JobScriptVo> jobScriptVoList = Lists.newArrayList();
            if (jobM != null) {
                List<JobSpeaker> jobSpeakers = jobM.getJobSpeakers();
                if (!CollectionUtils.isEmpty(jobSpeakers)) {
                    jobSpeakerVoList = makeJobSpeakerVoList(jobSpeakers);
                    setJobSpeakerSaveVoMapTagetByRedis(jobId, makeJobSpeakerSaveVoMap(jobSpeakers));
                }
                List<JobScript> jobScripts = jobM.getJobScripts();
                if (!CollectionUtils.isEmpty(jobScripts)) {
                    jobScriptVoList = makeJobScriptVoList(jobScripts);
                    setJobScriptsSaveVoMapTagetByRedis(jobId, makeJobScriptSaveVoMap(jobScripts));
                }
                return JobContentVo.of(jobM, jobSpeakerVoList, jobScriptVoList);
            }
            return new JobContentVo();
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0002", "작업 데이터를 불러오는중  오류가 발생했습니다.");
        }
    }

    @Override
    public List<JobSpeakerVo> getJobSpeakerList(String jobId) {
        try {
            List<JobSpeaker> list = jobSpeakerRepository.getActiveSpeakerListFindByJobId(jobId, Sort.by("orderSeq"));
            return list.stream().map(JobSpeakerVo::of).collect(Collectors.toList());
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0003", "화자 데이터를 불러오는중 오류가 발생했습니다.");
        }
    }

    @Override
    public List<JobScriptVo> getJobScriptList(String jobId) {
        try {
            List<JobScript> list = jobScriptRepository.getActiveScriptListFindByJobId(jobId, Sort.by("orderSeq"));
            return list.stream().map(JobScriptVo::of).collect(Collectors.toList());
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0004", "Script List 데이터를 불러오는중 오류가 발생했습니다.");
        }
    }

    @Override
    public JobScriptVo getJobScript(String jobId, String scriptId) {
        try {
            return JobScriptVo.of(jobScriptRepository.getJobScriptFindByJobIdAndScriptId(jobId, scriptId));
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0005", "Script 데이터를 불러오는중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * JobSpeakerSaveVoMap 저장(Redis), JobContent 조회시 저장
     * </pre>
     *
     * @param jobId
     * @param jobSpeakerSaveVoMap
     * @return
     */
    private boolean setJobSpeakerSaveVoMapTagetByRedis(String jobId, Map<String, JobSpeakerSaveVo> jobSpeakerSaveVoMap) {
        try {
            String key = String.format("%s:speakers", jobId);
            redisTemplate.opsForHash().putAll(key, jobSpeakerSaveVoMap);
            redisTemplate.expireAt(key, DateUtils.asDate(DateUtils.localDateTimeNow().plusDays(2)));
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0006", "JobSpeakerSaveVo Map 업데이트 처리 중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * JobSpeakerSaveVo 조회(Redis), JobContent 저장시 사용
     * </pre>
     *
     * @param jobId
     * @param speakerId
     * @return
     */
    public JobSpeakerSaveVo getJobSpeakerSaveVoFromRedis(String jobId, String speakerId) {
        try {
            String key = String.format("%s:speakers", jobId);
            JobSpeakerSaveVo saveSpeaker = (JobSpeakerSaveVo) redisTemplate.opsForHash().get(key, speakerId);
            return saveSpeaker;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0007", "JobSpeakerSaveVo 조회중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * jobScriptsSaveVoMap 저장(Redis), JobContent 조회시 저장
     * </pre>
     *
     * @param jobId
     * @param jobScriptsSaveVoMap
     * @return
     */
    public boolean setJobScriptsSaveVoMapTagetByRedis(String jobId, Map<String, JobScriptSaveVo> jobScriptsSaveVoMap) {
        try {
            String key = String.format("%s:scripts", jobId);
            redisTemplate.opsForHash().putAll(key, jobScriptsSaveVoMap);
            redisTemplate.expireAt(key, DateUtils.asDate(DateUtils.localDateTimeNow().plusDays(2)));
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0008", "JobScriptSaveVo Map 업데이트 처리 중 오류가 발생했습니다.");
        }
    }


    /**
     * <pre>
     * JobScriptSaveVo 저장(Redis)
     * </pre>
     *
     * @param jobId
     * @param scriptId
     * @param saveVo
     * @return
     */
    @Override
    public boolean setJobScriptsSaveVoTagetByRedis(String jobId, String scriptId, JobScriptSaveVo saveVo) {
        try {
            String key = String.format("%s:scripts", jobId);
            redisTemplate.opsForHash().put(key, scriptId, saveVo);
            redisTemplate.expireAt(key, DateUtils.asDate(DateUtils.localDateTimeNow().plusDays(2)));
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0009", "JobScriptSaveVo 업데이트 처리 중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * JobSpeakerSaveVo List 저장(Redis)
     * </pre>
     *
     * @param jobId
     * @param jobSpeakerIdList
     * @return
     */
    @Override
    public boolean setJobSpeakerSaveVosTagetByRedis(String jobId, List<String> jobSpeakerIdList) {
        try {
            Map<String, JobSpeakerSaveVo> jobScriptsSaveVoMap = new HashMap<String, JobSpeakerSaveVo>();
            if (!CollectionUtils.isEmpty(jobSpeakerIdList)) {
                List<JobSpeakerSaveVo> jobSpeakerSaveVoList = jobSpeakerIdList.stream().map(jobSpeakerId -> JobSpeakerSaveVo.builder().jobId(jobId).speakerId(jobSpeakerId).build()).collect(Collectors.toList());
                jobScriptsSaveVoMap = jobSpeakerSaveVoList.stream().collect(Collectors.toMap(JobSpeakerSaveVo::getSpeakerId, Function.identity()));
            }
            setJobSpeakerSaveVoMapTagetByRedis(jobId, jobScriptsSaveVoMap);
            return false;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0009", "JobScriptSaveVo 업데이트 처리 중 오류가 발생했습니다.");
        }
    }

    /**
     * <pre>
     * JobScriptSaveVo 조회(Redis), JobContent 저장시 사용
     * </pre>
     *
     * @param jobId
     * @param scriptId
     * @return
     */
    public JobScriptSaveVo getJobScriptSaveVoFromRedis(String jobId, String scriptId) {
        try {
            String key = String.format("%s:scripts", jobId);
            JobScriptSaveVo saveScript = (JobScriptSaveVo) redisTemplate.opsForHash().get(key, scriptId);
            return saveScript;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ00010", "JobScriptSaveVo 조회중 오류가 발생했습니다.");
        }
    }

    @Transactional
    @Override
    public boolean saveAsJobContent(String jobId, JobContentSaveVo jobContentSaveVo) {
        logger.info("Saving {} jobContent  ==> {}", jobId, jobContentSaveVo);
        //기존에 작업한 것이 있는 경우
        JobM jobM_check = jobMRepository.getJobTitle(jobContentSaveVo.getJobTitle(), jobContentSaveVo.getUserId());
        if (jobM_check != null) {
            jobId = jobM_check.getJobId();
        }

        try {
            List<JobSpeaker> jobSpeakers = Lists.newArrayList();
            List<JobScript> jobScripts = Lists.newArrayList();
            // JOB 레코드 저장
            JobM jobM = jobMRepository.getJobContentFindByJobId(jobId);
            if (jobM != null) {
                jobM.setJobTitle(jobContentSaveVo.getJobTitle());
                jobM.setUpdatedAt(DateUtils.localDateTimeNow());
                em.merge(jobM);

                jobSpeakers = jobM.getJobSpeakers();
                jobScripts = jobM.getJobScripts();
            } else {
                jobM = new JobM();
                jobM.setJobId(jobId);
                jobM.setUserId(jobContentSaveVo.getUserId());
                jobM.setJobTitle(jobContentSaveVo.getJobTitle());
                jobM.setCreatedAt(DateUtils.localDateTimeNow());
                jobM.setUpdatedAt(DateUtils.localDateTimeNow());
                em.merge(jobM);
            }
            Map<String, JobSpeaker> jobSpeakersMap = makeJobSpeakerMap(jobSpeakers);
            Map<String, Speaker> speakersMap = speakerService.allEntities().stream()
                    .collect(Collectors.toMap(Speaker::getSpeakerId, Function.identity()));

            // 선택한 화자 삭제
            logger.info("Deleting selected speaker");
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getDeletedSpeakers())) {
                for (JobSpeakerSaveVo deletedSpeaker : jobContentSaveVo.getDeletedSpeakers()) {
                    if (jobSpeakersMap.containsKey(deletedSpeaker.getSpeakerId())) {
                        JobSpeaker jobSpeaker = jobSpeakersMap.get(deletedSpeaker.getSpeakerId());
                        jobSpeaker.setIsDeleted("Y");
                        em.merge(jobSpeaker);
                    }
                }
            }
            // 선택한 화자 저장/수정
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getSpeakers())) {
                long orderSeq = 1L;
                for (JobSpeakerSaveVo saveSpeaker : jobContentSaveVo.getSpeakers()) {

                    boolean existDbJobSpeaker = jobSpeakersMap.containsKey(saveSpeaker.getSpeakerId());
                    JobSpeakerSaveVo jobSpeakerTempData = getJobSpeakerSaveVoFromRedis(jobId, saveSpeaker.getSpeakerId());
                    saveSpeaker = Optional.ofNullable(jobSpeakerTempData).orElse(saveSpeaker); //비어있으면 saveSpeaker 안비어있으면 jobSpeakerTempData

                    if (existDbJobSpeaker) {
                        JobSpeaker jobSpeaker = jobSpeakersMap.get(saveSpeaker.getSpeakerId());
                        jobSpeaker.setOrderSeq(orderSeq);
                        jobSpeaker.setUpdatedAt(DateUtils.localDateTimeNow());
                        em.merge(jobSpeaker);
                    } else {
                        JobSpeaker jobSpeaker = new JobSpeaker();
                        jobSpeaker.setJobId(jobId);
                        jobSpeaker.setSpeaker(speakersMap.get(saveSpeaker.getSpeakerId()));
                        jobSpeaker.setOrderSeq(orderSeq);
                        jobSpeaker.setCreatedAt(DateUtils.localDateTimeNow());
                        jobSpeaker.setUpdatedAt(DateUtils.localDateTimeNow());
                        em.merge(jobSpeaker);
                    }
                    orderSeq++;
                }
            }

            Map<String, JobScript> jobScriptsMap = makeJobScriptMap(jobScripts);

            // 기존 스크립트 삭제
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getDeletedScripts())) {
                for (JobScriptSaveVo deletedScript : jobContentSaveVo.getDeletedScripts()) {
                    if (jobScriptsMap.containsKey(deletedScript.getScriptId())) {
                        JobScript jobScript = jobScriptsMap.get(deletedScript.getScriptId());
                        jobScript.setIsDeleted("Y");
                        em.merge(jobScript);
                    }
                }
            }
            // 스크립트 저장/수정
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getScripts())) {
                long orderSeq = 1L;
                for (JobScriptSaveVo saveScript : jobContentSaveVo.getScripts()) {
                    boolean existDbJobScript = jobScriptsMap.containsKey(saveScript.getScriptId());

                    JobScriptSaveVo jobScriptTemp = getJobScriptSaveVoFromRedis(jobId, saveScript.getScriptId());
                    saveScript = Optional.ofNullable(jobScriptTemp).orElse(saveScript);
                    Speaker speaker = speakersMap.get(saveScript.getSpeakerId());
                    JobScript jobScript = null;
                    if (existDbJobScript) {
                        jobScript = jobScriptsMap.get(saveScript.getScriptId());
                        jobScript = adjUpdateJobScript(existDbJobScript, jobScript, speaker, saveScript, orderSeq);
                    } else {
                        jobScript = new JobScript();
                        jobScript.setJobId(jobId);
                        jobScript = adjUpdateJobScript(existDbJobScript, jobScript, speaker, saveScript, orderSeq);
                    }
                    em.merge(jobScript);
                    setJobScriptsSaveVoTagetByRedis(jobId, saveScript.getScriptId(), JobScriptSaveVo.of(jobScript));
                    orderSeq++;
                }
            }

            jobScriptRepository.deleteByIsDeleted("Y");

            em.flush();
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0007", "JOB 저장 처리 중 오류가 발생했습니다. : " + ex.getMessage());
        }
    }

    @Transactional
    @Override
    public boolean saveAsJobContentOverwrite(String jobId, String newJobTitle, String newJobId, JobContentSaveVo jobContentSaveVo) {
        logger.info("Saving {} jobContent  ==> {}", jobId, jobContentSaveVo);
        //기존에 작업한 것이 있는 경우
        try {
            JobM jobMSrc = jobMRepository.getJobByTitle(jobContentSaveVo.getJobTitle(), jobContentSaveVo.getUserId());
            JobM jobMOld = jobMRepository.getJobByTitle(newJobTitle, jobContentSaveVo.getUserId());


            List<JobSpeaker> jobOldSpeakers = jobMOld.getJobSpeakers();
            List<JobScript> jobOldScripts = jobMOld.getJobScripts();


            jobMOld.setUpdatedAt(DateUtils.localDateTimeNow());
            jobMOld.setIsDeleted("Y");
            // JOB 레코드 저장
//            em.merge(jobMOld);



            // 선택한 화자 삭제
            logger.info("Deleting selected speaker");
            if (!CollectionUtils.isEmpty(jobMOld.getJobSpeakers())) {
                for (JobSpeaker speaker : jobMOld.getJobSpeakers()) {
//                    speaker.setIsDeleted("Y");
//                    em.merge(speaker);
                    em.remove(speaker);

                }
            }
            em.flush();


            if (!CollectionUtils.isEmpty(jobMOld.getJobScripts())) {
                for (JobScript script : jobMOld.getJobScripts()) {
//                    script.setIsDeleted("Y");
//                    em.merge(script);
                    em.remove(script);
                }
            }
            em.flush();

            em.remove(jobMOld);
            em.flush();

            JobM jobM = new JobM();
            jobM.setJobId(newJobId);
            jobM.setUserId(jobContentSaveVo.getUserId());
            jobM.setJobTitle(newJobTitle);
            jobM.setCreatedAt(DateUtils.localDateTimeNow());
            jobM.setUpdatedAt(DateUtils.localDateTimeNow());
            em.merge(jobM);

            em.flush();

            List<JobSpeaker> jobSpeakers = jobM.getJobSpeakers();
            List<JobScript> jobScripts = jobM.getJobScripts();

            Map<String, JobSpeaker> jobSpeakersMap = makeJobSpeakerMap(jobSpeakers);
            Map<String, Speaker> speakersMap = speakerService.allEntities().stream()
                    .collect(Collectors.toMap(Speaker::getSpeakerId, Function.identity()));

            // 선택한 화자 삭제
            logger.info("Deleting selected speaker");
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getDeletedSpeakers())) {
                for (JobSpeakerSaveVo deletedSpeaker : jobContentSaveVo.getDeletedSpeakers()) {
                    if (jobSpeakersMap.containsKey(deletedSpeaker.getSpeakerId())) {
                        JobSpeaker jobSpeaker = jobSpeakersMap.get(deletedSpeaker.getSpeakerId());
                        jobSpeaker.setIsDeleted("Y");
                        em.merge(jobSpeaker);
                    }
                }
            }
            em.flush();
//
//
            // 선택한 화자 저장/수정
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getSpeakers())) {
                long orderSeq = 1L;
                for (JobSpeakerSaveVo saveSpeaker : jobContentSaveVo.getSpeakers()) {

                    boolean existDbJobSpeaker = jobSpeakersMap.containsKey(saveSpeaker.getSpeakerId());
                    JobSpeakerSaveVo jobSpeakerTempData = getJobSpeakerSaveVoFromRedis(newJobId, saveSpeaker.getSpeakerId());
                    saveSpeaker = Optional.ofNullable(jobSpeakerTempData).orElse(saveSpeaker); //비어있으면 saveSpeaker 안비어있으면 jobSpeakerTempData

                    if (existDbJobSpeaker) {
                        JobSpeaker jobSpeaker = jobSpeakersMap.get(saveSpeaker.getSpeakerId());
                        jobSpeaker.setOrderSeq(orderSeq);
                        jobSpeaker.setUpdatedAt(DateUtils.localDateTimeNow());
                        em.merge(jobSpeaker);
                    } else {
                        JobSpeaker jobSpeaker = new JobSpeaker();
                        jobSpeaker.setJobId(newJobId);
                        jobSpeaker.setSpeaker(speakersMap.get(saveSpeaker.getSpeakerId()));
                        jobSpeaker.setOrderSeq(orderSeq);
                        jobSpeaker.setCreatedAt(DateUtils.localDateTimeNow());
                        jobSpeaker.setUpdatedAt(DateUtils.localDateTimeNow());
                        em.merge(jobSpeaker);
                    }
                    orderSeq++;
                }
            }
            em.flush();


            Map<String, JobScript> jobScriptsMap = makeJobScriptMap(jobScripts);

            // 기존 스크립트 삭제
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getDeletedScripts())) {
                for (JobScriptSaveVo deletedScript : jobContentSaveVo.getDeletedScripts()) {
                    if (jobScriptsMap.containsKey(deletedScript.getScriptId())) {
                        JobScript jobScript = jobScriptsMap.get(deletedScript.getScriptId());
                        jobScript.setIsDeleted("Y");
                        em.merge(jobScript);
                    }
                }
            }
            em.flush();

            // 스크립트 저장/수정
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getScripts())) {
                long orderSeq = 1L;
                for (JobScriptSaveVo saveScript : jobContentSaveVo.getScripts()) {
                    boolean existDbJobScript = jobScriptsMap.containsKey(saveScript.getScriptId());

                    JobScriptSaveVo jobScriptTemp = getJobScriptSaveVoFromRedis(newJobId, saveScript.getScriptId());
                    saveScript = Optional.ofNullable(jobScriptTemp).orElse(saveScript);
                    Speaker speaker = speakersMap.get(saveScript.getSpeakerId());
                    JobScript jobScript = null;
                    if (existDbJobScript) {
                        jobScript = jobScriptsMap.get(saveScript.getScriptId());
                        jobScript = adjUpdateJobScript(existDbJobScript, jobScript, speaker, saveScript, orderSeq);
                    } else {
                        jobScript = new JobScript();
                        jobScript.setJobId(newJobId);
                        jobScript = adjUpdateJobScript(existDbJobScript, jobScript, speaker, saveScript, orderSeq);
                    }
                    em.merge(jobScript);
                    setJobScriptsSaveVoTagetByRedis(newJobId, saveScript.getScriptId(), JobScriptSaveVo.of(jobScript));
                    orderSeq++;
                }
            }

//            jobScriptRepository.deleteByIsDeleted("Y");
//            jobSpeakerRepository.deleteByIsDeleted("Y");
//            jobMRepository.deleteByIsDeleted("Y");
//
            em.flush();
            return true;
        } catch (Exception ex) {
            System.out.println(ex);
            ex.printStackTrace();
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0007", "JOB 저장 처리 중 오류가 발생했습니다. : " + ex.getMessage());
        }
    }


    @Transactional
    @Override
    public boolean saveAsJobContentNew(String jobId, JobContentSaveVo jobContentSaveVo) {
        logger.info("Saving {} new jobContent  ==> {}", jobId, jobContentSaveVo);


        try {
            List<JobSpeaker> jobSpeakers = Lists.newArrayList();
            List<JobScript> jobScripts = Lists.newArrayList();
            // JOB 레코드 저장

            JobM jobM = new JobM();
            jobM.setJobId(jobId);
            jobM.setUserId(jobContentSaveVo.getUserId());
            jobM.setJobTitle(jobContentSaveVo.getJobTitle());
            jobM.setCreatedAt(DateUtils.localDateTimeNow());
            jobM.setUpdatedAt(DateUtils.localDateTimeNow());
            em.merge(jobM);

            Map<String, JobSpeaker> jobSpeakersMap = makeJobSpeakerMap(jobSpeakers);
            Map<String, Speaker> speakersMap = speakerService.allEntities().stream()
                    .collect(Collectors.toMap(Speaker::getSpeakerId, Function.identity()));

            // 선택한 화자 삭제
            logger.info("Deleting selected speaker");
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getDeletedSpeakers())) {
                for (JobSpeakerSaveVo deletedSpeaker : jobContentSaveVo.getDeletedSpeakers()) {
                    if (jobSpeakersMap.containsKey(deletedSpeaker.getSpeakerId())) {
                        JobSpeaker jobSpeaker = jobSpeakersMap.get(deletedSpeaker.getSpeakerId());
                        jobSpeaker.setIsDeleted("Y");
                        em.merge(jobSpeaker);
                    }
                }
            }
            // 선택한 화자 저장/수정
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getSpeakers())) {
                long orderSeq = 1L;
                for (JobSpeakerSaveVo saveSpeaker : jobContentSaveVo.getSpeakers()) {

                    boolean existDbJobSpeaker = jobSpeakersMap.containsKey(saveSpeaker.getSpeakerId());
                    JobSpeakerSaveVo jobSpeakerTempData = getJobSpeakerSaveVoFromRedis(jobId, saveSpeaker.getSpeakerId());
                    saveSpeaker = Optional.ofNullable(jobSpeakerTempData).orElse(saveSpeaker); //비어있으면 saveSpeaker 안비어있으면 jobSpeakerTempData

                    if (existDbJobSpeaker) {
                        JobSpeaker jobSpeaker = jobSpeakersMap.get(saveSpeaker.getSpeakerId());
                        jobSpeaker.setOrderSeq(orderSeq);
                        jobSpeaker.setUpdatedAt(DateUtils.localDateTimeNow());
                        em.merge(jobSpeaker);
                    } else {
                        JobSpeaker jobSpeaker = new JobSpeaker();
                        jobSpeaker.setJobId(jobId);
                        jobSpeaker.setSpeaker(speakersMap.get(saveSpeaker.getSpeakerId()));
                        jobSpeaker.setOrderSeq(orderSeq);
                        jobSpeaker.setCreatedAt(DateUtils.localDateTimeNow());
                        jobSpeaker.setUpdatedAt(DateUtils.localDateTimeNow());
                        em.merge(jobSpeaker);
                    }
                    orderSeq++;
                }
            }

            Map<String, JobScript> jobScriptsMap = makeJobScriptMap(jobScripts);

            // 기존 스크립트 삭제
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getDeletedScripts())) {
                for (JobScriptSaveVo deletedScript : jobContentSaveVo.getDeletedScripts()) {
                    if (jobScriptsMap.containsKey(deletedScript.getScriptId())) {
                        JobScript jobScript = jobScriptsMap.get(deletedScript.getScriptId());
                        jobScript.setIsDeleted("Y");
                        em.merge(jobScript);
                    }
                }
            }
            // 스크립트 저장/수정
            if (!CollectionUtils.isEmpty(jobContentSaveVo.getScripts())) {
                long orderSeq = 1L;
                for (JobScriptSaveVo saveScript : jobContentSaveVo.getScripts()) {
                    boolean existDbJobScript = jobScriptsMap.containsKey(saveScript.getScriptId());

                    JobScriptSaveVo jobScriptTemp = getJobScriptSaveVoFromRedis(jobId, saveScript.getScriptId());
                    saveScript = Optional.ofNullable(jobScriptTemp).orElse(saveScript);
                    Speaker speaker = speakersMap.get(saveScript.getSpeakerId());
                    JobScript jobScript = null;
                    if (existDbJobScript) {
                        jobScript = jobScriptsMap.get(saveScript.getScriptId());
                        jobScript = adjUpdateJobScript(existDbJobScript, jobScript, speaker, saveScript, orderSeq);
                    } else {
                        jobScript = new JobScript();
                        jobScript.setJobId(jobId);
                        jobScript = adjUpdateJobScript(existDbJobScript, jobScript, speaker, saveScript, orderSeq);
                    }
                    em.merge(jobScript);
                    setJobScriptsSaveVoTagetByRedis(jobId, saveScript.getScriptId(), JobScriptSaveVo.of(jobScript));
                    orderSeq++;
                }
            }

            em.flush();
            return true;
        } catch (Exception ex) {
            throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0007", "JOB 저장 처리 중 오류가 발생했습니다. : " + ex.getMessage());
        }
    }

    /**
     * <pre>
     * JobScript 데이터 가공(신규/업데이트)
     * </pre>
     *
     * @param existJobScript
     * @param jobScript
     * @param speaker
     * @param saveVo
     * @param orderSeq
     * @return
     */
    private JobScript adjUpdateJobScript(boolean existJobScript, JobScript jobScript, Speaker speaker, JobScriptSaveVo saveVo, long orderSeq) {
        logger.info("Updating JobScript for {}", jobScript.getJobId());

        JobSettingsVo settings = Optional.ofNullable(saveVo.getSettings()).orElse(new JobSettingsVo());
        if (existJobScript) {
            jobScript.setScriptText(saveVo.getScriptText());                                // TTS 스크립트 Text
            jobScript.setTtsResourceUrl(saveVo.getTtsResourceUrl());                    // TTS Resource URL정보
            jobScript.setTtsResourceExpiryDate(saveVo.getTtsResourceExpiryDate());    // TTS Resource 만료일
            jobScript.setSettingsStyle(settings.getStyle());                                // 음성 제어 스타일 정보
            jobScript.setSettingsVolume(settings.getVolume());                            // 음성 제어 볼륨 정보
            jobScript.setSettingsSpeed(settings.getSpeed());                                // 음성 제어 스피드 정보
            jobScript.setSettingsSleep(settings.getSleep());                                // 음성 제어 읽기 쉬기 정보
            jobScript.setOrderSeq(orderSeq);                                                // 정렬순서
            jobScript.setTtsState(saveVo.getTtsState());                                    // TTS 상태
            jobScript.setRowState("S");                                                        // ROW 상태
            jobScript.setCreatedAt(DateUtils.localDateTimeNow());
            jobScript.setUpdatedAt(DateUtils.localDateTimeNow());
        } else {
            jobScript.setScriptId(saveVo.getScriptId());
            jobScript.setScriptText(saveVo.getScriptText());                                // TTS 스크립트 Text
            jobScript.setSpeaker(speaker);                                                    // 화자 정보
            jobScript.setTtsResourceUrl(saveVo.getTtsResourceUrl());                    // TTS Resource URL정보
            jobScript.setTtsResourceExpiryDate(saveVo.getTtsResourceExpiryDate());    // TTS Resource 만료일
            jobScript.setSettingsStyle(settings.getStyle());                                // 음성 제어 스타일 정보
            jobScript.setSettingsVolume(settings.getVolume());                            // 음성 제어 볼륨 정보
            jobScript.setSettingsSpeed(settings.getSpeed());                                // 음성 제어 스피드 정보
            jobScript.setSettingsSleep(settings.getSleep());                                // 음성 제어 읽기 쉬기 정보
            jobScript.setOrderSeq(orderSeq);                                                // 정렬순서
            jobScript.setTtsState(saveVo.getTtsState());                                    // TTS 상태
            jobScript.setRowState("S");                                                        // ROW 상태
            jobScript.setCreatedAt(DateUtils.localDateTimeNow());
        }
        return jobScript;
    }

    /**
     * <pre>
     * List<JobSpeaker> to Map<String, JobSpeaker> 처리
     * </pre>
     *
     * @param jobSpeakers
     * @return
     */
    private Map<String, JobSpeaker> makeJobSpeakerMap(List<JobSpeaker> jobSpeakers) {
        logger.info("Mapping JobSpeaker {}", jobSpeakers);
        if (!CollectionUtils.isEmpty(jobSpeakers)) {
            Function<JobSpeaker, String> jobSpeakerIdFunction = (JobSpeaker JobSpeaker) -> JobSpeaker.getSpeaker().getSpeakerId();
            return jobSpeakers.stream().collect(Collectors.toMap(jobSpeakerIdFunction, Function.identity()));
        }
        return new HashMap<>();
    }

    /**
     * <pre>
     * List<JobScript> to Map<String, JobScript> 처리
     * </pre>
     *
     * @param jobScripts
     * @return
     */
    private Map<String, JobScript> makeJobScriptMap(List<JobScript> jobScripts) {
        logger.info("Mapping JobScript {}", jobScripts);
        if (!CollectionUtils.isEmpty(jobScripts)) {
            return jobScripts.stream().collect(Collectors.toMap(JobScript::getScriptId, Function.identity()));
        }
        return new HashMap<>();
    }

    /**
     * <pre>
     * List<jobSpeakers> to List<JobSpeakerVo> 처리
     * </pre>
     *
     * @param jobSpeakers
     * @return
     */
    private List<JobSpeakerVo> makeJobSpeakerVoList(List<JobSpeaker> jobSpeakers) {
        logger.info("Making JobSpeaker list {}", jobSpeakers);
        if (!CollectionUtils.isEmpty(jobSpeakers)) {
            return jobSpeakers.stream().map(JobSpeakerVo::of).collect(Collectors.toList());
        }
        return Lists.newArrayList();
    }

    /**
     * <pre>
     * List<JobScript> to List<JobScriptVo> 처리
     * </pre>
     *
     * @param jobScripts
     * @return
     */
    private List<JobScriptVo> makeJobScriptVoList(List<JobScript> jobScripts) {
        logger.info("Making JobScript list {}", jobScripts);

        if (!CollectionUtils.isEmpty(jobScripts)) {
            return jobScripts.stream().map(JobScriptVo::of).collect(Collectors.toList());
        }
        return Lists.newArrayList();
    }

    /**
     * <pre>
     * List<JobScript> to Map<String, JobScriptSaveVo> 처리
     * </pre>
     *
     * @param jobScripts
     * @return
     */
    private Map<String, JobScriptSaveVo> makeJobScriptSaveVoMap(List<JobScript> jobScripts) {
        if (!CollectionUtils.isEmpty(jobScripts)) {
            return jobScripts.stream().map(JobScriptSaveVo::of).collect(Collectors.toMap(JobScriptSaveVo::getScriptId, Function.identity()));
        }
        return new HashMap<>();
    }

    /**
     * <pre>
     * List<JobSpeaker> to Map<String, JobSpeakerSaveVo> 처리
     * </pre>
     *
     * @param jobSpeakers
     * @return
     */
    private Map<String, JobSpeakerSaveVo> makeJobSpeakerSaveVoMap(List<JobSpeaker> jobSpeakers) {
        if (!CollectionUtils.isEmpty(jobSpeakers)) {
            return jobSpeakers.stream().map(JobSpeakerSaveVo::of).collect(Collectors.toMap(JobSpeakerSaveVo::getSpeakerId, Function.identity()));
        }
        return new HashMap<>();
    }

    /**
     * <pre>
     * 해상 openApi 정보 가져옴
     * </pre>
     *
     * @return
     */
    public String getSeaFcst() {
        return fcstService.getSeaFcst();
    }

    public void redownloadTTS(JobRedownloadVo jobRedownloadVo, HttpServletResponse response) throws IOException {
        logger.info("Redownloading Combined TTS File for user {}", jobRedownloadVo.getUserId());


        TTSFileDownload ttsFileDownload = ttsFileDownloadRepository.findById(jobRedownloadVo.getId()).orElse(null);

        // Get request url for ttsMakeFiles
        ArrayList<String> urlArray = new ArrayList<>();
        String fixedUrl = "http://localhost:8000/fileDownload";
        String tempPath = "/home/ubuntu/kbs_adapter/tts/" + jobRedownloadVo.getUrl();

        String replacedUrl = tempPath.replaceAll("/", "~");

        // Get all file paths from the download folder
        File folder = new File(tempPath);
        File[] listOfFiles = folder.listFiles();
        try {
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    urlArray.add(fixedUrl + "/" + replacedUrl + "/" + file.getName());
                }
            }
        } catch (Exception e) {
            logger.error("Error: redownloadTTS in JobServiceImpl {}", e.getMessage());
        }

        // Reverse url to have scripts in order
        Collections.reverse(urlArray);
        logger.info("Requested url for ttsMakeFiles {}", urlArray);

        File tempDir = Files.createTempDir();
        String outputPath = tempDir.getPath() + "/" + jobRedownloadVo.getJobTitle() + ".wav";


        try {
            String ffmpegCommand = FfmpegUtils.ffmpegCommandGenerator(urlArray, outputPath);
            Process p1 = Runtime.getRuntime().exec(ffmpegCommand);
//			String ffmpegCommand2 = FfmpegUtils.ffmpegScaling(outputPath);
//			Process p2 = Runtime.getRuntime().exec(ffmpegCommand2);
            p1.waitFor();
//			p2.waitFor();
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }


        // Download File
        File varFile = new File(outputPath);
        if (varFile.exists()) {
            response.setContentType("audio/x-wav");
            response.addHeader("Content-Disposition", "attachment; filename=" + varFile.getName());
            try {
                Files.copy(varFile, response.getOutputStream());
                response.getOutputStream().flush();
            } catch (Exception e) {
                logger.error(e.getMessage());
                e.printStackTrace();
            }
        }

        // Save log to DB
        Log log = new Log();
        log.setUserId(jobRedownloadVo.getUserId());
        User userInfo = userRepository.getUserbyUserId(jobRedownloadVo.getUserId());
        log.setName(userInfo.getName());
        log.setCode("Download");
        log.setContent("Download: " + jobRedownloadVo.getJobTitle());
        logService.saveLog(log);


        // Delete temp directory
        FileUtils.cleanDirectory(tempDir);
        tempDir.delete();
    }

    public byte[] redownloadEachTTS(JobRedownloadVo jobRedownloadVo) throws IOException {
        logger.info("Redownloading Each TTS File for user {}", jobRedownloadVo.getUserId());

        TTSFileDownload ttsFileDownload = ttsFileDownloadRepository.findById(jobRedownloadVo.getId()).orElse(null);


        // Get request url for ttsMakeFiles
        ArrayList<String> urlArray = new ArrayList<>();
        String fixedUrl = "http://localhost:8000/fileDownload";
        String tempPath = "/home/ubuntu/kbs_adapter/tts/" + jobRedownloadVo.getUrl();
        String replacedUrl = tempPath.replaceAll("/", "~");

        // Get all file paths from the download folder
        File folder = new File(tempPath);
        File[] listOfFiles = folder.listFiles();
        try {
            for (File file : listOfFiles) {
                if (file.isFile()) {
                    urlArray.add(fixedUrl + "/" + replacedUrl + "/" + file.getName());
                }
            }
        } catch (Exception e) {
            logger.error(" Error: redownloadEachTTS in JobServiceImpl ==> {}", e.getMessage());
        }

        // Reverse url to have scripts in order
        Collections.reverse(urlArray);
        logger.info("Requested url for ttsMakeFiles {}", urlArray);


        // Zip ttsMake files
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedOutputStream bos = new BufferedOutputStream(baos);
        ZipArchiveOutputStream zos = new ZipArchiveOutputStream(bos);

        Integer line = 0;
        for (String url : urlArray) {
            line++;
            zos.setEncoding("UTF-8");
            zos.putArchiveEntry(new ZipArchiveEntry("line-" + line + ".wav"));

            HttpGet req = new HttpGet(url);
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpResponse res = httpClient.execute(req);
            org.apache.http.HttpEntity entity = res.getEntity();
            InputStream is = entity.getContent();
            IOUtils.copy(is, zos);

            is.close();
            zos.closeArchiveEntry();
        }
        if (zos != null) {
            zos.finish();
            zos.flush();
            IOUtils.closeQuietly(zos);
        }
        // Save log to DB
        Log log = new Log();
        log.setUserId(jobRedownloadVo.getUserId());
        User userInfo = userRepository.getUserbyUserId(jobRedownloadVo.getUserId());
        log.setName(userInfo.getName());
        log.setCode("Download");
        log.setContent("Download: " + jobRedownloadVo.getJobTitle());
        logService.saveLog(log);


        IOUtils.closeQuietly(bos);
        IOUtils.closeQuietly(baos);
        return baos.toByteArray();
    }

    // Download
    @Override
    public void downloadTTS(JobContentSaveVo jobContentSaveVo, HttpServletResponse response) throws IOException {
        logger.info("Downloading Combined TTS File for user {}", jobContentSaveVo.getUserId());

        // Getting VoiceName & Text list
        UUID downloadId = UUID.randomUUID();
        String downloadPath = "download/" + jobContentSaveVo.getUserId() + "/" + downloadId.toString();

        // Get request url for ttsMakeFiles
        ArrayList<String> urlArray = new ArrayList<>();
        for (JobScriptSaveVo script : jobContentSaveVo.getScripts()) {
            String url = ttsService.requestTTSMakeFile(script.getVoiceName(),
                    script.getScriptText(),
                    null,
                    downloadPath
            )
                    .block().getDownloadURL();
            urlArray.add(url);
        }
        logger.info("Requested url for ttsMakeFiles {}", urlArray);

        // Creating temp directory to store the audio file
        File tempDir = Files.createTempDir();
        String outputPath = tempDir.getPath() + "/" + jobContentSaveVo.getJobTitle() + ".wav";
        System.out.println("Creating folder . . . " + outputPath);
        System.out.println("fffmpeg command starting . . . ");


        // Combine all ttsMakeFiles to a single audio file
        try {
            String ffmpegCommand = FfmpegUtils.ffmpegCommandGenerator(urlArray, outputPath);
            Process p1 = Runtime.getRuntime().exec(ffmpegCommand);
//			String ffmpegCommand2 = FfmpegUtils.ffmpegScaling(outputPath);
//			Process p2 = Runtime.getRuntime().exec(ffmpegCommand2);
            p1.waitFor();
//			p2.waitFor();
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }


        // Download File
        System.out.println("Downloading file . . . ");
        File varFile = new File(outputPath);
        if (varFile.exists()) {
            response.setContentType("audio/x-wav");
            response.addHeader("Content-Disposition", "attachment; filename=" + varFile.getName());
            try {
                Files.copy(varFile, response.getOutputStream());
                response.getOutputStream().flush();
            } catch (Exception e) {
                logger.error(e.getMessage());
                e.printStackTrace();
            }
        }

        // Save log to DB
        Log log = new Log();
        log.setUserId(jobContentSaveVo.getUserId());
        User userInfo = userRepository.getUserbyUserId(jobContentSaveVo.getUserId());
        log.setName(userInfo.getName());
        log.setCode("Download");
        log.setContent("Download: " + jobContentSaveVo.getJobTitle());
        logService.saveLog(log);

        // Get play length of audio file
        double durationInSeconds = -1;
        try (FileInputStream fis = new FileInputStream(varFile);
             BufferedInputStream bis = new BufferedInputStream(fis);
             AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(bis);) {

            AudioFormat format = audioInputStream.getFormat();
            long frames = audioInputStream.getFrameLength();
            durationInSeconds = (frames + 0.0) / format.getFrameRate();
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        }

        // Get scripts used for the audio file
        List<String> scriptList = new ArrayList<>();
        jobContentSaveVo.getScripts().forEach(jobScriptSaveVo -> {
            scriptList.add(jobScriptSaveVo.getScriptText());
        });

        SimpleDateFormat fileDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String now = fileDateFormat.format(new Date());

        // Save download record to DB
        TTSFileDownload ttsFileDownload = TTSFileDownload.builder()
                .userId(userInfo.getUserId())
                .jobId(jobContentSaveVo.getJobTitle())
                .scriptText(String.join(";", scriptList))
                .playLength(durationInSeconds)
                .downloadURL(now + "/" + downloadPath)
                .downloadedAt(DateUtils.localDateTimeNow())
                .isZiped("N")
                .build();
        logger.info("Saved Download record to DB {}", ttsFileDownload);

        ttsFileDownloadRepository.save(ttsFileDownload);

        // Delete temp directory
        FileUtils.cleanDirectory(tempDir);
        tempDir.delete();
    }

    @Override
    public List<UserDownloadInfoVo> getUserDownloadInfo(String userId) {
        logger.info("Getting DownloadInfo for user {}", userId);
        List<TTSFileDownload> ttsFileDownloads = ttsFileDownloadRepository.findAllByUserId(userId);
        return ttsFileDownloads.stream().map(UserDownloadInfoVo::of).collect(Collectors.toList());
    }

    @Override
    public byte[] downloadEachTTS(JobContentSaveVo jobContentSaveVo) throws IOException {
        logger.info("Downloading Each TTS File for user {}", jobContentSaveVo.getUserId());
        // Getting VoiceName & Text list
        UUID downloadId = UUID.randomUUID();
        String downloadPath = "download/" + jobContentSaveVo.getUserId() + "/" + downloadId.toString();

        // Get request url for ttsMakeFiles
        ArrayList<String> urlArray = new ArrayList<>();
        for (JobScriptSaveVo script : jobContentSaveVo.getScripts()) {
            String url = ttsService.requestTTSMakeFile(script.getVoiceName(),
                    script.getScriptText(),
                    null,
                    downloadPath)
                    .block().getDownloadURL();
            urlArray.add(url);
        }
        logger.info("Requested url for ttsMakeFiles {}", urlArray);

        // Parse url to find the path for ttsMakeFiles
        String tempPath = urlArray.get(0);
        int iend = urlArray.get(0).indexOf("~");
        String ttsFilePath;

        if (iend != 1) {
            ttsFilePath = tempPath.substring(iend);
            iend = ttsFilePath.indexOf("/");
            tempPath = ttsFilePath.substring(0, iend).replaceAll("~", "/");

        } else {
            logger.error("Cannot parse url to find the path for ttsMakeFiles");
        }

        // Get play length of audio file
        double durationInSeconds = 0;
        File folder = new File(tempPath);
        File[] listOfFiles = folder.listFiles();
        for (File file : listOfFiles) {
            try (FileInputStream fis = new FileInputStream(file);
                 BufferedInputStream bis = new BufferedInputStream(fis);
                 AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(bis);) {

                AudioFormat format = audioInputStream.getFormat();
                long frames = audioInputStream.getFrameLength();
                durationInSeconds += (frames + 0.0) / format.getFrameRate();
            } catch (UnsupportedAudioFileException e) {
                logger.error(e.getMessage());
                e.printStackTrace();
            }
        }

        // Zip ttsMake files
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedOutputStream bos = new BufferedOutputStream(baos);
        ZipArchiveOutputStream zos = new ZipArchiveOutputStream(bos);

        Integer line = 0;
        for (String url : urlArray) {
            line++;
            zos.setEncoding("UTF-8");
            zos.putArchiveEntry(new ZipArchiveEntry("line-" + line + ".wav"));

            HttpGet req = new HttpGet(url);
            CloseableHttpClient httpClient = HttpClients.createDefault();
            HttpResponse res = httpClient.execute(req);
            org.apache.http.HttpEntity entity = res.getEntity();
            InputStream is = entity.getContent();
            IOUtils.copy(is, zos);

            is.close();
            zos.closeArchiveEntry();
        }
        if (zos != null) {
            zos.finish();
            zos.flush();
            IOUtils.closeQuietly(zos);
        }

        // Save log to DB
        Log log = new Log();
        log.setUserId(jobContentSaveVo.getUserId());
        User userInfo = userRepository.getUserbyUserId(jobContentSaveVo.getUserId());
        log.setName(userInfo.getName());
        log.setCode("Download-Zip");
        log.setContent("Download-zip: " + jobContentSaveVo.getJobTitle());
        logService.saveLog(log);
        List<String> scriptList = new ArrayList<>();

        jobContentSaveVo.getScripts().forEach(jobScriptSaveVo -> {
            scriptList.add(jobScriptSaveVo.getScriptText());
        });

        SimpleDateFormat fileDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String now = fileDateFormat.format(new Date());

        // Save download record to DB
        TTSFileDownload ttsFileDownload = TTSFileDownload.builder()
                .userId(userInfo.getUserId())
                .jobId(jobContentSaveVo.getJobTitle())
                .scriptText(String.join(";", scriptList))
                .playLength(durationInSeconds)
                .downloadURL(now + "/" + downloadPath)
                .downloadedAt(DateUtils.localDateTimeNow())
                .isZiped("Y")
                .build();
        logger.info("Saved Download record to DB {}", ttsFileDownload);

        ttsFileDownloadRepository.save(ttsFileDownload);

        IOUtils.closeQuietly(bos);
        IOUtils.closeQuietly(baos);
        return baos.toByteArray();
    }

    // Send
    @Override
    public Object sendTTStoDMZ(JobContentSaveVo jobContentSaveVo) throws IOException {
        logger.info("Sending TTS To DMZ for user {}", jobContentSaveVo.getUserId());
        // Getting VoiceName & Text list
        ArrayList<String> urlArray = new ArrayList<>();
        for (JobScriptSaveVo script : jobContentSaveVo.getScripts()) {
            String url = ttsService.requestTTSMakeFile(script.getVoiceName(), script.getScriptText(), null, null).block().getDownloadURL();
            urlArray.add(url);
        }

        SimpleDateFormat format1 = new SimpleDateFormat("yyyyMMddHHmmss");
        String currentTime = format1.format(System.currentTimeMillis());

        File tempDir = Files.createTempDir();
        String filename = jobContentSaveVo.getJobTitle() + "-" + currentTime + ".wav";
        String outputPath2 = tempDir.getPath() + "/" + "firstWav.wav";
        String outputPath = tempDir.getPath() + "/" + filename;

        try {
            String ffmpegCommand = FfmpegUtils.ffmpegCommandGenerator(urlArray, outputPath2);
            Process p1 = Runtime.getRuntime().exec(ffmpegCommand);
            p1.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Sending merged wav file to DMZ
        File wavFile = new File(outputPath2);
        long wavFileDuration = 0;
        try {
            wavFileDuration = (long) getDuration(wavFile);
        } catch (UnsupportedAudioFileException e) {
            e.printStackTrace();
        }
        try {
            String ffmpegCommand = FfmpegUtils.ffmpeg24bit(outputPath2, outputPath);
            Process p1 = Runtime.getRuntime().exec(ffmpegCommand);
            System.out.println("this is 24bit wait for " + p1.waitFor());
        } catch (Exception e) {
            e.printStackTrace();
        }

//		File wavFile2 = new File(outputPath);

        File wavFile2 = new File(new String(outputPath.getBytes(), "UTF-8"));
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", new FileSystemResource(wavFile2));
        body.add("systemId", "AI");
        body.add("daletUserId", jobContentSaveVo.getUserId());
        body.add("programTitle", jobContentSaveVo.getDestination().getTitle());
        body.add("categoryId", jobContentSaveVo.getDestination().getCategoryId());
        body.add("itemCode", jobContentSaveVo.getDestination().getItemCode());
        body.add("channel", jobContentSaveVo.getDestination().getChannel());
        body.add("fileName", filename);
        body.add("durationMS", wavFileDuration);

        logger.info("{}", body);
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> response = restTemplate.postForEntity(DMZ_SERVER + "/tts/createProgram", requestEntity, Object.class);
        System.out.println(response);
        Log log = new Log();
        log.setUserId(jobContentSaveVo.getUserId());
        User userInfo = userRepository.getUserbyUserId(jobContentSaveVo.getUserId());
        log.setName(userInfo.getName());
        log.setCode(jobContentSaveVo.getDestination().getKind());
        log.setContent("Send: " + jobContentSaveVo.getJobTitle());
        logService.saveLog(log);

        // Deleting temp directory
        FileUtils.cleanDirectory(tempDir);
        tempDir.delete();

        return response;
    }

    @Override
    public Object checkItemCode(JobContentSaveVo jobContentSaveVo) {
        logger.info("Checking ItemCode for user {}", jobContentSaveVo.getUserId());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("itemCode", jobContentSaveVo.getDestination().getItemCode());
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Object> response = restTemplate.postForEntity(DMZ_SERVER + "/tts/checkItemCode", requestEntity, Object.class);

        System.out.println("ChekcItemCode");
        System.out.println(response);
        System.out.println();
        return response;
    }

    private static float getDuration(File file) throws IOException, UnsupportedAudioFileException {
        logger.info("Getting Duration of {}", file);

        AudioInputStream ais = AudioSystem.getAudioInputStream(file);
        AudioFormat format = ais.getFormat();
        long audioFileLength = file.length();
        int frameSize = format.getFrameSize();
        float frameRate = format.getFrameRate();
        float durationInMS = (audioFileLength / (frameSize * frameRate)) * 1000;

        System.out.println(durationInMS);
        return (durationInMS);
    }
}
