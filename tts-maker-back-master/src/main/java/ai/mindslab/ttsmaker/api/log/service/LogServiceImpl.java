package ai.mindslab.ttsmaker.api.log.service;

import org.springframework.stereotype.Service;
import ai.mindslab.ttsmaker.api.log.domain.Log;
import ai.mindslab.ttsmaker.api.log.repository.LogRepository;

import java.util.List;

@Service
public class LogServiceImpl implements LogService {

    private final LogRepository logRepository;

    public LogServiceImpl(LogRepository logRepository) {
        this.logRepository = logRepository;
    }

    public void saveLog(Log log){
        try {
            logRepository.save(log);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public List<Log> getAllLog() {
        return logRepository.findAll();
    }

    public List<Log> searchLog(String search) {

        return logRepository.searchLog(search);
    }

}
