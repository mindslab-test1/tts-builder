package ai.mindslab.ttsmaker.api.provider.maumai.tts.service;

import ai.mindslab.ttsmaker.api.provider.maumai.tts.exception.MaumAIApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIRequestParam;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIResponseFile;

import java.util.UUID;


@Service
@PropertySource("classpath:application.properties")
public class TTSApiService {

    private static final String CACHE_CONTROL = "no-cache";
    private static final String API_MIME_TYPE = "application/json";

    @Value("${api.baseUrl}")
    private String API_BASE_URL;
    private static final String API_ID = "swhange91d6f0f28ab";
    private static final String API_KEY = "78ab9ae227d149539d96ed553ae448a4";

    private final WebClient webClient;

    private static Logger logger = LoggerFactory.getLogger(TTSApiService.class.getName());

    public TTSApiService(@Value("${api.baseUrl}") final String API_BASE_URL) {

        this.webClient = WebClient.builder()
                .baseUrl(API_BASE_URL)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, API_MIME_TYPE)
                .defaultHeader(HttpHeaders.CACHE_CONTROL, CACHE_CONTROL)
                .build();

    }

    //https://stackoverflow.com/questions/57652631/how-do-custom-exception-in-webflux-webclient
    //https://stackoverflow.com/questions/49485523/get-api-response-error-message-using-web-client-mono-in-spring-boot
    //https://supawer0728.github.io/2018/03/11/Spring-request-model3/
    //https://www.baeldung.com/exception-handling-for-rest-with-spring
    public Mono<MaumAIResponseFile> requestTTSMakeFile(String voiceName, String text, String downloadId) {
        Float speed = 1.0f;
        UUID fileName = UUID.randomUUID();

        MaumAIRequestParam param = new MaumAIRequestParam(API_ID, API_KEY, voiceName, text, speed, fileName.toString(), downloadId);
        logger.info("Request [{}]: {}", API_BASE_URL, param);

        return webClient.post().uri("/makeFile")
                .body(Mono.just(param), MaumAIRequestParam.class).retrieve()
                .onStatus(HttpStatus::isError, response -> {
                    MaumAIApiException exception = null;
                    HttpStatus statusCode = response.statusCode();
                    if (statusCode != null) {
                        switch (statusCode) {
                            case FORBIDDEN:
                                logger.error("{}: ERRA0001 - MaumAI API provider 인증 오류 ", response.statusCode());
                                exception = new MaumAIApiException(response.statusCode(), "ERRA0001", "MaumAI API provider 인증 오류");
                                break;
                            case BAD_GATEWAY:
                                logger.error("{}: ERRA0002 - MaumAI API provider와의 통신 오류", response.statusCode());
                                exception = new MaumAIApiException(response.statusCode(), "ERRA0002", "MaumAI API provider와의 통신 오류");
                                break;
                            case BAD_REQUEST:
                                logger.error("{}: ERRA0003 - The request parameter value is invalid", response.statusCode());
                                exception = new MaumAIApiException(response.statusCode(), "ERRA0003", "The request parameter value is invalid.(MaumAI API)");
                                break;
                            default:
                                if (statusCode.is4xxClientError() || statusCode.is5xxServerError()) {
                                    logger.error("{}: ERRA0000 - MaumAIAPI provider 통신 오류", response.statusCode());
                                    exception = new MaumAIApiException(response.statusCode(), "ERRA0000", "MaumAIAPI provider 통신 오류");
                                }
                                break;
                        }
                    } else {
                        exception = new MaumAIApiException(response.statusCode(), "ERRA0000", "MaumAIAPI provider 통신 오류");
                    }
                    return Mono.error(exception);
                })
                .bodyToMono(MaumAIResponseFile.class);
    }
}
