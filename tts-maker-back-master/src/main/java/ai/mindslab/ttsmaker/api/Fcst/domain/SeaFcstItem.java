package ai.mindslab.ttsmaker.api.Fcst.domain;

import lombok.Data;

@Data
public class SeaFcstItem {
    public String tmFc;
    public String wh2;
    public String wh1;
    public String numEf;
    public String rnYn;
    public String wd2;
    public String wd1;
    public String wdTnd;
    public String wfCd;
    public String wf;
    public String ws1;
    public String ws2;
    public String regId;
}
