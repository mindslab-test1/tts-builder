package ai.mindslab.ttsmaker.api.login.domain;

import lombok.*;
import ai.mindslab.ttsmaker.api.login.vo.MembershipRoleVo;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "ROLES")
public class MembershipRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private MembershipRoleVo name;

    @OneToMany(mappedBy = "membershipRole")
    private List<User> user;
}