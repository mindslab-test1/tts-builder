package ai.mindslab.ttsmaker.api.speaker.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import ai.mindslab.ttsmaker.api.speaker.service.SpeakerService;
import ai.mindslab.ttsmaker.api.speaker.vo.SpeakerVo;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/speaker")
@CrossOrigin
public class SpeakerRestController {

	private final SpeakerService speakerService;

	private static Logger logger = LoggerFactory.getLogger(SpeakerRestController.class.getName());

	public SpeakerRestController(SpeakerService speakerService) {
		this.speakerService = speakerService;
	}

	@ResponseBody
	@GetMapping
	public List<SpeakerVo> list() throws IOException {
		logger.info("[Get Speaker List]");
		return speakerService.getAllList();
	}
	
	@ResponseBody
	@GetMapping(value = "/randomSpeaker")
	public SpeakerVo randomSpeaker() throws IOException {
		logger.info("[Get Random Speaker]");
		return speakerService.getRandomSpeaker();
	}
}
