package ai.mindslab.ttsmaker.api.payment.service;

import ai.mindslab.ttsmaker.api.payment.vo.BillingFormVo;
import ai.mindslab.ttsmaker.api.payment.vo.PaymentFilterRequestVo;
import ai.mindslab.ttsmaker.api.payment.vo.PaymentVo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface PaymentService {
    ResponseEntity<BillingFormVo> basicBilling(String method, String p, String userId) throws Exception;

    RedirectView billingPay(String email, HttpServletRequest request, RedirectAttributes rediectAttr);

    String getFilteredPayments(PaymentFilterRequestVo filterRequestVo);

    public HashMap<String, Object> cancelPay(String uip, String tid, Long userId);

    List<PaymentVo> getRoutineUserList(LocalDate currentDate);

    String getPaymentMethod(Long userId);

    void routineBill(PaymentVo billing) throws Exception;

    Map<String, Object> planCancel(Long userNo);

    LocalDate getUserPaymentExpirationDate(Long userNo);

    void planCancelProcedure(Long userNo);

    int deleteBillInfo(Long userNo);

    List<PaymentVo> getUnpaymentList();

    public String getUip(HttpServletRequest request);
}
