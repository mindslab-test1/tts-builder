package ai.mindslab.ttsmaker.api.log.vo;

import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogVo {
    private String id;
    private String userId;
    private String name;
    private String code;
    private String content;
    private String search;
}
