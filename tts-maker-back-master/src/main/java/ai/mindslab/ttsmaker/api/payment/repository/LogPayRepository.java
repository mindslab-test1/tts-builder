package ai.mindslab.ttsmaker.api.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ai.mindslab.ttsmaker.api.payment.domain.LogPay;

@Repository
public interface LogPayRepository extends JpaRepository<LogPay, Long> {
}
