package ai.mindslab.ttsmaker.api.provider.maumai.tts.vo;

import lombok.Data;

@Data
public class MaumAIResponseMessage {
	private String message;
	private int status;
}
