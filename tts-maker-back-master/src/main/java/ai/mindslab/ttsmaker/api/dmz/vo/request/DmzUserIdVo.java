package ai.mindslab.ttsmaker.api.dmz.vo.request;

import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DmzUserIdVo {
    private String userId;
}
