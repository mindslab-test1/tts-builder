package ai.mindslab.ttsmaker.api.payment.vo;

import lombok.*;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PaymentFilterResponseVo {
    private Long id;
    private String name;
    private String userId;
    private String phoneNumber;
    private String company;
    private String payment;
    private String productName;
    private Long price;
    private String currency;
    private String createDate;
    private String cancelledDate;
    private String cardName;
    private String tid;
    private String paNo;
    private String status;
    private Long payCount;
    private String paymentDate;
    private Integer canCancel;






}
