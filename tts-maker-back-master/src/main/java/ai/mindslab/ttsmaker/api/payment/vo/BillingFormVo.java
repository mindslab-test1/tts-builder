package ai.mindslab.ttsmaker.api.payment.vo;

import lombok.*;

import java.util.Map;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BillingFormVo {
    private String mid;
    private String timestamp;
    private String oid;
    private String basicPrice;
    private String basicName;
    private String businessPrice;
    private String businessName;
    private String mKey;
    private Map<String, String> signParam;
    private String basicSignature;
    private String businessSignature;
    private String siteDomain;
    private String dateTo;
    private String dateFrom;
    private String method;
    private String name;
    private String phoneNumber;
    private String userId;
}
