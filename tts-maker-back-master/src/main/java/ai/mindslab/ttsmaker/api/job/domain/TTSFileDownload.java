package ai.mindslab.ttsmaker.api.job.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Builder
@Table(name = "TTS_FILE_DOWNLOAD")
public class TTSFileDownload implements Serializable {
    private static final long serialVersionUID = 9182436812539983497L;

    //관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private long id;

    //저장한 유저 정보
    @Column(name = "USERID", columnDefinition = "varchar(100)")
    private String userId;

    //JOB ID
    @Column(name = "JOB_ID", columnDefinition = "varchar(36)", nullable = false)
    private String jobId;

    // TTS 스크립트 Text
    @Column(name = "SCRIPT_TEXT", columnDefinition = "TEXT", nullable = false)
    private String scriptText;

    @Column(name = "PLAY_LENGTH", columnDefinition = "decimal(10, 2)")
    private double playLength;

    @Column(name = "DOWNLOAD_URL", columnDefinition = "varchar(100)")
    private String downloadURL;

    @Column(name = "IS_ZIPED", columnDefinition="char(1)")
    private String isZiped;

    //등록일시
    @Column(name = "DOWNLOAD_DT")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime downloadedAt;
}
