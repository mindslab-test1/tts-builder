package ai.mindslab.ttsmaker.api.payment.service;

import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import ai.mindslab.ttsmaker.api.login.domain.MembershipRole;
import ai.mindslab.ttsmaker.api.login.domain.User;
import ai.mindslab.ttsmaker.api.login.repository.MembershipRoleRepository;
import ai.mindslab.ttsmaker.api.login.repository.UserRepository;
import ai.mindslab.ttsmaker.api.login.vo.MembershipRoleVo;
import ai.mindslab.ttsmaker.api.payment.domain.*;
import ai.mindslab.ttsmaker.api.payment.repository.*;
import ai.mindslab.ttsmaker.api.payment.vo.BillingFormVo;
import ai.mindslab.ttsmaker.api.payment.vo.PaymentFilterRequestVo;
import ai.mindslab.ttsmaker.api.payment.vo.PaymentFilterResponseVo;
import ai.mindslab.ttsmaker.api.payment.vo.PaymentVo;
import ai.mindslab.ttsmaker.util.DateUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.inicis.inipay4.INIpay;
import com.inicis.inipay4.util.INIdata;
import com.inicis.std.util.HttpUtil;
import com.inicis.std.util.ParseUtil;
import com.inicis.std.util.SignatureUtil;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Service
public class PaymentServiceImpl implements PaymentService {
    private static Logger logger = LoggerFactory.getLogger(PaymentService.class.getName());

    @Value("${api.paymentUrl}")
    private String API_BASE_URL;

    @Value("${file.inipayHome}")
    private String mPath_InipayHome;

    private final ProductRepository productRepository;

    private final BillingRepository billingRepository;

    private final LogPayRepository logPayRepository;

    private final UserRepository userRepository;

    private final MasterCodeRepository masterCodeRepository;

    private final BillLogRepository billLogRepository;

    private final PayRepository payRepository;

    final MembershipRoleRepository membershipRoleRepository;

    @PersistenceContext
    private EntityManager em;

    public PaymentServiceImpl(ProductRepository productRepository, BillingRepository billingRepository, LogPayRepository logPayRepository, UserRepository userRepository, MasterCodeRepository masterCodeRepository, BillLogRepository billLogRepository, PayRepository payRepository, MembershipRoleRepository membershipRoleRepository) {
        this.productRepository = productRepository;
        this.billingRepository = billingRepository;
        this.logPayRepository = logPayRepository;
        this.userRepository = userRepository;
        this.masterCodeRepository = masterCodeRepository;
        this.billLogRepository = billLogRepository;
        this.payRepository = payRepository;
        this.membershipRoleRepository = membershipRoleRepository;
    }

    @Override
    public ResponseEntity<BillingFormVo> basicBilling(String method, String price, String userId) throws Exception {
        User user = userRepository.getUserInfo(userId);

        logger.info("Creating basic billing information");
        String mid = "mindslab02";
        String signKey = "d0tJaWl2QXlMeWxrWFh5RFVYaVlmQT09";
        String timestamp = SignatureUtil.getTimestamp();
        String oid = mid + "_" + timestamp;
        String basicName = "Basic";
        String basicPrice = "29000";
        String businessPrice = price;
//        String businessPrice = "1000";
        String businessName = "FREE";

        // TODO: CHANGE price
        if (price.equals("14900")) {
//        if (businessPrice.equals("1000")) {
            businessName = "BASIC";
        } else if (businessPrice.equals("44000")) {
            businessName = "PRO";
        } else if (businessPrice.equals("98000")) {
            businessName = "PROPLUS";
        }

        String mKey = SignatureUtil.hash(signKey, "SHA-256");

        // 결제 제공기간 설정 (오늘 ~ 오늘 + 30일)
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Calendar cal = Calendar.getInstance();
        String dateTo = sdf.format(cal.getTime());
        cal.add(Calendar.DATE, 30);
        String dateFrom = sdf.format(cal.getTime());

        Map<String, String> signParam = new HashMap<String, String>();
        Map<String, String> basicSignParam = new HashMap<String, String>();
        Map<String, String> businessSignParam = new HashMap<String, String>();

        // BasicSignParam 과 BusinessSignParam 분리 관리
        basicSignParam.put("oid", oid);
        basicSignParam.put("price", basicPrice);
        basicSignParam.put("timestamp", timestamp);

        businessSignParam.put("oid", oid);
        businessSignParam.put("price", businessPrice);
        businessSignParam.put("timestamp", timestamp);

        String basicSignature = SignatureUtil.makeSignature(basicSignParam);
        String businessSignature = SignatureUtil.makeSignature(businessSignParam);

        BillingFormVo billingFormVo = new BillingFormVo(mid, timestamp, oid, basicPrice,
                basicName, businessPrice, businessName, mKey,
                signParam, basicSignature, businessSignature, API_BASE_URL /*http://127.0.0.1:8080*/,
                dateTo, dateFrom, method, user.getName(), user.getPhoneNumber(), user.getUserId());

        logger.info("Billing form created: {}", billingFormVo);
        return ResponseEntity.ok(billingFormVo);
    }

    @Override
    public RedirectView billingPay(String userId, HttpServletRequest request, RedirectAttributes redirectAttr) {

        Map<String, String> resultMap = new HashMap<>();
        Calendar cal = Calendar.getInstance();

        // Get user information
        User user = userRepository.getUserInfo(userId);

        try {

            //#############################
            // 인증결과 파라미터 일괄 수신
            //#############################
            request.setCharacterEncoding("UTF-8");

            Map<String, String> paramMap = new Hashtable<String, String>();

            Enumeration elems = request.getParameterNames();

            String temp = "";

            while (elems.hasMoreElements()) {
                temp = (String) elems.nextElement();
                paramMap.put(temp, request.getParameter(temp));
            }

            //#####################
            // 인증이 성공일 경우만
            //#####################
            logger.info("Authentication result parameters : " + paramMap.toString());
            if ("0000".equals(paramMap.get("resultCode"))) {
                logger.info("Authentication successful");


                //############################################
                // 1.전문 필드 값 설정(***가맹점 개발수정***)
                //############################################

                String mid = paramMap.get("mid");                        // 가맹점 ID 수신 받은 데이터로 설정
                String signKey = "d0tJaWl2QXlMeWxrWFh5RFVYaVlmQT09";        // 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
                String timestamp = SignatureUtil.getTimestamp();                // util에 의해서 자동생성
                String charset = "UTF-8";                                    // 리턴형식[UTF-8,EUC-KR](가맹점 수정후 고정)
                String format = "JSON";                                    // 리턴형식[XML,JSON,NVP](가맹점 수정후 고정)
                String authToken = paramMap.get("authToken");                // 취소 요청 tid에 따라서 유동적(가맹점 수정후 고정)
                String authUrl = paramMap.get("authUrl");                    // 승인요청 API url(수신 받은 값으로 설정, 임의 세팅 금지)
                String netCancel = paramMap.get("netCancelUrl");                // 망취소 API url(수신 받은 값으로 설정, 임의 세팅 금지)
                String ackUrl = paramMap.get("checkAckUrl");                // 가맹점 내부 로직 처리후 최종 확인 API URL(수신 받은 값으로 설정, 임의 세팅 금지)
                String merchantData = paramMap.get("merchantData");            // 가맹점 관리데이터 수신

                //#####################
                // 2.signature 생성
                //#####################
                Map<String, String> signParam = new HashMap<String, String>();

                signParam.put("authToken", authToken);        // 필수
                signParam.put("timestamp", timestamp);        // 필수

                // signature 데이터 생성 (모듈에서 자동으로 signParam을 알파벳 순으로 정렬후 NVP 방식으로 나열해 hash)
                String signature = SignatureUtil.makeSignature(signParam);

                //#####################
                // 3.API 요청 전문 생성
                //#####################
                Map<String, String> authMap = new Hashtable<String, String>();

                authMap.put("mid", mid);            // 필수
                authMap.put("authToken", authToken);    // 필수
                authMap.put("signature", signature);    // 필수
                authMap.put("timestamp", timestamp);    // 필수
                authMap.put("charset", charset);        // default=UTF-8
                authMap.put("format", format);        // default=XML
                //authMap.put("price" 		,price);		// 가격위변조체크기능 (선택사용)

                System.out.println("##승인요청 API 요청##");

                HttpUtil httpUtil = new HttpUtil();

                try {
                    //#####################
                    // 4.API 통신 시작
                    //#####################

                    String authResultString = "";
                    authResultString = httpUtil.processHTTP(authMap, authUrl);

                    //############################################################
                    //5.API 통신결과 처리(***가맹점 개발수정***)
                    //############################################################
                    String test = authResultString.replace(",", "&").replace(":", "=").replace("\"", "").replace(" ", "").replace("\n", "").replace("}", "").replace("{", "");
                    resultMap = ParseUtil.parseStringToMap(test); //문자열을 MAP형식으로 파싱
                    logger.info("Authentication result:  " + resultMap);

                    /*************************  결제보안 강화 2016-05-18 START ****************************/
                    Map<String, String> secureMap = new HashMap<String, String>();
                    secureMap.put("mid", mid);                                //mid
                    secureMap.put("tstamp", timestamp);                        //timestemp
                    secureMap.put("MOID", resultMap.get("MOID"));            //MOID
                    secureMap.put("TotPrice", resultMap.get("TotPrice"));        //TotPrice

                    // signature 데이터 생성
                    String secureSignature = SignatureUtil.makeSignatureAuth(secureMap);
                    /*************************  결제보안 강화 2016-05-18 END ****************************/

                    if ("0000".equals(resultMap.get("resultCode")) && secureSignature.equals(resultMap.get("authSignature"))) {    //결제보안 강화 2016-05-18
                        /*****************************************************************************
                         * 여기에 가맹점 내부 DB에 결제 결과를 반영하는 관련 프로그램 코드를 구현한다.

                         [중요!] 승인내용에 이상이 없음을 확인한 뒤 가맹점 DB에 해당건이 정상처리 되었음을 반영함
                         처리중 에러 발생시 망취소를 한다.
                         ******************************************************************************/

                        logger.info("Preparing for INICIS Billing Payment");
                        // 빌링 결제 이니시스 전송 - YBU 2019.04.03
                        INIpay inipay = new INIpay();
                        INIdata data = new INIdata();
                        String uip = this.getUip(request);

                        data.setData("inipayHome", mPath_InipayHome);         // INIpay45 절대경로 (key / log 디렉토리) 추후 위치 수정 예정
                        data.setData("logMode", "DEBUG");               // 로그모드 (INFO / DEBUG)
                        data.setData("type", "reqrealbill");            // type (고정)
                        data.setData("paymethod", "Card");              // 지불수단 (고정)
                        data.setData("crypto", "execure");              // 암복호화모듈 (고정)

                        data.setData("uip", uip);
                        data.setData("url", API_BASE_URL);
                        data.setData("mid", "mindslab02");
                        data.setData("uid", "mindslab02");
                        data.setData("keyPW", "1111");

                        data.setData("moid", resultMap.get("MOID"));
                        data.setData("goodname", resultMap.get("goodName"));
                        data.setData("price", resultMap.get("TotPrice"));
                        data.setData("currency", "WON");

                        data.setData("buyername", resultMap.get("buyerName"));
                        data.setData("buyertel", resultMap.get("buyerTel"));
                        data.setData("buyeremail", resultMap.get("buyerEmail"));

                        data.setData("cardquota", resultMap.get("CARD_Quota"));
                        data.setData("quotaInterest", "0");
                        data.setData("billkey", resultMap.get("CARD_BillKey"));
                        data.setData("authentification", "00");

                        logger.info("Requesting INICIS payment");
                        INIdata resultData = inipay.payRequest(data);
//                        logger.info("INICIS RESULT DATA : " + resultData.toString());
                        logger.info("INICIS RESULT TID : " + resultData.getData("tid"));

                        /* GET PAYMENT INFORMATION FOR LOGGING */
                        PaymentVo payment = new PaymentVo();
                        LogPay logPay = new LogPay();

                        logPay.setPaymentType("Payment");

                        payment.setUserNo(user.getId());
                        logPay.setUserId(user.getId());

                        payment.setPayment(resultData.getData("PayMethod"));
                        logPay.setPayment(resultData.getData("PayMethod"));

                        payment.setIssuer(resultData.getData("CardResultCode"));

                        MasterCode masterCode = masterCodeRepository.findByCategory3(resultData.getData("CardResultCode"));
                        logPay.setCode(masterCode);

                        payment.setBillingKey(resultData.getData("billkey"));
                        logPay.setBillKey(resultData.getData("billkey"));

                        payment.setCardNo(resultData.getData("CardResultNumber") + "****");
                        logPay.setNumber(resultData.getData("CardResultNumber") + "****");

                        payment.setStatus(resultData.getData("ResultCode"));
                        logPay.setStatus(0);

                        Product product = productRepository.findByName(resultData.getData("goodname"));
                        payment.setGoodCode(product.getId());
                        logPay.setProduct(product.getId());

                        payment.setPrice(Long.parseLong(resultData.getData("Price")));
                        logPay.setPrice(Long.parseLong(resultData.getData("Price")));

                        payment.setPano(resultData.getData("moid"));
                        logPay.setPaNo(resultData.getData("moid"));

                        payment.setPayDate(resultData.getData("PGauthdate"));
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

                        payment.setDateFrom(sdf.format(cal.getTime()));
                        logPay.setDateFrom(LocalDateTime.ofInstant(cal.getTime().toInstant(),
                                ZoneId.systemDefault()).toLocalDate());

                        cal.add(Calendar.MONTH, 1);
                        LocalDate nextMonth = LocalDateTime.ofInstant(cal.getTime().toInstant(),
                                ZoneId.systemDefault()).toLocalDate();

                        payment.setDateTo(sdf.format(cal.getTime()));
                        logPay.setDateTo(nextMonth);

                        LocalDateTime now = DateUtils.localDateTimeNow();

                        logPay.setCreateDate(now);
                        logPay.setPaymentDate(now.toLocalDate());

                        payment.setTid(resultData.getData("tid"));


                        if (resultData.getData("ResultCode").equals("00")) {
//                            logger.info(payment.toString());

                            // Stored procedure
                            billingRepository.commonBilling(payment.getUserNo(), payment.getPayment(), payment.getIssuer(),
                                    payment.getBillingKey(), payment.getCardNo(), payment.getStatus(), payment.getGoodCode(),
                                    payment.getPrice(), payment.getPano(), payment.getPayDate(),
                                    payment.getDateFrom(), payment.getDateTo(), payment.getTid());
                            logger.info("@ commonBilling success");

//                            this.cancelPay(this.getUip(request), resultData.getData("tid"), user.getId());

                            // LogPay_t insert - 2019. 12. 26 - LYJ
                            logPayRepository.save(logPay);
                            logger.info("Insert LogPay_t @ billingPay success : userId " + logPay.getUserId());

                            // 사용자 payCount update - 2020. 01. 20 - LYJ
                            userRepository.increaseUserPayCount(user.getUserId(), 1L);
                            Integer userPayCount = userRepository.getPayCountbyUserId(user.getUserId());
                            logger.info(" @ billingPay payCount update success : " + user.getUserId() + " / update payCount = " + userPayCount);

                            MembershipRole membershipRole = membershipRoleRepository.findByName(MembershipRoleVo.ROLE_PAID)
                                    .orElseThrow(() -> new EntityNotFoundException());

                            // User Status update - 19. 09. 20 - LYJ
                            if (payment.getPrice() == 14900L) {
//                            if (payment.getPrice() == 1000L) {
                                userRepository.updateUserProduct(user.getId(), 1);
                                userRepository.updateUserMembershipRole(user.getUserId(), membershipRole);
                                userRepository.updateUserCharLimit(user.getUserId(), 70_000);
                            } else if (payment.getPrice() == 44000L) {
                                userRepository.updateUserProduct(user.getId(), 2);
                                userRepository.updateUserMembershipRole(user.getUserId(), membershipRole);
                                userRepository.updateUserCharLimit(user.getUserId(), 115_000);
                            } else if (payment.getPrice() == 98000L) {
                                userRepository.updateUserProduct(user.getId(), 3);
                                userRepository.updateUserMembershipRole(user.getUserId(), membershipRole);
                                userRepository.updateUserCharLimit(user.getUserId(), 250_000);
                            }

                            // billing_t paymentDate(다음 결제일) 설정 - 2019. 10. 19 LYJ
                            billingRepository.updatePaymentDate(user.getId(), nextMonth);
                            logger.info("@ update nex payment date --> {}", nextMonth);


                            LocalDate payDate = billingRepository.getPaymentDate(user.getId());

                            // 결제 성공 시 이동 할 url
                            RedirectView redirectView = new RedirectView(API_BASE_URL + "/billingSuccess");
                            redirectAttr.addFlashAttribute("payDate", payDate);

                            return redirectView;
                        } else {

                            logger.info("paymentVo : " + payment.toString());

                            String reason = resultData.getData("ResultDetailMsg");

                            StringTokenizer st = new StringTokenizer(reason, "|");
                            st.nextToken();

                            // LogPay_t insert - 2019. 12. 26 - LYJ
                            logPay.setFailReason(reason);
                            logPay.setStatus(-1);
                            logPayRepository.save(logPay);
                            logger.info("Insert LogPay_t @ billingPay fail : userNo" + logPay.getUserId());

                            RedirectView redirectView = new RedirectView(API_BASE_URL + "/billingFail");
                            redirectAttr.addAttribute("reason", reason);
                            redirectAttr.addAttribute("moid", "");

                            return redirectView;
                        }

                        //////////////////////////////////
                    } else {

                        //결제보안키가 다른 경우
                        if (!secureSignature.equals(resultMap.get("authSignature")) && "0000".equals(resultMap.get("resultCode"))) {

                            //auth signature 가 어떤 상황(사용자의 입력 정보 오류 or 시그니처 변경)에 의해 달라졌을 경우, 망취소
                            if ("0000".equals(resultMap.get("resultCode"))) {
                                throw new Exception("데이터 위변조 체크 실패");
                            }
                        }
                    }

                    // 수신결과를 파싱후 resultCode가 "0000"이면 승인성공 이외 실패
                    // 가맹점에서 스스로 파싱후 내부 DB 처리 후 화면에 결과 표시

                    // payViewType을 popup으로 해서 결제를 하셨을 경우
                    // 내부처리후 스크립트를 이용해 opener의 화면 전환처리를 하세요

                    //throw new Exception("강제 Exception");
                } catch (Exception ex) {

                    //####################################
                    // 실패시 처리(***가맹점 개발수정***)
                    //####################################

                    //---- db 저장 실패시 등 예외처리----//
                    logger.error(ex.getMessage());

                    //#####################
                    // 망취소 API
                    //#####################
                    //					String netcancelResultString = httpUtil.processHTTP(authMap, netCancel);	// 망취소 요청 API url(고정, 임의 세팅 금지)

                    ex.printStackTrace();
                }

            } else {
                //#############
                // 인증 실패시
                //#############
                logger.info("message : " + resultMap.get("fail"));
                logger.info("moid : " + resultMap.get("MOID"));

                RedirectView redirectView = new RedirectView(API_BASE_URL + "/billingFail");
                redirectAttr.addFlashAttribute("reason", resultMap.get("resultMsg"));
                redirectAttr.addFlashAttribute("moid", resultMap.get("MOID"));

                logger.info(resultMap.toString());

                return redirectView;
                //return "redirect:/support/krPricingFail";

            }

        } catch (Exception e) {
            logger.info(e.getMessage());
        }
        logger.info("message : " + resultMap.get("resultMsg"));
        logger.info("moid : " + resultMap.get("MOID"));

        RedirectView redirectView = new RedirectView(API_BASE_URL + "/billingFail");
        redirectAttr.addAttribute("reason", resultMap.get("resultMsg"));
        redirectAttr.addAttribute("moid", resultMap.get("MOID").replace("mindslab02_", ""));
        logger.info(resultMap.toString());

        return redirectView;
    }


    /**
     * 정기결제 진행
     * Modified by LYJ - 2019. 11. 12 - 결제 성공 시, 프로시저 'Routine_Bill'을 타지 않도록 변경
     */
    public void routineBill(PaymentVo paymentVo) throws Exception {
        INIpay inipay = new INIpay();
        INIdata data = new INIdata();
        Calendar cal = Calendar.getInstance();
        String mid = "mindslab02";

        int productNo = userRepository.getUserbyUserId(paymentVo.getUserEmail()).getProductId();
        Product product = productRepository.findById(productNo)
                .orElseThrow(() -> new EntityNotFoundException());
        Long routineBillPrice = product.getPrice();


        logger.info(" @ routineBill ==> productNo = " + productNo + " / routineBillPrice = " + routineBillPrice);

        LogPay logPay = new LogPay();
        logPay.setUserId(paymentVo.getUserNo());
        logPay.setPaymentType("Payment");
        logPay.setPayment("Card");
        logPay.setBillKey(paymentVo.getBillingKey());
        logPay.setPrice(routineBillPrice);
        logPay.setProduct(Math.toIntExact(productNo));


        data.setData("inipayHome", mPath_InipayHome);         // INIpay45 절대경로 (key / log 디렉토리) 추후 위치 수정 예정
        data.setData("logMode", "DEBUG");               // 로그모드 (INFO / DEBUG)
        data.setData("type", "reqrealbill");            // type (고정)
        data.setData("paymethod", "Card");              // 지불수단 (고정)
        data.setData("crypto", "execure");              // 암복호화모듈 (고정)

        data.setData("uip", "127.0.0.1");
        data.setData("url", API_BASE_URL);
        data.setData("mid", mid);
        data.setData("uid", mid);
        data.setData("keyPW", "1111");

        data.setData("moid", mid + "_" + cal.getTimeInMillis());

        String goodName = productRepository.findById(paymentVo.getGoodCode())
                .orElseThrow(() -> new EntityNotFoundException()).getName();
        data.setData("goodname", goodName);

        data.setData("price", paymentVo.getPrice() + "");
        //        data.setData("price", "1000");
        data.setData("currency", "WON");

        data.setData("buyername", paymentVo.getUserName());
        data.setData("buyertel", "010-0000-0000");
        data.setData("buyeremail", paymentVo.getUserEmail());

        data.setData("cardquota", "0");
        data.setData("quotaInterest", "0");
        data.setData("billkey", paymentVo.getBillingKey());
        data.setData("authentification", "00");

        logger.info("INICIS request DATA : {}", data);
        INIdata resultData = inipay.payRequest(data);
        logger.info("INICIS RESULT DATA : " + resultData.toString());

        if (resultData.getData("ResultCode").equals("00")) {
            logger.info("routinBillSuccess : " + paymentVo.toString());
            logger.info(resultData.toString());

            paymentVo.setGoodCode(productNo); //정기결제 상품 코드
            paymentVo.setPayment(resultData.getData("PayMethod"));
            paymentVo.setIssuer(resultData.getData("CardResultCode"));
            paymentVo.setCardNo(resultData.getData("CardResultNumber") + "****");
            paymentVo.setPrice(routineBillPrice); //정기결제 상품 가격

            StringBuilder sb = new StringBuilder(resultData.getData("PGauthdate"));
            sb.insert(4, '-');
            sb.insert(7, '-');

            paymentVo.setStatus(resultData.getData("ResultCode"));
            paymentVo.setPano(resultData.getData("moid"));
            paymentVo.setPayDate(sb.toString());

            logPay.setPaNo(resultData.getData("moid"));
            MasterCode masterCode = masterCodeRepository.findByCategory3(resultData.getData("CardResultCode"));
            logPay.setCode(masterCode);
            logPay.setNumber(resultData.getData("CardResultNumber") + "****");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            BillLog billLog = new BillLog();

            // 다음 결제일 (billing_t paymentDate, pay_t DateTo) update - 2019. 10. 22 - LYJ, LWJ
            // paymentDate(결제예정일) 조회
            LocalDate paymentDateTime = billingRepository.getPaymentDate(paymentVo.getUserNo());
            String paymentDateTimeStr = paymentDateTime.toString();

            paymentVo.setDateFrom(paymentDateTimeStr);
            billLog.setDateFrom(paymentDateTime);
            paymentVo.setCreateDate(paymentDateTimeStr);
            logPay.setDateFrom(LocalDate.parse(paymentDateTimeStr));
            cal.add(Calendar.MONTH, 1);

            LocalDate createDateTime = billingRepository.getCreateDate(paymentVo.getUserNo());
            int createDate = Integer.parseInt(createDateTime.toString().substring(8, 10));

            int lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH); //다음 달의 마지막 날짜

            if (lastDayOfMonth >= createDate) {
                cal.set(Calendar.DAY_OF_MONTH, createDate);
            } else {
                cal.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
            }


            paymentVo.setDateTo(sdf.format(cal.getTime()));
            billLog.setDateTo(LocalDateTime.ofInstant(cal.getTime().toInstant(),
                    ZoneId.systemDefault()).toLocalDate());

            LocalDate paymentDate = LocalDateTime.ofInstant(cal.getTime().toInstant(),
                    ZoneId.systemDefault()).toLocalDate();

            logPay.setDateTo(paymentDate);
            paymentVo.setTid(resultData.getData("tid"));

            billLog.setPaymentDate(LocalDateTime.ofInstant(cal.getTime().toInstant(),
                    ZoneId.systemDefault()).toLocalDate());

            User user = userRepository.findById(paymentVo.getUserNo()).orElseThrow(() -> new EntityNotFoundException());
            billLog.setUserId(user);

            billingRepository.updatePaymentDate(user.getId(), paymentDate);
            logger.info("@ update nex payment date --> {}", paymentDate);

            //billLog 설정 (billlog_t & billing_t)
            billLog.setPayment(resultData.getData("PayMethod"));

            MasterCode masterCode1 = masterCodeRepository.findByCategory3(resultData.getData("CardResultCode"));
            billLog.setCode(masterCode1);
            billLog.setBillKey(paymentVo.getBillingKey());
            billLog.setNumber(resultData.getData("CardResultNumber") + "****");
            billLog.setResult("00");

            Product product1 = productRepository.findById(productNo).orElseThrow(() -> new EntityNotFoundException());
            billLog.setProduct(product1);
            billLog.setPrice(routineBillPrice);
            billLog.setAcCode(resultData.getData("moid"));
            billLog.setTid(resultData.getData("tid"));

            /**
             * 프로시저 대신 수행할 기능들 - 2019. 11. 12. - LYJ
             * 1. billlog_t insert --> billLog
             * 2. billing_t update --> billLog
             * 3. pay_y insert --> PaymentVo
             * 4. user_t update --> billLog
             */
            billLogRepository.save(billLog);
            logger.info("@ save new billing success");


            billingRepository.updateBillingInfo(billLog.getBillKey(), billLog.getProduct().getId(), billLog.getPayment()
                    , billLog.getCode().getCategory3(), billLog.getNumber(), billLog.getPaymentDate(), DateUtils.localDateTimeNow(), billLog.getUserId().getId());

            payRepository.insertPayInfo(paymentVo.getGoodCode(), paymentVo.getPayment(), paymentVo.getIssuer()
                    , paymentVo.getCardNo(), LocalDate.parse(paymentVo.getDateFrom()), LocalDate.parse(paymentVo.getDateTo())
                    , paymentVo.getPrice(), paymentVo.getUserNo(), paymentVo.getStatus(), paymentVo.getTid(), paymentVo.getPano()
                    , LocalDate.parse(paymentVo.getPayDate()), DateUtils.localDateTimeNow());

            logger.info("@ save new pay info success");

            // LogPay_t insert - 2019. 12. 26 - LYJ
            logPay.setStatus(0);
            logPayRepository.save(logPay);
            logger.info("Insert LogPay_t @ routineBill success : userNo" + logPay.getUserId());

            // 사용자 payCount update - 2020. 01. 20 - LYJ
            userRepository.increaseUserPayCount(paymentVo.getUserEmail(), 1L);
//            this.cancelPay("127.0.0.1", resultData.getData("tid"), paymentVo.getUserNo());

        } else {
            logger.info("routineBillFail : " + paymentVo.toString());
            logger.info(resultData.toString());

            paymentVo.setFailReason(resultData.get("ResultDetailMsg").toString());
            billingRepository.updateBillingFail(paymentVo.getUserNo(), paymentVo.getFailReason());

            // LogPay_t update - 2019. 12. 26 - LYJ
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            LocalDate paymentDateTime = billingRepository.getPaymentDate(paymentVo.getUserNo());
            String paymentDateStr = paymentDateTime.toString();
            cal.set(Integer.parseInt(paymentDateStr.split("-")[0]), Integer.parseInt(paymentDateStr.split("-")[1]) - 1, Integer.parseInt(paymentDateStr.split("-")[2]));
            logPay.setDateFrom(paymentDateTime);
            cal.add(Calendar.MONTH, 1);
            logPay.setDateTo(LocalDateTime.ofInstant(cal.getTime().toInstant(),
                    ZoneId.systemDefault()).toLocalDate());

            logPay.setFailReason(resultData.get("ResultDetailMsg").toString());
            logPay.setStatus(-1);
            logPayRepository.save(logPay);
            logger.info("Insert LogPay_t @ routineBill fail : " + paymentVo.getUserEmail());
        }
    }

    @Override
    public List<PaymentVo> getRoutineUserList(LocalDate currentDate) {
        Query jpaQuery = em.createNativeQuery(
                "select b.USER_ID as userNo, b.BILL_KEY as billingKey, u.NAME as userName, u.USER_ID as userEmail," +
                        " b.PRODUCT as goodCode , p.PRICE as price FROM BILLING b\n" +
                        " inner join USER u on b.USER_ID = u.ID\n" +
                        " inner join PRODUCT p ON b.PRODUCT = p.ID\n" +
                        " where b.ACTIVE = 1 and (b.PRODUCT = 1 OR b.PRODUCT = 2 or b.PRODUCT = 3) " +
                        "and b.PAYMENT_DATE <= " + "'" + currentDate.toString() + "'"
                , "RoutineBillingMapper"
        );

        return jpaQuery.getResultList();
    }

    @Override
    public String getUip(HttpServletRequest request) {

        String uip = request.getHeader("X-FORWARDED-FOR");

        if (uip == null) {
            uip = request.getHeader("Proxy-Client-IP");
        }

        if (uip == null) {
            uip = request.getHeader("WL-Proxy-Client-IP");
        }

        if (uip == null) {
            uip = request.getHeader("HTTP_CLIENT_IP");
        }

        if (uip == null) {
            uip = request.getHeader("HTTP_X_FORWARDED_FOR");
        }

        if (uip == null) {
            uip = request.getRemoteAddr();
        }

        return uip;
    }

    public String getFilteredPayments(PaymentFilterRequestVo filterRequest) {
        List<String> whereCause = new ArrayList<String>();
        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append(
                "    SELECT\n" +
                        "      ROW_NUMBER() OVER( ORDER BY p.CREATE_DATE ASC) AS ROW_NUM,\n" +
                        "      p.ID,\n" +
                        "      u.NAME,\n" +
                        "      u.USER_ID as userId,\n" +
                        "      u.PHONE_NUMBER as phoneNumber,\n" +
                        "      u.COMPANY,\n" +
                        "      coalesce(p.PAYMENT, 'N/A') as payment,\n" +
                        "      coalesce(pName.NAME, 'Free') as productName,\n" +
                        "      coalesce(p.PRICE, 0) as price,\n" +
                        "      'KRW' as CURRENCY,\n" +
                        "      coalesce(p.CREATE_DATE, 'N/A') as createDate,\n" +
                        "      coalesce(p.CANCELLED_DATE, 'N/A') as cancelledDate,\n" +
                        "      coalesce(mT.NAME, 'N/A')  as cardName,\n" +
                        "      p.TID,\n" +
                        "      p.PA_NO as paNo,\n" +
                        "      case when p.STATUS = '00' then '승인' when p.STATUS = '99' then '취소' else 'N/A' end as STATUS,\n" +
                        "      u.PAY_COUNT as payCount,\n" +
                        "      coalesce(p.PAYMENT_DATE, 'N/A') as paymentDate,\n" +
                        "      case when CONVERT(DATE_ADD(NOW(), INTERVAL -1 MONTH), CHAR) <= p.PAYMENT_DATE\n" +
                        "      and p.PAYMENT_DATE <= CONVERT(NOW(), CHAR)\n" +
                        "      and p.STATUS = '00' then 1 else null end as canCancel\n" +
                        "    FROM\n" +
                        "      USER u\n" +
                        "      LEFT OUTER JOIN PAY p ON p.USER_ID = u.ID\n" +
                        "      LEFT OUTER JOIN MASTER_CODE mT ON p.ISSUER = mT.CATEGORY3\n" +
                        " --     and mt.CATEGORY1 = '결제'\n" +
                        "     LEFT OUTER JOIN BILLING b ON p.USER_ID = b.USER_ID\n" +
                        "      LEFT OUTER JOIN PRODUCT pName ON p.PRODUCT = pName.ID\n" +
                        "    WHERE\n" +
                        "      1 = 1 ");


        if (filterRequest.isFiltered()) {
            queryBuilder.append("AND ");

            if (!filterRequest.getUserEmail().isEmpty()) {
                whereCause.add("u.USER_ID = '" + filterRequest.getUserEmail() + "'");
            }

            if (!filterRequest.getUserCompany().isEmpty()) {
                whereCause.add("u.COMPANY = '" + filterRequest.getUserCompany() + "'");
            }

            if (!filterRequest.getUserName().isEmpty()) {
                whereCause.add("u.NAME = '" + filterRequest.getUserName() + "'");
            }
            if (!filterRequest.getPayCount().isEmpty()) {
                if (filterRequest.getPayCountRange().equals("GTE")) {
                    whereCause.add("u.PAY_COUNT >= " + filterRequest.getPayCount());
                } else if (filterRequest.getPayCountRange().equals("LTE")) {
                    whereCause.add("u.PAY_COUNT <= " + filterRequest.getPayCount());
                } else {
                    whereCause.add("u.PAY_COUNT = " + filterRequest.getPayCount());
                }
            }

            if (!filterRequest.getRoleName().isEmpty()) {
                if (filterRequest.getRoleName().equals("ROLE_FREE")) {
                    whereCause.add("u.ROLES = " + "1");
                } else if (filterRequest.getRoleName().equals("ROLE_PAID")) {
                    whereCause.add("u.ROLES <= " +  "2");
                }
            }

            if (!filterRequest.getSelectedStatus().isEmpty()) {
                whereCause.add("p.STATUS = '" + filterRequest.getSelectedStatus() + "'");
            }
            if (!filterRequest.getSelectPayment().isEmpty()) {
                whereCause.add("p.PAYMENT = '" + filterRequest.getSelectPayment() + "'");
            }

            if (!filterRequest.getProductName().isEmpty()) {
                if (filterRequest.getProductName().equals("Free")) {
                    whereCause.add("u.PRODUCT_ID = 4");
                } else if(filterRequest.getProductName().equals("Basic")){
                    whereCause.add("u.PRODUCT_ID = 1");
                }else if(filterRequest.getProductName().equals("Pro")){
                    whereCause.add("u.PRODUCT_ID = 2");
                }else if(filterRequest.getProductName().equals("Proplus")){
                    whereCause.add("u.PRODUCT_ID = 3");
                } else {
                    whereCause.add("u.PRODUCT_ID = 5");
                }

            }

            if(filterRequest.isLatestOnly()) {
                if (filterRequest.getProductName().isEmpty()) {
                    whereCause.add("p.CREATE_DATE = (select max(CREATE_DATE) from PAY p2 WHERE p.USER_ID = p2.USER_ID) OR u.PRODUCT_ID=4");
                } else {
                    whereCause.add("p.CREATE_DATE = (select max(CREATE_DATE) from PAY p2 WHERE p.USER_ID = p2.USER_ID)");
                }
            }

            queryBuilder.append(StringUtils.join(whereCause, " AND "));
        }

        Query jpaQuery = em.createNativeQuery(queryBuilder.toString(), "GetPaymentsMapping");
        List<PaymentFilterResponseVo> filteredResults = jpaQuery.getResultList();
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.writeValueAsString(filteredResults.toArray());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Override
    public String getPaymentMethod(Long userId) {
        return billingRepository.getPaymentMethod(userId);
    }

    public HashMap<String, Object> cancelPay(String uip, String tid, Long userId) {
        logger.info("Cancelling pay for user --> {}", userId);
        HashMap<String, Object> resultMsgMap = new HashMap<>();

        INIpay inipay = new INIpay();
        INIdata data = new INIdata();

        data.setData("inipayHome", mPath_InipayHome);         // INIpay45 절대경로 (key / log 디렉토리) 추후 위치 수정 예정
        data.setData("logMode", "DEBUG");               // 로그모드 (INFO / DEBUG)
        data.setData("type", "cancel");                 // type (고정)
        data.setData("crypto", "execure");
        data.setData("uip", uip);
//        data.setData("url", "https://maum.ai");
        data.setData("url", API_BASE_URL);
        data.setData("mid", "mindslab02");
        data.setData("uid", "mindslab02");
        data.setData("keyPW", "1111");
        data.setData("tid", tid);

        INIdata resultData = inipay.payRequest(data);
        logger.info(resultData.toString());

        logger.info("결제 취소 결과 : {}", resultData);

        String resultCode = resultData.getData("ResultCode");
        String resultMsg = resultData.getData("ResultMsg");



        try {
            HashMap<String, Object> map = new HashMap<>();
            LogPay logPay = new LogPay();
            logger.info("############pay {}", tid);
            Pay pay = payRepository.findByTid(tid);
            if (resultCode.equals("00")) {
                logger.info("이니시스 결제 취소 성공");
                logger.info("pay {}", pay);


                logPay.setPaNo(pay.getPaNo());
                logPay.setUserId(pay.getUserId().getId());
                logPay.setPaymentType("cancel");
                logPay.setPayment(pay.getPayment());
                logPay.setCode(pay.getIssuer());
                logPay.setBillKey(null);
                logPay.setNumber(pay.getCardNo());
                logPay.setCreateDate(DateUtils.localDateTimeNow());
                logPay.setStatus(0);
                logPay.setFailReason(null);
                logPay.setProduct(pay.getProduct().getId());
                logPay.setPrice(pay.getPrice());
                logPay.setPrice(pay.getPrice());
                logPay.setDateFrom(pay.getDateFrom());
                logPay.setDateTo(pay.getDateTo());
                logPay.setPaymentDate(pay.getPaymentDate());

                payRepository.updatePayToCancel(DateUtils.localDateNow(), userId, tid); // pay_T update
                logPayRepository.save(logPay); // LogPay insert

                resultMsgMap.put("status", "SUCCESS");
                return resultMsgMap;

            } else {
                logger.error("이니시스 결제 취소 실패 :: {}", resultMsg);

                logPay.setPaNo(pay.getPaNo());
                logPay.setUserId(pay.getUserId().getId());
                logPay.setPaymentType("cancel");
                logPay.setPayment(pay.getPayment());
                logPay.setCode(pay.getIssuer());
                logPay.setBillKey(null);
                logPay.setNumber(pay.getCardNo());
                logPay.setCreateDate(DateUtils.localDateTimeNow());
                logPay.setStatus(-1);
                logPay.setFailReason(resultMsg);
                logPay.setProduct(pay.getProduct().getId());
                logPay.setPrice(pay.getPrice());
                logPay.setDateFrom(pay.getDateFrom());
                logPay.setDateTo(pay.getDateTo());
                logPay.setPaymentDate(pay.getPaymentDate());

                logPayRepository.save(logPay); // LogPay insert

                resultMsgMap.put("status", "FAIL");
                throw new TTSMakerApiException(HttpStatus.INTERNAL_SERVER_ERROR, "ERRJ0011", "결제 취소 중 오류가 발생했습니다. : ");
            }
        } catch (Exception e) {
            logger.error("{}", e);
            resultMsgMap.put("status", "FAIL");
            throw new EntityNotFoundException("ERRJ0011 결제 취소 중 오류가 발생했습니다. : " + e.getMessage());
        }
    }

    @Override
    public Map<String, Object> planCancel(Long userNo) {

        logger.info("Plan cancelling for user --> {}", userNo);

        Map<String, Object> returnMap = new HashMap<String, Object>();

        User user = userRepository.findById(userNo).orElseThrow(() -> new EntityNotFoundException());

        int userProductId = user.getProductId();
//        if (userProductId == 5) { // ProductId 5 == UNSUBSCRIBING
//            try {
//
//                userRepository.updateUserProduct(userNo, 4);    // ProductId 4 == FREE
//
//                MembershipRole membershipRole = membershipRoleRepository.findByName(MembershipRoleVo.ROLE_FREE)
//                        .orElseThrow(() -> new EntityNotFoundException());
//                userRepository.updateUserMembershipRole(user.getUserId(), membershipRole);
//
//                billingRepository.planCancelProcedure(userNo);
//                billingRepository.deleteBillingByUserId(userNo); // 사용자의 결제 정보 삭제
//
//
//                logger.info("function planCancel's event : {}  --> unsubscribed", user.getUserId());
//                returnMap.put("msg", "SUCCESS");
//
//                return returnMap;
//
//            } catch (Exception e) {
//                logger.info("function planCancel's event : {}", e);
//                returnMap.put("msg", "FAIL");
//                returnMap.put("errMsg", e);
//
//                return returnMap;
//            }

//        } else {
        // User Status update 19. 09. 20 LYJ
        userRepository.updateUserProduct(userNo, 5);    // ProductId 4 == FREE

        /* 구독 해지 신청한 고객 안내 메일 -> 유료 고객 - 2019. 11. 15 - LYJ
         * Modified : 국문/영문 분기 - 2020. 05. 11 - LYJ
         * */
        try {


        } catch (Exception e) {
            logger.info("planCancel Error ==> User : " + user.getUserId() + ", Reason : " + e);
        }

        logger.info("function planCancel's event : --> unsubscribing");
        returnMap.put("msg", "SUCCESS");
        return returnMap;
    }
//    }

    @Override
    public LocalDate getUserPaymentExpirationDate(Long userNo) {
        return billingRepository.getPaymentDate(userNo);
    }

    public void planCancelProcedure(Long userNo) {
        billingRepository.planCancelProcedure(userNo);
    }

    public int deleteBillInfo(Long userNo) {
        return billingRepository.deleteBillingByUserId(userNo);
    }

    @Override
    public List<PaymentVo> getUnpaymentList() {
        Query jpaQuery = em.createNativeQuery(
                "select\n" +
                        " b.USER_ID as userNo,\n" +
                        " u.USER_ID as userEmail, b.PAYMENT_DATE as paymentDate\n" +
                        " from BILLING b\n" +
                        " inner join USER u ON b.USER_ID = u.ID where\n" +
                        " ( u.PRODUCT_ID = 1 OR u.PRODUCT_ID = 2 OR PRODUCT_ID = 3 ) and b.PAYMENT = 'Card'\n" +
                        " and b.ACTIVE = 1\n" +
                        " and DATEDIFF(NOW(),  b.PAYMENT_DATE ) >= 5 "
                , "UnpaymentMapper"
        );

        return jpaQuery.getResultList();
    }
}
