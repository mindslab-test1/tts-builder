package ai.mindslab.ttsmaker.api.payment.vo;

import ai.mindslab.ttsmaker.api.job.domain.JobM;
import ai.mindslab.ttsmaker.api.job.vo.response.JobListVo;
import lombok.Data;

@Data
public class LogPayVo {
    private String paNo;
    private Long userNo;
    private String type;
    private String payment;
    private String code;
    private String billKey;
    private String cardNumber;
    private String createDate;
    private int status;
    private String failReason;
    private int product;
    private int price;
    private String dateFrom;
    private String dateTo;
    private String paymentDate;

}
