package ai.mindslab.ttsmaker.api.job.repository;

import ai.mindslab.ttsmaker.api.job.domain.TTSFileDownload;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface TTSFileDownloadRepository extends JpaRepository<TTSFileDownload, Long> {
    List<TTSFileDownload> findAllByUserId(String userId);

    Optional<TTSFileDownload> findById(Long id);
}
