package ai.mindslab.ttsmaker.api.payment.controller;

import ai.mindslab.ttsmaker.api.login.domain.User;
import ai.mindslab.ttsmaker.api.login.repository.MembershipRoleRepository;
import ai.mindslab.ttsmaker.api.login.repository.UserRepository;
import ai.mindslab.ttsmaker.api.payment.vo.BillingFormVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;
import ai.mindslab.ttsmaker.api.payment.service.PaymentService;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/payment")
public class PaymentController {

    final PaymentService paymentService;

    final MembershipRoleRepository membershipRoleRepository;

    final UserRepository userRepository;

    private static Logger logger = LoggerFactory.getLogger(PaymentController.class.getName());

    public PaymentController(PaymentService paymentService, MembershipRoleRepository membershipRoleRepository, UserRepository userRepository) {
        this.paymentService = paymentService;
        this.membershipRoleRepository = membershipRoleRepository;
        this.userRepository = userRepository;
    }

    @GetMapping("/billingForm")
    public ResponseEntity<BillingFormVo> basicBilling(@RequestParam String method,
                                                      @RequestParam String p,
                                                      @RequestParam String userId) throws Exception {
        logger.info("[Basic Billing] : {} {} {}", method, p, userId);
        return paymentService.basicBilling(method, p, userId);
    }

    /*
     ** Date: 2019. 07. 12. (By belsnake)
     ** Desc: 요금 정책 변경으로 첫달 무료, 이후 정기 결제로 변경됨.
     */
    @PostMapping(value = "/billingPay")
    public RedirectView billingPay(@RequestParam(value = "email") String email,
                                   HttpServletRequest request, RedirectAttributes redirectAttr) {
        logger.info("[Billing Pay] : {}", email);

        RedirectView redirectView = paymentService.billingPay(email, request, redirectAttr);
//        RedirectView redirectView = new RedirectView(redirectUrl);
        return redirectView;
    }

    @RequestMapping(value = "/close")
    public String close() {
        logger.info("[Close Billing]");

        return "<script type=\"text/javascript\">\n" +
                "    parent.INIStdPay.viewOff();\n" +
                "    location.reload()\n" +
                "</script>";
    }

    @RequestMapping("/planCancel/{userId}")
    @ResponseBody
    public Map<String, Object> planCancel(HttpServletRequest request, @PathVariable String userId) {

        logger.info("{} .planCancel() ", getClass().getSimpleName());

        User user = userRepository.findByUserId(userId)
                .orElseThrow(()-> new EntityNotFoundException("구독 취소중 유저 아이디 조회에 실패하였습니다."));

        return paymentService.planCancel(user.getId());
    }
}
