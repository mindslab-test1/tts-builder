package ai.mindslab.ttsmaker.api.job.vo.request;

import ai.mindslab.ttsmaker.api.job.domain.JobSpeaker;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import ai.mindslab.ttsmaker.api.speaker.domain.Speaker;

import java.util.Optional;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobSpeakerSaveVo {
	private String jobId;		// JOB ID
	private String speakerId;	// 화자 ID
	
	@JsonIgnore
	public static JobSpeakerSaveVo of(JobSpeaker entity) {
		Speaker speaker = Optional.ofNullable(entity.getSpeaker()).orElse(new Speaker());
		return JobSpeakerSaveVo.builder()
        		.jobId(entity.getJobId())
        		.speakerId(speaker.getSpeakerId())
        		.build();
    }
}
