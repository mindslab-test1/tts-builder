package ai.mindslab.ttsmaker.api.job.vo.response;

import ai.mindslab.ttsmaker.api.job.domain.JobM;
import lombok.*;

import ai.mindslab.ttsmaker.util.DateUtils;

@Builder(toBuilder = true)
@ToString
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class JobListVo {
	private long id;
	private String jobId;
	private String jobTitle;
	private long orderSeq;
	private String updatedAt;
	
	public static JobListVo of(JobM entity) {
        return JobListVo.builder()
        		.id(entity.getId())
        		.jobId(entity.getJobId())
        		.jobTitle(entity.getJobTitle())
        		.orderSeq(entity.getOrderSeq())
        		.updatedAt(DateUtils.asLocalDate(entity.getUpdatedAt()))
        		.build();
    }
}
