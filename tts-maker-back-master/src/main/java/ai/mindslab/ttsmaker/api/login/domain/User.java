package ai.mindslab.ttsmaker.api.login.domain;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import ai.mindslab.ttsmaker.api.payment.domain.BillLog;
import ai.mindslab.ttsmaker.api.payment.domain.Billing;
import ai.mindslab.ttsmaker.api.payment.domain.Pay;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "USER", uniqueConstraints = {@UniqueConstraint(name = "UK_USER_ID", columnNames = {"USER_ID"})})
public class User {

    //관리자번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private Long id;

    //user ID
    @Column(name = "USER_ID", columnDefinition = "varchar(100)")
    private String userId;

    //user PW
    @Column(name = "USER_PW", columnDefinition = "varchar(100)")
    private String userPw;

    //권한
    @Column(name = "AUTH", columnDefinition = "int(11)")
    private int auth;

    //마스터 계정 승인
    @Column(name = "CONFIRM", columnDefinition = "int(11)")
    private int confirm;

    //이름
    @Column(name = "NAME", columnDefinition = "varchar(100)")
    private String name;

    @ManyToOne
    @JoinColumn(name = "ROLES")
    private MembershipRole membershipRole;


    //최대 글자수
    @Column(name = "CHAR_LIMIT", columnDefinition = "int(8)")
    private Integer charLimit;

    //사용 글자수
    @Column(name = "CHAR_CONSUMED", columnDefinition = "int(8)")
    private Integer charConsumed;

    //회사
    @Column(name = "COMPANY", columnDefinition = "varchar(20)")
    private String company;

    //전화번호
    @Column(name = "PHONE_NUMBER", columnDefinition = "varchar(11)")
    private String phoneNumber;

//    //멤버쉽
//    @Enumerated(EnumType.STRING)
//    @Column(name = "MEMBERSHIP", columnDefinition = "varchar(20) default 'FREE'")
//    private UserMembership membership;

    //Pay Product
    @Column(name = "PRODUCT_ID", columnDefinition = "int(8)")
    private int productId;

    //Pay Product
    @Column(name = "PAY_COUNT", columnDefinition = "int(3) default 0")
    private Long payCount = 0L;

    //등록일시
    @Column(name = "CREATE_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createDate;

    //수정일시
    @Column(name = "UPDATE_DATE")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedDate;

    @EqualsAndHashCode.Exclude
    @OneToOne(mappedBy = "userId")
    private Billing billing;

    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "userId")
    private List<Pay> payList;

    @OneToMany(mappedBy = "userId")
    @EqualsAndHashCode.Exclude
    private List<BillLog> billLogs;

}
