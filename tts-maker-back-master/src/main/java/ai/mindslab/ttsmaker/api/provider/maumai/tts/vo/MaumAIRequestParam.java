package ai.mindslab.ttsmaker.api.provider.maumai.tts.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MaumAIRequestParam {
	private String apiId;
	private String apiKey;
	private String voiceName;
	private String text;
	private Float speed;
	private String requestName;
	private String downloadId;
}
