package ai.mindslab.ttsmaker.api.login.service;

import ai.mindslab.ttsmaker.api.login.vo.UserVo;
import org.springframework.http.ResponseEntity;
import ai.mindslab.ttsmaker.api.login.domain.User;

import javax.servlet.http.HttpSession;
import java.util.List;

public interface LoginService {

    public ResponseEntity<?> userCheck(UserVo userVo, HttpSession session);

    public boolean idCheck(String id);

    public ResponseEntity registUser(User user);

    public List<User> waitingUser();

    public String registConfirm(User user);

    public String registReject(User user);

    ResponseEntity<?> reauth(String token);

    ResponseEntity<?> logout(String token);

}
