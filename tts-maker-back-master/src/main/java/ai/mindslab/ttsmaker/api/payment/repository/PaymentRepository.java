package ai.mindslab.ttsmaker.api.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ai.mindslab.ttsmaker.api.payment.domain.Payment;

public interface PaymentRepository extends JpaRepository<Payment, Long> {
}
