package ai.mindslab.ttsmaker.api.login.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReauthRespVo {
    String token;
    String userId;
}
