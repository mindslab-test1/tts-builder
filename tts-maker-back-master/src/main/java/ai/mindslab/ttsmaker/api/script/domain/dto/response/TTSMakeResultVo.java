package ai.mindslab.ttsmaker.api.script.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ai.mindslab.ttsmaker.api.provider.maumai.tts.vo.MaumAIResponseFile;

import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TTSMakeResultVo {
	private String requestId;		// 요청 UUID
	private String voiceName;		// 화자 
	private String text;				// TTS Text
	private int statusCd;			// 프로바이더와의 통신 상태
	private String statusNm;			// 프로바이더와의 통신 상태
	private String downloadURL;		// 파일을 다운로드 받을 수 있는 경로
	private String expiryDate; 		// 파일 다운로드 만료일자 (yyyy-MM-dd 형태로 출력)
	
	public static TTSMakeResultVo of(String voiceName, String text, MaumAIResponseFile result) {
		return TTSMakeResultVo.builder()
				.requestId(UUID.randomUUID().toString())
				.voiceName(voiceName)
				.text(text)
				.statusCd(result.getMessage().getStatus())
				.statusNm(result.getMessage().getMessage())
				.downloadURL(result.getDownloadURL())
				.expiryDate(result.getExpiryDate())
				.build();
	}
}
