package ai.mindslab.ttsmaker.api.login.vo;

public enum MembershipRoleVo {
    ROLE_FREE, ROLE_PAID, ROLE_ADMIN
}
