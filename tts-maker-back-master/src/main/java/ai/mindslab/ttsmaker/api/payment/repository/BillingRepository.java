package ai.mindslab.ttsmaker.api.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ai.mindslab.ttsmaker.api.payment.domain.Billing;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Repository
public interface BillingRepository extends JpaRepository<Billing, Long> {
    @Transactional
    @Procedure("Bill")
    void commonBilling(@Param("p_userid") Long userId, @Param("p_payment") String payment, @Param("p_code") String code,
                       @Param("p_billkey") String billKey, @Param("p_num") String num,
                       @Param("p_result") String result, @Param("p_product") int product,
                       @Param("p_price") long price, @Param("p_AcCode") String acCode,
                       @Param("p_pDate") String pdate, @Param("p_SdateFrom") String sDateFrom,
                       @Param("p_SdateTo") String sDateTo, @Param("p_tid") String tid);

    @Transactional
    @Procedure("Bill_Plan_Cancel")
    void planCancelProcedure(@Param("p_userid") Long userId);

    @Transactional
    @Modifying
    @Query("Update Billing b set b.paymentDate=?2 Where b.userId.id = ?1")
    int updatePaymentDate(Long userNo, LocalDate paymentDate);

    @Transactional
    @Modifying
    @Query("delete from Billing b where b.userId.id = ?1")
    int deleteBillingByUserId(Long userId);

//    @Query("select b.userId, b.billKey, b.product" +
//             " FROM Billing b " +
//            "inner join b.userId u on b.userId.id = u.id " +
//            "inner join b.product p on b.product.id = p.id " +
//            "where b.active = 1 and (b.product.id = 1 OR b.product.id = 2 or b.product.id = 3)" +
//            "            and b.paymentDate <= ?1")
//    List<Billing> getRoutineUserList(LocalDate currentDate);

    @Query("select b.payment from Billing b Where b.userId.id = ?1")
    String getPaymentMethod(Long userId);

    @Query(value = "select PAYMENT_DATE from BILLING where USER_ID = ?1",
    nativeQuery = true)
    LocalDate getPaymentDate(Long userId);

    @Query("select b.createDate from Billing b where b.userId.id = ?1")
    LocalDate getCreateDate(Long userId);

    @Transactional
    @Modifying
    @Query("update Billing b set b.billKey = ?1, b.product.id = ?2, b.payment = ?3" +
            ", b.issuer.category3 = ?4, b.number = ?5, b.paymentDate = ?6, b.updateDate = ?7" +
            ", b.updateUser = ?8, b.failReason = null where b.userId.id = ?8 ")
    void updateBillingInfo(String billKey, int id, String payment, String code, String number, LocalDate paymentDate, LocalDateTime updateDate, Long id1);

    @Transactional
    @Modifying
    @Query("update Billing b set b.failReason = ?2 where b.userId.id = ?1 ")
    void updateBillingFail(Long userNo, String failReason);
}
