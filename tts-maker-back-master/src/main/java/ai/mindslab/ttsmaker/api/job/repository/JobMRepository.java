package ai.mindslab.ttsmaker.api.job.repository;

import ai.mindslab.ttsmaker.api.job.domain.JobM;
import com.google.common.base.Optional;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.EntityGraph.EntityGraphType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JobMRepository extends JpaRepository<JobM, Long> {
	
	@Query("select p from JobM p where p.isDeleted = 'N'")
	List<JobM> getActiveJobList(Sort sort);

	@Query("select p from JobM p where p.isDeleted = 'N' and p.userId = ?1")
	List<JobM> getActiveJobListByUserId(String userId, Sort sort);
	
	@EntityGraph(attributePaths = {"jobSpeakers"}, type = EntityGraphType.LOAD)  
	@Query("select p from JobM p where p.isDeleted = 'N' and p.jobId = ?1")
	JobM getJobContentFindByJobId(String jobId);
	
	@Query("select p from JobM p where p.isDeleted = 'N' and p.jobId = ?1")
	Optional<JobM> getJobFindByJobId(String jobId);

    @Query("select p from JobM p where p.jobTitle = ?1 and p.userId = ?2")
    JobM getJobTitle(String jobTitle, String userId);

	@EntityGraph(attributePaths = {"jobSpeakers"}, type = EntityGraphType.LOAD)
	@Query("select p from JobM p where p.jobTitle = ?1 and p.userId = ?2")
	JobM getJobByTitle(String jobTitle, String userId);

	int deleteByIsDeleted(String isDeleted);
}
