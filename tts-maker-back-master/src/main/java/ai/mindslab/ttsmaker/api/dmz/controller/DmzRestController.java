package ai.mindslab.ttsmaker.api.dmz.controller;

import org.springframework.web.bind.annotation.*;
import ai.mindslab.ttsmaker.api.dmz.service.DmzService;
import ai.mindslab.ttsmaker.api.dmz.vo.request.DmzUserIdVo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/dmz")
public class DmzRestController {

    private final DmzService dmzService;

    public DmzRestController(DmzService dmzService) {
        this.dmzService = dmzService;
    }

    @ResponseBody
    @PostMapping(value = "/userCategoryList")
    public Object userCategoryList(@RequestBody DmzUserIdVo dmzUserIdVo, HttpServletRequest request, HttpServletResponse response) {
        System.out.println(dmzService.getUserCategoryList(dmzUserIdVo.getUserId()));
        return dmzService.getUserCategoryList(dmzUserIdVo.getUserId());
    }
}
