package ai.mindslab.ttsmaker.api.dmz.service;

public interface DmzService {
    public Object getUserCategoryList(String userId);
}
