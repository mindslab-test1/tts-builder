package ai.mindslab.ttsmaker.api.admin.controller;


import ai.mindslab.ttsmaker.api.exception.TTSMakerApiException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ai.mindslab.ttsmaker.api.login.service.UserService;
import ai.mindslab.ttsmaker.api.payment.service.PaymentService;
import ai.mindslab.ttsmaker.api.payment.vo.PaymentFilterRequestVo;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.HashMap;

@RestController
@RequestMapping("/admin")
public class AdminController {
    final PaymentService paymentService;

    final UserService userService;

    private static Logger logger = LoggerFactory.getLogger(AdminController.class.getName());

    public AdminController(PaymentService paymentService, UserService userService) {
        this.paymentService = paymentService;
        this.userService = userService;
    }


    @ResponseBody
    @PostMapping(value = "/filteringInfo")
    public String filterInfo(@RequestBody PaymentFilterRequestVo filterRequest) throws IOException {
        String paymentPages = paymentService.getFilteredPayments(filterRequest);
        logger.info("[Filtering Payment Lists] : {}", filterRequest);
        return paymentPages;
    }

    /**
     * 결제 - 취소
     */
    @ResponseBody
    @PostMapping(value = "/payment/payCancel")
    public HashMap<String, Object> payCancel(HttpServletRequest request,
                                             @RequestParam(value = "tid") String cancelTid,
                                             @RequestParam(value = "userId") String userId) {
        logger.info("[Cancelling Pay] : user[{}], tid[{}]", userId, cancelTid);
        HashMap<String, Object> map = new HashMap<>();

        Long userNo = userService.getUserIdByUserEmail(userId);
        map = paymentService.cancelPay(paymentService.getUip(request), cancelTid, userNo);

        return map;
    }

}
