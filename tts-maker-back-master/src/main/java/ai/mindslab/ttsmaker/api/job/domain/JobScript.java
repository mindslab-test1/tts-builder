package ai.mindslab.ttsmaker.api.job.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import ai.mindslab.ttsmaker.api.speaker.domain.Speaker;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@DynamicInsert
@DynamicUpdate
@Entity
@Builder
@Table(name = "JOB_SCRIPT")
public class JobScript implements Serializable {
	
	private static final long serialVersionUID = 9182436812539983497L;

	//관리번호
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private Long id;
    
    //JOB ID
    @Column(name = "JOB_ID", columnDefinition = "varchar(36)", nullable = false)
    private String jobId;
    
    //SCRIPT_ID
    @Column(name = "SCRIPT_ID", columnDefinition = "varchar(36)", nullable = false)
    private String scriptId;
    
    //화자 ID
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SPEAKER_ID", columnDefinition = "varchar(36)", referencedColumnName = "SPEAKER_ID", nullable = false, foreignKey = @ForeignKey(name = "SPEAKER_FK2"))
    private Speaker speaker;
    
    // TTS 스크립트 Text
    @Column(name = "SCRIPT_TEXT", columnDefinition = "TEXT", nullable = false)
    private String scriptText;
    
    // TTS Resource URL정보
    @Column(name = "TTS_RESOURCE_URL", columnDefinition = "varchar(1000)")
    private String ttsResourceUrl;
    
    // TTS Resource 만료일
    @Column(name = "TTS_RESOURCE_EXPIRY_DATE", columnDefinition = "varchar(10)")
    private String ttsResourceExpiryDate;
    
    //음성 제어 스타일 정보
    @Column(name = "SETTINGS_STYLE", columnDefinition = "varchar(100)")
    private String settingsStyle;

    //음성 제어 볼륨 정보
    @Column(name = "SETTINGS_VOLUME", columnDefinition = "int(3)")
	private Integer settingsVolume;
    
    //음성 제어 스피드 정보
    @Column(name = "SETTINGS_SPEED", columnDefinition = "varchar(100)")
	private String settingsSpeed;

    //음성 제어 읽기 쉬기 정보
    @Column(name = "SETTINGS_SLEEP", columnDefinition = "int(8)")
	private Long settingsSleep;
    
    //정렬순서
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ORDER_SEQ", columnDefinition = "int(11)")
    private Long orderSeq;
    
    //삭제여부
    @Column(name = "DEL_YN", columnDefinition = "char(1) default 'N'")
    private String isDeleted;
    
    //TTS 상태
    @Column(name = "TTS_STATE", columnDefinition = "varchar(10) default 'INIT'")
    private String ttsState;
    
    //ROW 상태
    @Column(name = "ROW_STATE", columnDefinition = "varchar(10) default 'S'")
    private String rowState;
    
    //등록일시
    @Column(name = "INS_DT")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createdAt;

    //수정일시
    @Column(name = "UPD_DT")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime updatedAt;
}
