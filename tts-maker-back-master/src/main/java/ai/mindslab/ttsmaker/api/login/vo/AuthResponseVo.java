package ai.mindslab.ttsmaker.api.login.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthResponseVo {
    private String token;
    private String type = "Bearer";
    private Long id;
    private String username;
    private String roles;
    private int productId;
    private String pageUrl;
    private int charConsumed;
    private int charLimit;

    public AuthResponseVo(String accessToken, Long id, String username, String roles, String pageUrl, int productId, int charConsumed, int charLimit) {
        this.token = accessToken;
        this.id = id;
        this.username = username;
        this.roles = roles;
        this.pageUrl = pageUrl;
        this.productId = productId;
        this.charConsumed = charConsumed;
        this.charLimit = charLimit;
    }

}
