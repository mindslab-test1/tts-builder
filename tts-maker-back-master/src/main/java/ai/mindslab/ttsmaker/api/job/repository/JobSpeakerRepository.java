package ai.mindslab.ttsmaker.api.job.repository;

import ai.mindslab.ttsmaker.api.job.domain.JobSpeaker;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface JobSpeakerRepository extends JpaRepository<JobSpeaker, Long> {
	
	@Query("select p from JobSpeaker p where p.isDeleted = 'N' and p.jobId = ?1")
	List<JobSpeaker> getActiveSpeakerListFindByJobId(String jobId, Sort sort);
	
	@Query("select p from JobSpeaker p where p.jobId = ?1 and p.speaker.speakerId = ?2")
	Optional<JobSpeaker> getJobSpeakerFindByJobIdAndSpeakerId(String jobId, String speakerId);

	int deleteByIsDeleted(String isDeleted);
}
