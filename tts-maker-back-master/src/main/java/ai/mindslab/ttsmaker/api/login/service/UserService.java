package ai.mindslab.ttsmaker.api.login.service;

import ai.mindslab.ttsmaker.api.login.domain.MembershipRole;
import ai.mindslab.ttsmaker.api.login.vo.UserMembershipInfoVo;
import ai.mindslab.ttsmaker.api.login.domain.User;

import java.util.List;

public interface UserService {
    public int getCharConsumed(String userId);

    public void updateCharConsumed(String userId, int charConsumed);

    public UserMembershipInfoVo getUserMembershipInfo(String userId);

    public User getUserInfo(String userId);

    public void increaseUserPayCount(String userId, Long payCount);

    List<User> findAllByProductId(int i);

    int updateUserProduct(Long id, int prodId);

    int updateUserMembershipRole(String userEmail, MembershipRole membershipRole);

    Long getUserIdByUserEmail(String userId);
}
