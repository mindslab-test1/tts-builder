package ai.mindslab.ttsmaker.api.payment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ai.mindslab.ttsmaker.api.payment.domain.BillLog;

public interface BillLogRepository extends JpaRepository<BillLog, Long> {
}
