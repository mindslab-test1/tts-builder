package ai.mindslab.ttsmaker.api.payment.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "LOG_PAY")
public class LogPay {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", columnDefinition = "int(11)")
    private int id;

    @Column(name = "PA_NO", columnDefinition = "varchar(50)")
    private String paNo;

    @Column(name = "USER_ID", columnDefinition = "int(8)")
    private Long userId;

    @Column(name = "TYPE", columnDefinition = "varchar(10)")
    private String paymentType;

    @Column(name = "PAYMENT", columnDefinition = "varchar(50)")
    private String payment;

    @OneToOne
    @JoinColumn(name = "CODE", referencedColumnName = "CATEGORY3")
    private MasterCode code;

    @Column(name = "BILL_KEY", columnDefinition = "varchar(200)")
    private String billKey;

    @Column(name = "NUMBER", columnDefinition = "varchar(30)")
    private String number;

    //등록일시
    @Column(name = "CREATE_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDateTime createDate;

    @Column(name = "STATUS", columnDefinition = "int(1)")
    private Integer status;

    @Column(name = "FAIL_REASON", columnDefinition = "varchar(50)")
    private String failReason;

    @Column(name = "PRODUCT", columnDefinition = "int(11)")
    private int product;

    @Column(name = "PRICE", columnDefinition = "int(11)")
    private Long price;

//    @ManyToOne
//    @JoinColumn(name="PAY_ID", referencedColumnName = "ID")
//    private Pay pay;

    //등록일시
    @Column(name = "DATE_FROM")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate dateFrom;

    //수정일시
    @Column(name = "DATE_TO")
    @LastModifiedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate dateTo;

    @Column(name = "PAYMENT_DATE")
    @CreatedDate
    @JsonIgnore
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyyMMdd")
    private LocalDate paymentDate;
}
