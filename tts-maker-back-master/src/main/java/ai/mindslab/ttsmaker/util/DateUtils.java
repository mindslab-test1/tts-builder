package ai.mindslab.ttsmaker.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class DateUtils {
    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }
    public static Date asDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
    public static String asLocalDate(LocalDateTime date) {
        final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.KOREA);
        LocalDate ldate = date.toLocalDate();
        return ldate.format(formatter);
    }

    public static LocalDate localDateNow(){
        return LocalDate.now(ZoneId.of("Asia/Seoul"));
    }

    public static LocalDateTime localDateTimeNow(){
        return LocalDateTime.now(ZoneId.of("Asia/Seoul"));
    }

}