import Vue from 'vue';
import App from './App';
import router from './routes';
import store from './store';

import BootstrapVue from 'bootstrap-vue';
import "~assets/styles/common.scss"
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
Vue.use(BootstrapVue);

Vue.config.productionTip = false;


new Vue({
    router,
    store,
    el: '#app',
    render: h => h(App),
});