import Vue from 'vue';
import Router from 'vue-router';

import NotFoundComponent from '@/components/pages/NotFoundComponent.vue';
import store from '../store/index';
import Repository from "@/repositories/Repository";
import axios from "axios";

Vue.use(Router);

const rejectAuthUser = (to, from, next) => {
    if (store.state.isLogin === true) {
        //이미 로그인 된 유저
        alert('이미 로그인을 하였습니다')
        next('Make')
    } else {
        next()
    }
}

const onlyAuthUser = (to, from, next) => {
    const API_BASE_URL = process.env.API_BASE_URL;
    const reauthUrl = [API_BASE_URL, '/login/reauth'].join('');

    if (store.state.isLogin === false) {
        const token = localStorage.getItem("token")

        /* If token exists reauthenticate*/
        if (token) {
            axios.post(reauthUrl, {
                token: token
            }).then(res => {
                if (!res.data) {
                    alert("로그인을 다시 해주세요")
                    next('/')
                }
                localStorage.setItem("token", res.data.token)
                Repository.defaults.headers.common['Authorization'] = `Bearer ${res.data.token}`;
                store.dispatch("reauth", {
                    status: "success",
                    userId: res.data.username,
                    productId: res.data.productId,
                    roles: res.data.roles,
                    charConsumed: res.data.charConsumed,
                    charLimit: res.data.charLimit,
                })
                next()
            })
        } else {
            alert('로그인이 필요한 기능입니다.')
            next('/')

        }
    } else {
        next()
    }
}


const onlyMasterUser = (to, from, next) => {
    if (store.state.isLogin === true && store.state.isMaster === true) {
        next()
    } else {
        alert("마스터계정만 접근 가능합니다.")
        next('Make')
    }
}

const routes = [
    {
        path: '/',
        redirect: '/login'
    },
    {
        path: '/login',
        name: 'loginView',
        beforeEnter: rejectAuthUser,
        component: require('@/views/Login').default,
        meta: {
            title: 'TTS Builder LOGIN',
        }
    },
    {
        path: '/signUp',
        name: 'signUpView',
        component: require('@/views/signUpView').default,
        meta: {
            title: '회원가입'
        }
    },
    {
        path: '/terms',
        name: 'termsMain',
        component: require('@/views/page/TermsMain').default,
        meta: {
            title: '이용약관'
        }
    },
    {
        path: '/log',
        name: 'LogTableView',
        beforeEnter: onlyAuthUser,
        component: require('@/views/LogTableView').default,
        meta: {
            title: '로그'
        }
    },
    {
        path: '/master',
        name: 'masterView',
        beforeEnter: onlyMasterUser,
        component: require('@/views/masterView').default,
        meta: {
            title: '마스터'
        }
    },
    {
        path: '/Make',
        name: 'MakeHomeView',
        beforeEnter: onlyAuthUser,
        component: require('@/views/MakeHomeView').default,
        meta: {
            title: 'TTS 에디터',
        }
    },
    {
        path: '/membership',
        name: 'PaymentView',
        beforeEnter: onlyAuthUser,
        component: require('@/views/page/PaymentView').default,
        meta: {
            title: 'TTS 결제',
        }
    },
    {
        path: '/downloadlist',
        name: 'DownloadListView',
        beforeEnter: onlyAuthUser,
        component: require('@/views/page/DownloadListView').default,
        meta: {
            title: 'TTS 다운로드 내역',
        }
    },
    {
        path: '/adminPay',
        name: 'AdminView',
        beforeEnter: onlyAuthUser,
        component: require('@/views/admin/AdminView').default,
        meta: {
            title: 'TTS 어드민',
        }
    },
    {
        path: '/billingFail',
        name: 'BillingFailView',
        beforeEnter: onlyAuthUser,
        component: require('@/views/page/BillingFailView').default,
    },
    {
        path: '/billingSuccess',
        name: 'BillingSuccessView',
        beforeEnter: onlyAuthUser,
        component: require('@/views/page/BillingSuccessView').default,
    },
    {
        path: '/error',
        name: 'ErrorHandlingPage',
        component: require('@/views/error/Error').default,
        props: true,
    },
    {
        path: '*',
        component: require('@/views/error/Error').default,
        meta: {
            title: 'Not Found',
        },
        props: {
            'httpStatus' : 404,
            'message' : '페이지를 찾을 수 없습니다.',
        },
    }
];
const router = new Router({
    mode: 'history',
    routes: routes
});

const DEFAULT_TITLE = 'TTS Home';
router.afterEach((to, from) => {
    document.title = to.meta.title || DEFAULT_TITLE;
});

export default router;