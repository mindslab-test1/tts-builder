import Repository from "./Repository";

const resource = "/user";
export default {
    getUserMembershipInfo(userId) {
        return Repository.get(`${resource}/${userId}/userMembershipInfo`)
    },

    getCharConsumed(userId) {
        return Repository.get(`${resource}/${userId}/charConsumed`)
    },

    getUserInfo(userId) {
        return Repository.get(`${resource}/${userId}`)
    },
}