import 'url-search-params-polyfill';
import Repository from "./Repository";

const resource = "/job";
export default {
    toMakeTTS(voiceName, text) {
        var params = new URLSearchParams();
        params.append('text', text);
        return Repository.post(`${resource}/toMakeTTS/${voiceName}`, params);
    },
    checkItemCode(jobContent){
        return Repository.post(`${resource}/checkItemCode`, jobContent)
    },
    sendTTS(jobContent) {
        return Repository.post(`${resource}/sendTTS`, jobContent);
    },
    redownloadTTS(jobContent) {
        return Repository.post(`${resource}/download/redownloadTTS`, jobContent, {
            headers: {
                'Accept': 'audio/x-wav',
            },
            responseType: 'blob'
        });
    },
    redownloadEachTTS(jobContent) {
        return Repository.post(`${resource}/download/redownloadEachTTS`, jobContent, {
            responseType: 'blob'
        });
    },
    downloadTTS(jobContent) {
        return Repository.post(`${resource}/download/downloadTTS`, jobContent, {
            headers: {
                'Accept': 'audio/x-wav',
            },
            responseType: 'blob'
        });
    },
    downloadEachTTS(jobContent) {
        return Repository.post(`${resource}/download/downloadEachTTS`, jobContent, {
            responseType: 'blob'
        });
    },
};