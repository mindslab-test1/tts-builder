import Repository from "./Repository";

const resource = "/job";
export default {
    getMeteorologicalApi(){
        return Repository.get(`${resource}/meteorological`)
    },
    jobList() {
        return Repository.get(`${resource}/list`);
    },
    jobListByUserId(userId) {
        return Repository.get(`${resource}/listByUserId/${userId}`);
    },
    getJobContent(jobId) {
        return Repository.get(`${resource}/${jobId}`);
    },
    getUserDownloadInfo(userId){
      return Repository.get(`${resource}/${userId}/downloadInfo`)
    },
    updateJobContent(jobId, jobContent) {
        return Repository.put(`${resource}/${jobId}`, jobContent);
    },
    updateJobContentNew(jobId, jobContent) {
        return Repository.put(`${resource}/${jobId}/new`, jobContent);
    },
    updateJobContentOverwrite(jobId, newJobTitle, newJobId, jobContent) {
        return Repository.put(`${resource}/${jobId}/${newJobTitle}/${newJobId}/overwrite`, jobContent);
    },
    updateJobSpeakers(jobId, jobSpeakerIdList) {
        return Repository.put(`${resource}/${jobId}/speaker`, jobSpeakerIdList);
    },
    updateJobScript(jobId, jobScriptId, jobScript) {
        return Repository.put(`${resource}/${jobId}/script/${jobScriptId}`, jobScript);
    }
};