import Repository from "./Repository";

const resource = "/payment";

export default {
    getBillingFormInfo(price, userId) {
        const params = {
            params: {
                method: "billingPay",
                p: price,
                userId: userId
            }
        }
        return Repository.get(`${resource}/billingForm`, params);
    },

    unsubscribeUser(userId) {
        return Repository.get(`${resource}/planCancel/${userId}`);
    },

    getPaymentFilterResponseInfo(data){
        return Repository.post(`admin/filteringInfo`, data)
    },

    cancelPayment(tid, userId) {
        const params = {
            params:{
                tid: tid,
                userId: userId
            }
        }
        return Repository.post('admin/payment/payCancel',null, params);
    }

};