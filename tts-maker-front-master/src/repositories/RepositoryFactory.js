import JobRepository from "./jobRepository";
import SpeakerRepository from './speakerRepository';
import TTSRepository from './ttsRepository';
import DMZRepository from './dmzRepository';
import UserRepository from './userRepository';
import PaymentRepository from './paymentRepository'

const repositories = {
    job: JobRepository,
    speaker: SpeakerRepository,
    tts: TTSRepository,
    dmz: DMZRepository,
    user: UserRepository,
    payment: PaymentRepository
    // other repositories ...
  };
  
  export const RepositoryFactory = {
    get: name => repositories[name]
  };
  