
import { RepositoryFactory } from '@/repositories/RepositoryFactory';
const TTSRepository = RepositoryFactory.get('tts');
const JobRepository = RepositoryFactory.get('job');

const divideByIndexOfScripts = (scripts, index) => {
    return new Promise ((resolve) => {
        return resolve ({
            'prev' : scripts.slice(0, index),
            'next' : scripts.slice(index, scripts.length)
        });
    });
};
const detectConvertScriptsFromArray = (scripts, options = {}) => {
    return new Promise ((resolve, reject) => {
        const range = options.range || 'next';
        const detectScriptsSize = options.detectScriptsSize || 5;
        const detectActUpSize = options.detectActUpSize || 3;

        const targetScriptsAll = scripts[range];
        const detectRangeScripts = targetScriptsAll.slice(0, detectScriptsSize);

        const stateFilter = (scripts, ...compareStateArgs) => { return scripts.filter(script => compareStateArgs.indexOf(script.ttsState) > -1); };
        const waitScriptSates = ['W', 'E']; // WAIT, ERROR
        const detectRangeCompleteScripts = stateFilter(detectRangeScripts, 'S'); //STANDBY
        const detectRangeWaitScripts = stateFilter(detectRangeScripts.slice(0, detectActUpSize), ...waitScriptSates);
        
        let convertWaitScripts = [];
        //console.log("detectRangeCompleteScripts.length", detectRangeCompleteScripts.length);
        //console.log("detectRangeWaitScripts.length", detectRangeWaitScripts.length);

        if (detectRangeCompleteScripts.length < detectActUpSize) {
            convertWaitScripts = stateFilter(targetScriptsAll, ...waitScriptSates).slice(0, detectScriptsSize);
        }else if((detectRangeCompleteScripts.length >=  detectActUpSize) && (detectRangeWaitScripts.length > 0)) {
            convertWaitScripts = detectRangeWaitScripts;
        }
        return resolve({ detectRangeScripts : detectRangeScripts,  convertWaitScripts : convertWaitScripts });
    });
};

const toMakeTTSResult = ({status, data}) => {
    //console.log("status", status);
    //console.log("data", data);

    if (status == 200 && data.downloadURL && data.expiryDate ) {
        return { 
            'ttsState' : 'S', 'ttsResourceUrl' : data.downloadURL,
            'ttsResourceExpiryDate' : data.expiryDate, 
        };
    }else {
        return { 'ttsState' : 'E' };
    }
};

const requestTTSMakeFileLoop = async (commit, script) => {
    let reTry = 1;
    const reTryMax = 5;
    const scriptId = script.scriptId;
    if(script.ttsState === 'S' || script.scriptText.length === 0) return ;
    const sleep = (ms) => { return new Promise(resolve => setTimeout(resolve, ms)); };
    while (reTry <= reTryMax ) {
        commit('updateJobScriptFindByScriptId', { 'scriptId' : scriptId, 'ttsState' : 'C' });
        let result;
        try {
            result = toMakeTTSResult(await TTSRepository.toMakeTTS(script.voiceName, script.scriptText));
            console.log(result)
        } catch(e) {
            result = { 'ttsState' : 'E' };
        };
        commit('updateJobScriptFindByScriptId', Object.assign({ 'scriptId' : scriptId }, result));
        
        if(result.ttsState == 'S') {
            break;
        } else {
            if (reTry > reTryMax) {
                console.log('%c reTry 호출 초과 :' + reTryMax , 'background: yellow; color: red');
            } else {
                console.log('%c reTry 호출 시도 :' + reTry , 'background: red; color: black');
            }
        }
        reTry++;
        sleep(2000);
    }
};

const detectMoreConvertScripts = async (commit, scripts, index, divide = true) => {
    const divideScripts = await divideByIndexOfScripts(scripts, index);
    const targetScripts = await detectConvertScriptsFromArray(divideScripts);

    let currentScript = targetScripts.detectRangeScripts[0];
    if (currentScript.ttsState !== 'S') {
        const syncConvertWaitScripts = targetScripts.convertWaitScripts.splice(0, 1);
        currentScript = Promise.all(syncConvertWaitScripts.map( script => {
            return requestTTSMakeFileLoop(commit, script);
        }));
    }

    const convertWaitScripts = targetScripts.convertWaitScripts;
    if (convertWaitScripts.length > 0) {
        // console.log('convertWaitScripts convert 대상!!! convertWaitScripts.length = ' +  convertWaitScripts.length);
        convertWaitScripts.forEach(script => {
            requestTTSMakeFileLoop(commit, script);
        });
    }
    return currentScript;
};

const checkForConvertAvailableScriptIndex = (scripts, index) => {
    let candIndex = index;
    scripts.slice(index).some(script=> {
        if(script.scriptText.length > 0) {
            return true;
        }
        candIndex++;
    })

    return candIndex < scripts.length ? candIndex : -1;
};

export default {
    divideByIndexOfScripts,
    detectConvertScriptsFromArray,
    detectMoreConvertScripts,
    checkForConvertAvailableScriptIndex,
}