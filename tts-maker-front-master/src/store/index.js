import Vue from 'vue';
import Vuex from 'vuex';

import speaker from './modules/speaker';
import payment from './modules/payment';
import player from './modules/player';
import job from './modules/job';
import dmz from './modules/dmz';
import input from './modules/input';
import axios from 'axios';
import VueRouter from 'vue-router';
import router from '../routes/index';
import Repository from '../repositories/Repository'

Vue.use(Vuex);

export default new Vuex.Store({

    state: {
        userInfo: null,
        isLogin: false,
        isMaster: false,
        loginUser: "",
        charLimit: 3000,
        charConsumed: 0,
        status: "",
        roles: "ROLE_FREE",
        membership: 4
    },
    mutations: {
        loginSuccess(state, payload) {
            state.status = payload.userInfo
            state.loginUser = payload.userId
            state.isLogin = true
            state.membership = payload.productId
            state.roles = payload.roles
            state.charConsumed = payload.charConsumed
            state.charLimit = payload.charLimit
        },
        loginError(state) {
            state.isLogin = false
        },
        logout(state) {
            state.isLogin = false
            state.userInfo = null
        },
        masterUser(state) {
            state.isMaster = true
        },
        notMasterUset(state) {
            state.isMaster = false
        },
        updateCharConsumed(state, newCharConsumed) {
            state.charConsumed = newCharConsumed
        },
        updateMembership(state, membership) {
            state.membership = membership
        },
        updateRoles(state, newRole) {
            state.roles = newRole
        },
    },
    actions: {
        //로그인 시도
        login({state, commit}, loginObj) {
            const API_BASE_URL = process.env.API_BASE_URL;
            const getURL = [API_BASE_URL, '/login/userCheck'].join('');

            const token = localStorage.getItem("token");
            const data = {
                userId: loginObj.uid,
                userPw: loginObj.password,
            }
            if (token !== "") {
                data.token = token;
            }


            axios.post(getURL, data).then((res) => {
                if (res.data === "") {
                    alert("비밀번호 또는 아이디를 다시 확인해주세요.")
                } else if (res.data.confirm === 0 || res.data.auth === 0) {
                    alert("승인되지 않은 사용자입니다.")
                } else {
                    Repository.defaults.headers.common['Authorization'] = `${res.data.type}  ${res.data.token}`;
                    localStorage.setItem("token", res.data.token);

                    (res.data.auth === 1) ? commit("masterUser") : commit("notMasterUset")
                    const payload_val = "success"

                    commit("loginSuccess", {
                        status: payload_val,
                        userId: loginObj.uid,
                        productId: res.data.productId,
                        roles: res.data.roles,
                        charConsumed: res.data.charConsumed,
                        charLimit: res.data.charLimit,
                    })
                    router.push({name: res.data.pageUrl})
                }
            }).catch(() => {
                alert("아이디 또는 비밀번호를 다시 확인해주세요.");
            });
        },
        reauth({commit}, payload) {
            commit("loginSuccess", payload)
        },
        logout({commit}, loginObj) {
            const API_BASE_URL = process.env.API_BASE_URL;
            const getURL = [API_BASE_URL, '/login/logout'].join('');

            const token = localStorage.getItem("token");
            const data = {
                userId: loginObj.uid,
            }
            if (token !== "") {
                data.token = token;
            }
            axios.post(getURL, data).then((res) => {
                alert("로그아웃 되었습니다.")
                commit("logout")
                localStorage.removeItem("token")
                router.push({name: "loginView"})
            }).catch((e) => {
                alert("로그아웃 에러 : " + e);
            });
        },
        updateCharConsumed({commit}, newCharConsumed) {
            commit("updateCharConsumed", newCharConsumed);
        },
        updateMembership({commit}, membership) {
            commit('updateMembership', membership);
        },
        updateRoles({commit}, newRole) {
            commit("updateRoles", newRole);
        },
    },

    linkActiveClass: 'active',
    mode: 'hash',
    modules: {
        speaker,
        player,
        job,
        dmz,
        payment,
        input
    },
    strict: process.env.NODE_ENV !== 'production'
});