import uuidv4 from 'uuid/v4';
import JobScriptProcessor from '@/services/JobScriptProcessor';

import { RepositoryFactory } from '@/repositories/RepositoryFactory';
const JobRepository = RepositoryFactory.get('job');

const setisgetWeatherReport = ({commit}, isget) => {
    return commit('setisgetWeatherReport', isget);
};

/**
 * JOB List 저장 처리 - Action calle
 * - views/edit/EditJobButton.vue 사용
 * @param {*} jobList 작업 리스트
 */
const setJobList = ({ commit }, jobList) => {
    return commit('setJobList', jobList);
};

/**
 * JOB Title 저장 처리 - Action calle
 * - views/edit/EditJobButton.vue 사용
 * @param {*} jobTitle 작업 제목명
 */
const setJobTitle = ({commit}, jobTitle) => {
    return commit('setJobTitle', jobTitle);
};

/**
 * @param {*} clearJobTitle 
 */
const clearJobTitle = ({commit}) => {
    return commit('setJobTitle', '');
};

/**
 * JOB Content 저장(jobId, jobTitle, jobSpeakers, jobScripts);
 * - views/edit/EditJobButton.vue 사용
 * @param {*} jobId 
 * @param {*} jobTitle 
 * @param {*} jobSpeakers 
 * @param {*} jobScripts 
 */
const setJobContent = ({commit}, {jobId, jobTitle, jobSpeakers, jobScripts}) => {
    commit('setJobId', jobId);
    commit('setJobTitle', jobTitle);
    commit('setJobSpeakers', jobSpeakers);
    commit('setJobScripts', jobScripts);
};
/**
 *  JOB Content 초기화(jobId, jobTitle, jobSpeakers, jobScripts);
 * - views/edit/EditJobButton.vue 사용
 */
const clearJobContent = ({commit}) => {
    commit('setJobId', uuidv4());
    commit('setJobTitle', '');
    commit('setJobSpeakers', []);
    return commit('setJobScripts', []);
};

/**
 * JOB 기본정보 세팅(jobId, jobTitle)
 * @param {*} jobId 
 * @param {*} jobTitle 
 */
const setJobBasicInfo = ({commit}, {jobId, jobTitle}) => {
    commit('setJobId', jobId);
    commit('setJobTitle', jobTitle);
};
/************************** 리펙토링하자*************************************/
/**
 * 신규 JOB 생성
 * - views/edit/EditJobButton.vue 사용
 * - views/MakeHomeView.vue 사용
 */
const clearJobBasicInfo = ({commit}) => {
    commit('setJobId', uuidv4());
    commit('setJobTitle', '');
};

/**
 * JOB Speaker 처리 / START
 **/

const updateJobSpeakerSave = ({ }, {jobId, jobSpeakerIdList}) => {
    return jobApi.updateJobSpeakerList(jobId, jobSpeakerIdList);
};

/**
 * JOB Speaker All Row 삭제 처리 - Action calle
 */
const clearJobSpeakers = ({commit}) => {
    return commit('setJobSpeakers', []);
};

/**
 * JOB Speaker List 저장 처리 - Action calle
 * - views/edit/EditJobButton.vue 사용
 */
const setJobSpeakers = ({commit}, jobSpeakers) => {
    return commit('setJobSpeakers', jobSpeakers);
};

/**
 * JOB Speaker Row 추가 처리 - Action calle
 * - views/edit/EditJobButton.vue 사용
 * @param {*} addJobSpeaker 
 */
const addJobSpeaker = ({commit}, addJobSpeaker) => {
    return commit('addJobSpeaker', addJobSpeaker);
};

/**
 * JOB Speaker Row 삭제(index 기반) 처리 - Action calle
 * - views/speaker/SelectedSpeakerViewItem.vue 사용
 * @param {*} index 
 */
const removeAtJobSpeaker = ( { commit, state }, index) => {
    const deletedJobSpeakers = [];
    const currJobSpeaker = state.jobSpeakers[index];
    const isDeleteCandidate = [ 'S', 'U' ].indexOf(currJobSpeaker.rowState) > -1;
    if (isDeleteCandidate) {
        deletedJobSpeakers.push(Object.assign({}, state.jobSpeakers[index], { 'rowState' : 'D' }));
    }
    commit('removeAtJobSpeaker', { index : index, deletedJobSpeakers : deletedJobSpeakers });
};

/**
 * JOB Speaker Delete candidate Row 초기화 처리 - Action calle
 * - views/speaker/SelectedSpeakerViewItem.vue 사용
 */
const clearDeletedJobSpeakers = ( { commit }) => {
    return commit('clearDeletedJobSpeakers');
}

/**
 * JOB Speaker 처리 / END
 **/

/**
 * JOB Script 처리 / START
 **/

/**
 * JOB Script All Row 삭제 처리 - Action calle
 */
const clearJobScripts = ({commit}) => {
    return commit('setJobScripts', []);
};


/**
 * JOB Script List 저장 처리 - Action calle
 * - views/edit/EditJobButton.vue 사용
 * @param {*} jobScripts 
 */
const setJobScripts = ({ commit }, jobScripts) => {
    return commit('setJobScripts', jobScripts);
};

const setFakeJobScript = ({commit, state}) => {
    if(state.jobScripts.length == 0) {
        if(state.jobSpeakers.length > 0) {
            let { speakerId, voiceName } = state.jobSpeakers[state.jobSpeakers.length -1];
            let addJobScript = { 'scriptId' : uuidv4(), 'scriptText' : '', 'settings' : {}, 'ttsState' : 'W', 'rowState' : 'A' };
            addJobScript = Object.assign(addJobScript, {speakerId, voiceName});
            commit('insertAtCreateScript', { index: 0, script: addJobScript });
        }
    }
};

/**
 * JOB Script Delete candidate Row 초기화 처리 - Action calle
 * - views/speaker/SelectedSpeakerViewItem.vue 사용
 */
const clearDeletedJobScripts = ({commit}) => {
    return commit('clearDeletedJobScripts');
};

/**
 * 변경 텍스트 업데이트 처리 - Action callee
 * - views/edit/EditScriptListItem.vue 사용
 * @param {*} index 변경될 인덱스
 * @param {*} scriptText 변경될 텍스트
 */
const updateAtScriptText = ({ commit, state }, {index, scriptText }) => {
    const currScript = state.jobScripts[index];
    const isUpdateCandidate = [ 'S', 'U' ].indexOf(currScript.rowState) > -1;
    return commit('updateAtScriptText', { 'index' : index, 'scriptText' : scriptText, 'rowState': isUpdateCandidate ? 'U' : 'A' });
};

/**
 * 기존 스크립트 화자 업데이트 처리 - Action callee
 * - views/edit/EditScriptListItem.vue 사용
 * @param {*} index 변경될 인덱스
 * @param {*} changeSpeakerId 변경될 화자 ID
 * @param {*} changeVoiceName 변경될 화자 보이스
 */
const updateAtScriptSpeaker = ({ commit, state }, {index, changeSpeakerId, changeVoiceName }) => {
    const currScript = state.jobScripts[index];
    const isUpdateCandidate = [ 'S', 'U' ].indexOf(currScript.rowState) > -1;
    return commit('updateAtScriptSpeaker', { 'index' : index, 'speakerId' : changeSpeakerId, 'voiceName' : changeVoiceName, 'rowState': isUpdateCandidate ? 'U' : 'A' });
};

/**
 * 분리된 스크립트 특정 인덱스 삽입 처리 - Action calle
 * - views/edit/EditScriptListItem.vue 사용
 * @param {*} index 삽입하고자하는 index 
 * @param {*} splitScripts 분리된 스크립트 List
 */
const insertAtSplitScripts = ({ commit, state }, { index, splitScripts }) => {
    let focusScriptId;
    const deletedJobScripts = [];
    const addJobScripts = [];
    const currScript = state.jobScripts[index];
    const isDeleteCandidate = [ 'S', 'U' ].indexOf(currScript.rowState) > -1;
    if (isDeleteCandidate) {
        deletedJobScripts.push(Object.assign({}, state.jobScripts[index], { 'rowState' : 'D' }));
    }
    splitScripts.forEach((item, index) => {
        const mergedScript = Object.assign({ 'scriptId' : uuidv4(), 'scriptText' : '', 'settings' : {}, 'ttsState' : 'W', 'rowState' : 'A' }, item);
        delete mergedScript.type;
        if(mergedScript.hasOwnProperty("focus")) {
            focusScriptId = mergedScript.scriptId;
            delete mergedScript.focus;
        }
        addJobScripts.push(mergedScript);
    });
    commit('insertAtSplitScripts', { index : index, addJobScripts : addJobScripts, deletedJobScripts : deletedJobScripts });
    return focusScriptId;
};

/**
 * 신규 스크립트 생성 처리(alt+enter) - Action callee
 * - views/edit/EditScriptListItem.vue 사용
 * @param {*} payload {index: 삽입하고자하는 index, data: 삽입하고자하는 스크립트}
 */
const insertAtCreateScript = ({ commit, state }, { index, data }) => {
    const addJobScript = { 'index' : index, script : Object.assign({ 'scriptId' : uuidv4(), 'scriptText' : '', 'settings' : {}, 'ttsState' : 'W', 'rowState' : 'A' }, data) };
    commit('insertAtCreateScript', addJobScript);
    return addJobScript;
};

/**
 * 스크립트 삭제 처리 - Action callee
 * - views/edit/EditScriptListItem.vue 사용
 * @param {*} index 삭제하고자하는 index
 */
const removeAtScript = ( { commit, state }, index) => {
    const deletedJobScripts = [];
    const currScript = state.jobScripts[index];
    const isDeleteCandidate = [ 'S', 'U' ].indexOf(currScript.rowState) > -1;
    if (isDeleteCandidate) {
        deletedJobScripts.push(Object.assign({}, state.jobScripts[index], { 'rowState' : 'D' }));
    }
    commit('removeAtScript', { index : index, deletedJobScripts : deletedJobScripts });
};

/**
 * 스크립트 병합 처리 - Action callee
 * - views/edit/EditScriptListItem.vue 사용
 * @param {*} index merge 할 Index
 * @param {*} prevScript 이전 스크립트
 * @param {*} currScript 현재 스크립트
 */
const mergeAtScript = ({ commit, state }, { index, prevScript, currScript })  => {
    const deletedJobScripts = [];
    const changeText = prevScript.scriptText + currScript.scriptText;
    const isDeleteCandidate = [ 'S', 'U' ].indexOf(currScript.rowState) > -1;
    if (isDeleteCandidate) {
        deletedJobScripts.push(Object.assign({}, state.jobScripts[index], { 'rowState' : 'D' }));
    } 
    
    commit('removeAtScript', { index : index, deletedJobScripts : deletedJobScripts });
    const isUpdateCandidate = [ 'S', 'U' ].indexOf(prevScript.rowState) > -1;
    commit('updateAtScriptText', { index : index -1, 'scriptText' : changeText, 'rowState' : 'U' });
    return prevScript.scriptId;
};

/**
 * 스크립트 재할당 처리 - Action callee
 * - views/speaker/SelectedSpeakerViewItem.vue 사용
 * @param {*} param0 
 * @param {*} payload 
 */
const setReallocationScripts = ({commit}, payload) => {
    return commit('setReallocationScripts', payload);
};


/**
 * TTS 변환 대상 감지 처리
 * - views/player/Player.vue 사용
 * @param {*} index 인덱스
 */
const detectMoreConvertScripts = async ({commit, state}, index) => {
    await JobScriptProcessor.detectMoreConvertScripts(commit, state.jobScripts, index);
    const jobScript = state.jobScripts[index];
    await JobRepository.updateJobScript(state.jobId, jobScript.scriptId, jobScript);
};

/**
 * TTS 변환전 텍스트 유효성검사
 * @param {*} index 인덱스 
 */
const checkForConvertAvailableScriptIndex = ({state}, index) => {
    return JobScriptProcessor.checkForConvertAvailableScriptIndex(state.jobScripts, index);
};



/**
 * JOB Script 처리 / END
 **/

export default {
    setisgetWeatherReport,
    setJobList,
    setJobTitle,
    clearJobTitle,
    setJobContent,
    clearJobContent,

    setJobBasicInfo,
    clearJobBasicInfo,

    // JOB Speaker 처리 / START
    updateJobSpeakerSave,
    clearJobSpeakers,
    setJobSpeakers,
    addJobSpeaker,
    removeAtJobSpeaker,
    clearDeletedJobSpeakers,
    // JOB Speaker 처리 / END

    // JOB Script 처리 / START
    clearJobScripts,
    setJobScripts,
    setFakeJobScript,
    clearDeletedJobScripts,

    updateAtScriptText,
    updateAtScriptSpeaker,
    insertAtSplitScripts,
    insertAtCreateScript,
    removeAtScript,
    mergeAtScript,
    setReallocationScripts,
    // JOB Script 처리 / END

    //Detect Convert Script
    detectMoreConvertScripts,
    checkForConvertAvailableScriptIndex,

};