import actions from './actions';
import getters from './getters';
import mutations from './mutations';

const state = {
    jobList: [],
    jobId: "",
    jobTitle: "",

    jobSpeakers: [],
    deletedJobSpeakers: [],

    jobScripts: [],
    deletedJobScripts: [],
    isgetWeatherReport: false
};

export default {
    namespaced: true,
    state,
    actions,
    getters,
    mutations,
};