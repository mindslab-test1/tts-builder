const setJobList = (state, setJobList) => {
    state.jobList = setJobList || [];
};

const setisgetWeatherReport = (state, isget) => {
    state.isgetWeatherReport = isget;
} 

/**
 * JOB ID 저장
 * @param {*} jobId JOB ID
 */
const setJobId = (state, jobId) => {
    state.jobId = jobId;
}

/**
 * JOB TITLE 저장
 * @param {*} jobId JOB TITLE
 */
const setJobTitle = (state, jobTitle) => {
    state.jobTitle = jobTitle;
}


const clearJobTitle = (state) => {
    state.jobTitle = '';
};

/**
 * JOB Speaker List 저장
 * @param {*} jobSpeakers 
 */
const setJobSpeakers = (state, jobSpeakers) => {
    state.jobSpeakers = (jobSpeakers || []);
    state.deletedJobSpeakers = [];
};

/**
 * JOB Speaker Row 추가
 * @param {*} addJobSpeaker 
 */
const addJobSpeaker = (state, addJobSpeaker) => {
    state.jobSpeakers.push(addJobSpeaker);
};

/**
 * JOB Speaker Row 삭제(index 기반)
 * @param {*} index 
 * @param {*} deletedJobSpeakers 
 */
const removeAtJobSpeaker = (state, {index, deletedJobSpeakers}) => {
    state.deletedJobSpeakers.push(...deletedJobSpeakers);

    if ( state.jobSpeakers.length ) {
        state.jobSpeakers.splice(index, 1);
    }
};

/**
 * JOB Speaker Delete candidate Row 초기화 처리 - Action calle
 */
const clearDeletedJobSpeakers = (state) => {
    state.deletedJobSpeakers = [];
};


const setJobScripts = (state, jobScripts) => {
    state.deletedJobScripts = [];
    state.jobScripts = jobScripts || [];
};

/**
 * JOB Script Delete candidate Row 초기화 처리 - Action calle
 */
const clearDeletedJobScripts = (state) => {
    state.deletedJobScripts = [];
}

/**
 * 변경 텍스트 업데이트 처리 - Action callee
 * @param {*} index 변경하고자하는 스크립트 인덱스
 * @param {*} scriptText 변경하고자하는 텍스트
 * @param {*} rowState 변경하고자하는 스크립트 ROW 상태
 */
const updateAtScriptText = (state, { index, scriptText, rowState }) => {
    Object.assign(state.jobScripts[index], { 'scriptText' : scriptText, 'rowState' : rowState, 'ttsState' : 'W', 'expiryDate' : null, 'url' : null });
};

/**
 * 기존 스크립트 화자 업데이트 처리 - Action callee
 * @param {*} index 변경하고자하는 스크립트 인덱스
 * @param {*} speakerId 변경하고자하는 화자 ID
 * @param {*} voiceName 변경하고자하는 화자 VoiceName
 * @param {*} rowState 변경하고자하는 스크립트 ROW 상태
 */
const updateAtScriptSpeaker = (state, { index, speakerId, voiceName, rowState }) => {
    Object.assign(state.jobScripts[index], { 'speakerId' : speakerId, 'voiceName' : voiceName, 'rowState' : rowState, 'ttsState' : 'W', 'expiryDate' : null, 'url' : null });
};

/**
 * 분리된 스크립트 특정 인덱스 삽입 처리 - Action calle
 * @param {*} index 삽입할 레코드 인덱스 위치
 * @param {*} addScripts 추가하고자하는 스크립트
 * @param {*} deletedScripts 삭제될 스크립트
 */
const insertAtSplitScripts = (state, { index, addJobScripts, deletedJobScripts }) => {
    state.deletedJobScripts.push(...deletedJobScripts);
    state.jobScripts.splice(index, 1, ...addJobScripts);
};

/**
 * 신규 스크립트 생성 처리(alt+enter) - Action callee
 * @param {*} index 신규생성할 인덱스 위치
 * @param {*} script 신규 생성될 스크립트
 */
const insertAtCreateScript = (state, { index, script }) => {
    state.jobScripts.splice(index, 0, script);
};


/**
 * 스크립트 삭제 처리 - Action callee
 * @param {*} 삭제될 인덱스 위치 
 * @param {*} 삭제할 스크립트
 */
const removeAtScript = (state, { index, deletedJobScripts }) => {
    state.deletedJobScripts.push(...deletedJobScripts);

    if ( state.jobScripts.length ) {
        state.jobScripts.splice(index, 1);
    }
};

/**
 * 스크립트 재할당 처리 - Action callee
 * @param {*} targetScripts 재할당할 스크립트
 * @param {*} speakerId 변경될 화자 ID
 * @param {*} speakerNm 변경될 화자 이름
 * @param {*} voiceName 변경될 화자 보이스이름
 */
const setReallocationScripts = (state, {targetScripts, speakerId, speakerNm, voiceName}) => {
    if(targetScripts) {
        targetScripts.forEach((item, index) => {
            const rowIndex = state.jobScripts.findIndex(script => script.scriptId === item.scriptId);
            const targetScript = state.jobScripts[rowIndex];
            const isUpdateCandidate = [ 'S', 'U' ].indexOf(targetScript.rowState) > -1;
            const updateData = {'speakerId' : speakerId, 'speakerNm': speakerNm, 'voiceName': voiceName, 'ttsState' : 'W', 'rowState' : isUpdateCandidate ? 'U' : 'A'};
            Object.assign(state.jobScripts[rowIndex], updateData);
        });
    }
};

const updateJobScriptFindByScriptId = (state, payload) => {
    const searchKey = payload.scriptId;
    const index = state.jobScripts.findIndex(script => script.scriptId === searchKey);
    Object.assign(state.jobScripts[index], payload);
    return state.jobScripts[index];
};

export default {
    setisgetWeatherReport,
    setJobList,
    setJobId,
    setJobTitle,
    clearJobTitle,
    // JOB Speaker 처리 / START
    setJobSpeakers,
    addJobSpeaker,
    removeAtJobSpeaker,
    clearDeletedJobSpeakers,
    // JOB Speaker 처리 / END

    // JOB Script 처리 / START
    
    setJobScripts,

    clearDeletedJobScripts,
    updateAtScriptText,
    updateAtScriptSpeaker,
    insertAtSplitScripts,
    insertAtCreateScript,
    removeAtScript,
    setReallocationScripts,

    updateJobScriptFindByScriptId,
    // JOB Script 처리 / END
};