
// JOB Speaker getter(speakerId)
const getJobSpeaker = (state) => (speakerId) => {
    return state.jobSpeakers.find(jobSpeakers => jobSpeakers.speakerId === speakerId);
};

// JOB Speaker total count
const countJobSpeakers = (state) => () => {
    return state.jobSpeakers.length
};

// JOB Speaker Index
const getJobSpeakerIndex = (state) => (speakerId) => {
    return state.jobSpeakers.findIndex(jobSpeaker => jobSpeaker.speakerId === speakerId);
};

// JOB Speaker getter(index)
const getJobSpeakerFindByIndex = (state) => (index)  =>{
    return state.jobSpeakers[index];
};

// JOB Script getter(index)
const getJobScriptFindByIndex = (state) => (index)  =>{
    return state.jobScripts[index];
};

export default {
    getJobSpeaker,
    countJobSpeakers,
    getJobSpeakerIndex,
    getJobSpeakerFindByIndex,
    getJobScriptFindByIndex,
};