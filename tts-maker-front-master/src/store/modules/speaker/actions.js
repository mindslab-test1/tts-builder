/**
 * Speaker List 저장 - Action calle
 * - views/speaker/SelectedSpeakerView.vue 사용
 * @param {*} speakers 
 */
const setSpeakers = ({ commit }, speakers) => {
    return commit('setSpeakers', speakers);
};

const setSelectedSpeakerIndex = ({ commit }, index) => {
    commit('setSelectedSpeakerIndex', index);
};

/**
 * 선택한 화자 선택 취소
 */
const clearSelectedSpeakerIndex = ({commit}) => {
    commit('clearSelectedSpeakerIndex');
};

export default {
    setSpeakers,
    setSelectedSpeakerIndex,
    clearSelectedSpeakerIndex,
};