
const getPalette = ( state ) => {
    return function (index) {
        return state.palettes[index % state.palettes.length];
    };
};

const getSpeaker = (state) => (speakerId) => {
    return state.speakers.find(speaker => speaker.speakerId === speakerId);
};
const getChoiceSpeakers = ( state ) => {
    return state.choiceSpeakers || [];
};

const getChoiceSpeakersVoiceNameMap = (state) => {
    let voiceNameMap = {};
    state.choiceSpeakers.forEach(item =>  voiceNameMap[item.speakerId] = item.voiceName );
    return voiceNameMap;
};

// 초이스한 화자 데이터 getter(speakerId)
const getChoiceSpeaker = (state) => (speakerId) => {
    return state.choiceSpeakers.find(choiceSpeaker => choiceSpeaker.speakerId === speakerId);
};

const getChoiceSpeakerIndex = (state) => (speakerId) => {
    return state.choiceSpeakers.findIndex(choiceSpeaker => choiceSpeaker.speakerId === speakerId);
};

const getChoiceSpeakerByspeakerId = (state) => (speakerId) => {
    return state.choiceSpeakers.find(choiceSpeaker => choiceSpeaker.speakerId === speakerId);
};


const getSelectedSpeakerIndex = (state) => {
    return state.selectedSpeakerIndex;
};

export default {
    getPalette,
    getSpeaker,
    getChoiceSpeakers,
    getChoiceSpeakersVoiceNameMap,
    getChoiceSpeaker,
    getChoiceSpeakerIndex,
    getChoiceSpeakerByspeakerId,
    getSelectedSpeakerIndex
};