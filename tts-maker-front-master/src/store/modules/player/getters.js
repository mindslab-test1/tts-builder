const getCursor = ( state ) => {
    return state.cursor;
};

const getIsPlaying = ( state ) => {
    return state.isPlaying;
};

export default {
    getCursor,
    getIsPlaying,
};