
const setInputJobTitle1 = (state, title) => {
    state.jobInputTitle1 = title;
}

const setInputJobTitle2 = (state, title) => {
    state.jobInputTitle2 = title;
}

export default {
    setInputJobTitle1,
    setInputJobTitle2,
};