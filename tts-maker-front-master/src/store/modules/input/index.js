import actions from './actions';
import mutations from './mutations';

const state = {
    jobInputTitle1: "",
    jobInputTitle2: "",

};

export default {
    namespaced: true,
    state,
    actions,
    mutations,
};