const setInputJobTitle1 = ({ commit }, title) => {
    return commit('setInputJobTitle1', title);
};

const setInputJobTitle2 = ({ commit }, title) => {
    return commit('setInputJobTitle2', title);
};


export default {
    setInputJobTitle1,
    setInputJobTitle2

};