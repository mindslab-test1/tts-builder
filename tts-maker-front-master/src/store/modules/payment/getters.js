const getBillingFormData = (state) =>{
    return state.billingFormData;
}

export default {
    getBillingFormData
};