
/**
 * User Category List 추가
 * @param {*} setUserCategoryList 
 */
const setUserCategoryList = ({commit}, setUserCategoryList) => {
    return commit('setUserCategoryList', setUserCategoryList);
};


/**
 * Set Job Kind
 * @param {*} setJobKind 
 */
const setJobKind = ({commit}, setJobKind) => {
    return commit('setJobKind', setJobKind);
}
const clearJobKind = ({commit}) => {
    return commit('setJobKind', 'PG');
};

/**
 * Set Selected Category
 * @param {*} setSelectedCategory 
 */
const setSelectedCategory = ({commit}, setSelectedCategory) => {
    return commit('setSelectedCategory', setSelectedCategory);
}
const clearSelectedCategory = ({commit}) => {
    return commit('setSelectedCategory', '');
};

/**
 * Set Selected CategoryId
 * @param {*} setSelectedCategoryId 
 */
const setSelectedCategoryId = ({commit}, setSelectedCategoryId) => {
    return commit('setSelectedCategoryId', setSelectedCategoryId);
}
const clearSelectedCategoryId = ({commit}) => {
    return commit('setSelectedCategoryId', '');
};

/**
 * Set Selected Channel
 * @param {*} setSelectedChannel 
 */
const setSelectedChannel = ({commit}, setSelectedChannel) => {
    return commit('setSelectedChannel', setSelectedChannel);
}
const clearSelectedChannel = ({commit}) => {
    return commit('setSelectedChannel', '');
};

/**
 * Set Selected Date
 * @param {*} setSelectedChannel 
 */
const setSelectedDate = ({commit}, setSelectedDate) => {
    return commit('setSelectedDate', setSelectedDate);
}
const clearSelectedDate = ({commit}) => {
    return commit('setSelectedDate', '');
};

/**
 * Set Selected Title
 * @param {*} setSelectedChannel 
 */
const setSelectedTitle = ({commit}, setSelectedTitle) => {
    return commit('setSelectedTitle', setSelectedTitle);
}
const clearSelectedTitle = ({commit}) => {
    return commit('setSelectedTitle', '');
};

/**
 * Set ItemCode
 * @param {*} setItemCode 
 */
const setItemCode = ({commit}, setItemCode) => {
    return commit('setItemCode', setItemCode);
}
const clearItemCode = ({commit}) => {
    return commit('setItemCode', '');
};


export default {
    setUserCategoryList,
    setJobKind,
    setSelectedCategory,
    setSelectedCategoryId,
    setSelectedChannel,
    setSelectedDate,
    setSelectedTitle,
    setItemCode,
    clearSelectedCategory,
    clearSelectedCategoryId,
    clearJobKind,
    clearSelectedChannel,
    clearSelectedDate,
    clearSelectedTitle,
    clearItemCode,
};
